package sx.projects.pgumo.integration.webservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.common.MsgBean;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.datastore.SXTransaction;
import sx.datastore.params.SXUpdateObjParams;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.RguMoscowTransformerFactory;
import sx.projects.pgumo.integration.exceptions.RguIntegrationException;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.exceptions.RguTransformationFactoryException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.RGUServiceFacade;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.*;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.*;

public class RguIntegrator implements RguTransformerCallback {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.RguIntegrator.class);
    private static final Collection<String> TRANSFORMING_OBJECTS_TYPES = new HashSet<String>();

    static {
        TRANSFORMING_OBJECTS_TYPES.add("PsPassport");
        TRANSFORMING_OBJECTS_TYPES.add("RStateStructure");
    }

    private RGUServiceFacade service;
    private RguTransformerFactory transformerFactory = RguMoscowTransformerFactory.getFactory();
    private long lastSsn = 0;
    private long ssnTo = 0;
    private long currentSsn = 0;
    private boolean updateDictionaries = false;
    private boolean saveArchivedObjects = false;
    private boolean updateOnlyRegional = false;
    private List<String> selectedServices = new ArrayList<String>();
    private List<String> selectedOrgs = new ArrayList<String>();

    private RguIntegrator(String wsdl, String projectCode) {
        service = new RGUServiceFacade(wsdl);
        transformerFactory = RguTransformerFactoryFactory.getRguTransformerFactoryFactory().getTransformerFactory(projectCode);
    }

    public static sx.projects.pgumo.integration.webservice.RguIntegrator getRguIntegrator(String wsdl, String projectCode) {
        return new sx.projects.pgumo.integration.webservice.RguIntegrator(wsdl, projectCode);
    }

    public MsgBean integrate() throws RguIntegrationException, InterruptedException {
        log.debug("*************************************************************************************************");
        log.debug("������ ������� �� ���");

        log.error(" *** ������ ������ 100515 *** ");

        if (!selectedServices.isEmpty() || !selectedOrgs.isEmpty()) {
            importSelectedServices();
            importSelectedOrgs();
        } else {
            if (getUpdateDictionaries()) {
                getListDictionary();
            }

            long ssnFrom = getLastSsn();
            long ssnTo = getSsnTo();
            currentSsn = getChanges(ssnFrom, ssnTo);

            getRevokationList();
        }

        log.debug("������ �� ��� ������ �������");
        log.debug("*************************************************************************************************");
        setCurrentSsn(currentSsn);
        return new MsgBean("All ok");
    }

    private void importSelectedServices() {
        log.error("����������� ������ �� ������...");
        log.error("������: " + selectedServices);

        for (String idRgu : selectedServices) {
            GetPsPassportResponse.PsPassport psPassport = service.getServicePassport(Long.valueOf(idRgu));
            try {
                transformObj(psPassport);
                log.error("������� ������������� ������ ID " + idRgu);
            } catch (Exception e) {
                log.error("������ ��� ������� ������ ID " + idRgu);
                log.error(e.getMessage(), e);
            }
        }
    }

    private void importSelectedOrgs() {
        log.error("����������� ��� �� ������...");
        log.error("���: " + selectedOrgs);

        for (String idRgu : selectedOrgs) {
            GetRStateStructureResponse.RStateStructure stateStructure = service.getStateStructure(Long.valueOf(idRgu));
            try {
                transformObj(stateStructure);
                log.error("������� ������������ ��� ID " + idRgu);
            } catch (Exception e) {
                log.error("������ ��� ������� ��� ID " + idRgu);
                log.error(e.getMessage(), e);
            }
        }
    }

    private long getChanges(long ssnFrom, long ssnTo) throws RguIntegrationException {
        RGUServiceFacade.ChangedObjects changedObjectsInfo = service.getChangedObjects(ssnFrom, ssnTo);
        List<GetChangesResponse.ObjectRefList.List.ObjectRef> changedObjects = changedObjectsInfo.getObjects();
        SXTransaction.start();
        for (GetChangesResponse.ObjectRefList.List.ObjectRef changedObject : changedObjects) {
            String objectType = changedObject.getObjectType();
            if (TRANSFORMING_OBJECTS_TYPES.contains(objectType)) {
                BigInteger objectId = changedObject.getObjectId();

                try {
                    transformObj(objectType, objectId);
                } catch (RguTransformationException e) {
                    log.error(e.getMessage(), e);
                    try {
                        SXTransaction.rollback();
                    } catch (SQLException e1) {
                        log.error(e1.getMessage(), e1);
                    }
                    log.debug("�� ����� ������� �� ��� ��������� ������");
                    log.debug("*************************************************************************************************");
                    throw new RguIntegrationException("������ ��� ���������� � ���");
                } catch (Throwable t) {
                    log.error(t.getMessage(), t);
                    try {
                        SXTransaction.rollback();
                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                    }
                    log.debug("�� ����� ������� �� ��� ��������� ������");
                    log.debug("*************************************************************************************************");
                    throw new RguIntegrationException("������ ��� ���������� � ���");
                }
            }
        }

        try {
            SXTransaction.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }

        return changedObjectsInfo.getCurrentSsn();
    }

    private SXObj transformObj(Object transformingObj) throws RguTransformationException {
        SXObj transformedObj = null;

        if (transformingObj != null) {
            try {
                RguTransformer transformer = transformerFactory.getTransformer(transformingObj);
                transformedObj = transformer.transform(transformingObj, this);
            } catch (RguTransformationFactoryException e) {
                log.warn(e.getMessage(), e);
            }
        }

        if (transformedObj == null) {
            throw new RguTransformationException("�� �������� ���������������� ������ ������� ����" + transformingObj);
        }

        return transformedObj;
    }

    private SXObj transformObj(@Nonnull String objectType, BigInteger objectId) throws RguTransformationException {
        Object transformingObj = null;

        if ("RStateStructure".equals(objectType)) {
            transformingObj = service.getStateStructure(objectId.longValue());
        } else if ("PsPassport".equals(objectType)) {

            transformingObj = service.getServicePassport(objectId.longValue());
            if (transformingObj != null) {
                GetPsPassportResponse.PsPassport psPassport = ((GetPsPassportResponse.PsPassport) transformingObj);
                String level = psPassport.getAdministrativeLevel().getValue();
                if (!updateOnlyRegional || (updateOnlyRegional && level.equals("REGIONAL"))) {
                    log.error("Processing PsPassport " + objectType + " ID " + objectId + " " + psPassport.getShortTitle());
                    return transformObj(transformingObj);
                } else {
                    log.error("Skipping PsPassport " + objectType + " ID " + objectId + " " + psPassport.getShortTitle() + " -- " + level);
                    return null;
                }
            } else {
                log.error("ERROR - processing null object PsPassport");
                return null;
            }
        }

        return transformObj(transformingObj);
    }

    private void deleteObj(@Nonnull String objectType, BigInteger objectId) throws RguTransformationException {
        Object transformingObj = null;

        if ("RStateStructure".equals(objectType)) {
            transformingObj = service.getStateStructure(objectId.longValue());
        } else if ("PsPassport".equals(objectType)) {
            transformingObj = service.getServicePassport(objectId.longValue());
        } else {
            log.error("������� �������� ������� ������ " + objectType + " ID " + objectId.longValue() + ", ������� �� ������� ���� �� �����");
        }

        if (transformingObj != null) {
            RguTransformer transformer = transformerFactory.getTransformer(transformingObj);
            transformer.delete(transformingObj, this);
        }
    }

    @Nullable
    public SXObj getTransformedObject(@Nonnull String ref) throws RguTransformationException {
        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
        if (rguRef.id == null) {
            return null;
        }
        return transformObj(rguRef.type, rguRef.id);
    }

    private long getRevokationList() throws RguIntegrationException {
        RGUServiceFacade.DeletedObjects deletedObjectsInfo = service.getDeletedObjects(getLastSsn(), getSsnTo());
        List<GetRevokationListResponse.ObjectRefList.List.ObjectRef> deletedObjects = deletedObjectsInfo.getObjects();
        SXTransaction.start();
        for (GetRevokationListResponse.ObjectRefList.List.ObjectRef deletedObject : deletedObjects) {
            String objectType = deletedObject.getObjectType();
            if (TRANSFORMING_OBJECTS_TYPES.contains(objectType)) {
                BigInteger objectId = deletedObject.getObjectId();

                try {
                    deleteObj(objectType, objectId);
                } catch (RguTransformationException e) {
                    log.error(e.getMessage(), e);
                    try {
                        SXTransaction.rollback();
                    } catch (SQLException e1) {
                        log.error(e1.getMessage(), e1);
                    }
                    log.debug("�� ����� ������� �� ��� ��������� ������");
                    log.debug("*************************************************************************************************");
                    throw new RguIntegrationException("������ ��� ���������� � ���");
                } catch (Throwable t) {
                    log.error(t.getMessage(), t);
                    try {
                        SXTransaction.rollback();
                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                    }
                    log.debug("�� ����� ������� �� ��� ��������� ������");
                    log.debug("*************************************************************************************************");
                    throw new RguIntegrationException("������ ��� ���������� � ���");
                }
            }
        }

        try {
            SXTransaction.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }

        return deletedObjectsInfo.getCurrentSsn();
    }

    public void getListDictionary() throws RguIntegrationException, InterruptedException {
        RGUServiceFacade.DictionaryObjects dictionaries = service.getDictionaryObjects();
        List<GetListDictionaryResponse.ObjectRefList.List.ObjectRef> changedObjects = dictionaries.getDictionaries();

        //SXTransaction.start();

        for (GetListDictionaryResponse.ObjectRefList.List.ObjectRef changedObject : changedObjects) {
            BigInteger objectId = changedObject.getId();

            log.error(" - NOT ERROR REALLY :) - Processing object ID " + objectId);

            try {
                transformObj(changedObject);
            } catch (RguTransformationException e) {
                log.error(e.getMessage(), e);
//                try {
//                    SXTransaction.rollback();
//                } catch (SQLException e1) {
//                    log.error(e1.getMessage(), e1);
//                }
                log.debug("�� ����� ������� �� ��� ��������� ������");
                log.debug("*************************************************************************************************");
                throw new RguIntegrationException("������ ��� ���������� � ���");
            } catch (Throwable t) {
                log.error(t.getMessage(), t);
//                try {
//                    SXTransaction.rollback();
//                } catch (SQLException e) {
//                    log.error(e.getMessage(), e);
//                }
                log.debug("�� ����� ������� �� ��� ��������� ������");
                log.debug("*************************************************************************************************");
                throw new RguIntegrationException("������ ��� ���������� � ���");
            }
        }

//        try {
//            SXTransaction.commit();
//        } catch (SQLException e) {
//            log.error(e.getMessage(), e);
//        }

        for (GetListDictionaryResponse.ObjectRefList.List.ObjectRef changedObject : changedObjects) {
            String objectName = changedObject.getName();
            RGUServiceFacade.DictionaryObjectsByCode dictionariesByCode;
            List<GetDictionaryResponse.ObjectRefList.List.ObjectRef> dictionaryList;

            log.error("PROCESSING " + objectName);

            try {
                dictionariesByCode = service.getDictionary(objectName, objectName.equals("TERRITORY") ? "1036" : null);
                dictionaryList = dictionariesByCode.getDictionaries();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                continue;
            }

//            Thread.sleep(1000);

//            SXTransaction.start();

            if (objectName.equals("OBJECT_TYPES")) {
                int objectTypeStep = 0;
                String codeReqDicRgu = "", codeDictRgu = "";

                for (GetDictionaryResponse.ObjectRefList.List.ObjectRef dictionary : dictionaryList) {
                    switch (objectTypeStep) {
                        case 0:
                            codeReqDicRgu = dictionary.getDescription();
                            break;

                        case 1:
                            codeDictRgu = dictionary.getDescription();
                            break;

                        case 2:
                            RguTransformationUtils.updateClassification(codeDictRgu, codeReqDicRgu);
                            break;
                    }

                    objectTypeStep = objectTypeStep == 2 ? 0 : objectTypeStep + 1;
                }
            } else {
                RguTransformationUtils.clearOrphans();
                Map<String, SXObj> objects = new HashMap<String, SXObj>();

                for (GetDictionaryResponse.ObjectRefList.List.ObjectRef dictionary : dictionaryList) {
                    log.error("========== �������������� ������������� " + objectName + " " + dictionary.getId());
                    BigInteger parentId = dictionary.getParent();
                    if (parentId == null) {
                        log.error("Parent NOT FOUND - " + dictionary.getId() + " - setting default parent " + changedObject.getId());
                        dictionary.setParent(changedObject.getId());
                    }

                    try {
                        SXObj obj = transformObj(dictionary);
                        objects.put(dictionary.getId().toString(), obj);
                    } catch (RguTransformationException e) {
                        log.error(e.getMessage(), e);
//                        try {
//                            SXTransaction.rollback();
//                        } catch (SQLException e1) {
//                            log.error(e1.getMessage(), e1);
//                        }
                        log.debug("�� ����� ������� �� ��� ��������� ������");
                        log.debug("*************************************************************************************************");
                        throw new RguIntegrationException("������ ��� ���������� � ���");
                    } catch (Throwable t) {
                        log.error(t.getMessage(), t);
//                        try {
//                            SXTransaction.rollback();
//                        } catch (SQLException e) {
//                            log.error(e.getMessage(), e);
//                        }
                        log.debug("�� ����� ������� �� ��� ��������� ������");
                        log.debug("*************************************************************************************************");
                        throw new RguIntegrationException("������ ��� ���������� � ���");
                    }
                }

                Map<String, String> orphans = RguTransformationUtils.getOrphans();
                for (Map.Entry<String, String> orphan : orphans.entrySet()) {
                    SXObj parent = objects.get(orphan.getValue());
                    if (parent != null) {
                        SXObj obj = objects.get(orphan.getKey());
                        if (obj != null) {
                            String code = RguTransformationUtils.updateCode(parent.getId(), orphan.getKey());
                            try {
                                SXUpdateObjParams uParams = new SXUpdateObjParams(obj.getId());
                                uParams.setRunHandlers(false);
                                uParams.setUpdateReadOnlyAttr(true);
                                Map<String, Object> dataMap = new HashMap<String, Object>();
                                dataMap.put("parent", parent.getId());
                                dataMap.put("code", code);
                                uParams.setData(dataMap);
                                uParams.updateObj();

                                log.error("SUCCESSFULLY updated orphan " + orphan.getKey() + " with parent " + orphan.getValue());
                            } catch (Exception e) {
                                log.error("ERROR updating orphan " + orphan.getKey() + " with parent " + orphan.getValue());
                                log.error(e.getMessage(), e);
                            }
                        } else {
                            log.error("ERROR NOT FOUND orphan " + orphan.getKey() + " with parent " + orphan.getValue());
                        }
                    } else {
                        log.error("ERROR PARENT NOT FOUND at orphan " + orphan.getKey() + " with parent " + orphan.getValue());
                    }
                }
            }

//            try {
//                SXTransaction.commit();
//            } catch (SQLException e) {
//                log.error(e.getMessage(), e);
//            }
        }
    }

    public long getLastSsn() {
        return lastSsn;
    }

    public void setLastSsn(long lastSsn) {
        this.lastSsn = lastSsn;
    }

    public long getSsnTo() {
        return ssnTo;
    }

    public void setSsnTo(long ssnTo) {
        this.ssnTo = ssnTo;
    }

    public long getCurrentSsn() {
        return currentSsn;
    }

    public void setCurrentSsn(long currentSsn) {
        this.currentSsn = currentSsn;
    }

    public boolean getUpdateDictionaries() {
        return updateDictionaries;
    }

    public void setUpdateDictionaries(boolean updateDictionaries) {
        this.updateDictionaries = updateDictionaries;
    }

    public boolean getSaveArchivedObjects() {
        return saveArchivedObjects;
    }

    public void setSaveArchivedObjects(boolean saveArchivedObjects) {
        this.saveArchivedObjects = saveArchivedObjects;
    }

    public boolean getUpdateOnlyRegional() {
        return updateOnlyRegional;
    }

    public void setUpdateOnlyRegional(boolean updateOnlyRegional) {
        this.updateOnlyRegional = updateOnlyRegional;
    }

    public List<String> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(List<String> selectedServices) {
        this.selectedServices = selectedServices;
    }

    public List<String> getSelectedOrgs() {
        return selectedOrgs;
    }

    public void setSelectedOrgs(List<String> selectedOrgs) {
        this.selectedOrgs = selectedOrgs;
    }
}
