package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.RguMoscowTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class RguTransformerFactoryFactory {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactoryFactory.class);

    private static Map<String, RguTransformerFactory> transformerFactoryMap = new HashMap<String, RguTransformerFactory>();

    static {
        transformerFactoryMap.put("moscow", RguMoscowTransformerFactory.getFactory());
    }

    private static sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactoryFactory rguTransformerFactoryFactory = new sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactoryFactory();

    private RguTransformerFactoryFactory() {
    }

    public static sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactoryFactory getRguTransformerFactoryFactory() {
        return rguTransformerFactoryFactory;
    }

    public RguTransformerFactory getTransformerFactory(@Nonnull String projectCode) {
        return transformerFactoryMap.get(projectCode.toLowerCase());
    }
}
