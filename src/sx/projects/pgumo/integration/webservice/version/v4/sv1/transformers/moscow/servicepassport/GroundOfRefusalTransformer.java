package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class GroundOfRefusalTransformer<T extends GroundOfRefusal> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(GroundOfRefusalTransformer.class);

    public GroundOfRefusalTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }


    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T groundOfRefusal) throws RguTransformationException {
        return RguTransformationUtils.parseRef(groundOfRefusal.getId()).id;
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T groundOfRefusal) {
        return groundOfRefusal.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T groundOfRefusal, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("title", groundOfRefusal.getTitle());
        map.put("description", groundOfRefusal.getDescription());
        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T groundOfRefusal, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return createSXObj(groundOfRefusal, "egGroundOfRefusal", callback, true);
    }
}
