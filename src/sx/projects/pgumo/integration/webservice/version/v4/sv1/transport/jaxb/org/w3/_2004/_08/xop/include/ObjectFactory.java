package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.w3._2004._08.xop.include package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Include_QNAME = new QName("http://www.w3.org/2004/08/xop/include", "Include");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.w3._2004._08.xop.include
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include createInclude() {
        return new sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2004/08/xop/include", name = "Include")
    public JAXBElement<sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include> createInclude(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include value) {
        return new JAXBElement<sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.org.w3._2004._08.xop.include.Include>(_Include_QNAME, Include.class, null, value);
    }

}
