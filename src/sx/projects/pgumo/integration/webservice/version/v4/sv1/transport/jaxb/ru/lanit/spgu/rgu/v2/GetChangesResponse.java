package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;
import java.util.ArrayList;


/**
 * <p>Java class for anonymous complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectRefList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="currentSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
 *                   &lt;element name="list" form="unqualified">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ObjectRef" maxOccurs="unbounded" form="unqualified">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="objectType" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
 *                                       &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
 *                                       &lt;element name="objectSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
 *                                       &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectRefList"
})
@XmlRootElement(name = "getChangesResponse")
public class GetChangesResponse {

    @XmlElement(name = "ObjectRefList", required = true)
    protected ObjectRefList objectRefList;

    /**
     * Gets the value of the objectRefList property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList }
     */
    public ObjectRefList getObjectRefList() {
        return objectRefList;
    }

    /**
     * Sets the value of the objectRefList property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList }
     */
    public void setObjectRefList(ObjectRefList value) {
        this.objectRefList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="currentSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
     *         &lt;element name="list" form="unqualified">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ObjectRef" maxOccurs="unbounded" form="unqualified">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="objectType" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
     *                             &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
     *                             &lt;element name="objectSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
     *                             &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "currentSsn",
            "list"
    })
    public static class ObjectRefList {

        @XmlElement(namespace = "", required = true)
        protected BigInteger currentSsn;
        @XmlElement(namespace = "", required = true)
        protected List list;

        /**
         * Gets the value of the currentSsn property.
         *
         * @return possible object is
         * {@link java.math.BigInteger }
         */
        public BigInteger getCurrentSsn() {
            return currentSsn;
        }

        /**
         * Sets the value of the currentSsn property.
         *
         * @param value allowed object is
         *              {@link java.math.BigInteger }
         */
        public void setCurrentSsn(BigInteger value) {
            this.currentSsn = value;
        }

        /**
         * Gets the value of the list property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List }
         */
        public List getList() {
            return list;
        }

        /**
         * Sets the value of the list property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List }
         */
        public void setList(List value) {
            this.list = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ObjectRef" maxOccurs="unbounded" form="unqualified">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="objectType" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
         *                   &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
         *                   &lt;element name="objectSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
         *                   &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "objectRef"
        })
        public static class List {

            @XmlElement(name = "ObjectRef", namespace = "", required = true)
            protected java.util.List<ObjectRef> objectRef;

            /**
             * Gets the value of the objectRef property.
             * <p/>
             * <p/>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the objectRef property.
             * <p/>
             * <p/>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getObjectRef().add(newItem);
             * </pre>
             * <p/>
             * <p/>
             * <p/>
             * Objects of the following type(s) are allowed in the list
             * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List.ObjectRef }
             */
            public java.util.List<ObjectRef> getObjectRef() {
                if (objectRef == null) {
                    objectRef = new ArrayList<ObjectRef>();
                }
                return this.objectRef;
            }


            /**
             * <p>Java class for anonymous complex type.
             * <p/>
             * <p>The following schema fragment specifies the expected content contained within this class.
             * <p/>
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="objectType" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
             *         &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
             *         &lt;element name="objectSsn" type="{http://www.w3.org/2001/XMLSchema}integer" form="unqualified"/>
             *         &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}NCName" form="unqualified"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "objectType",
                    "objectId",
                    "objectSsn",
                    "currentStatus"
            })
            public static class ObjectRef {

                @XmlElement(namespace = "", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "NCName")
                protected String objectType;
                @XmlElement(namespace = "", required = true)
                protected BigInteger objectId;
                @XmlElement(namespace = "", required = true)
                protected BigInteger objectSsn;
                @XmlElement(namespace = "", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "NCName")
                protected String currentStatus;

                /**
                 * Gets the value of the objectType property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getObjectType() {
                    return objectType;
                }

                /**
                 * Sets the value of the objectType property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setObjectType(String value) {
                    this.objectType = value;
                }

                /**
                 * Gets the value of the objectId property.
                 *
                 * @return possible object is
                 * {@link java.math.BigInteger }
                 */
                public BigInteger getObjectId() {
                    return objectId;
                }

                /**
                 * Sets the value of the objectId property.
                 *
                 * @param value allowed object is
                 *              {@link java.math.BigInteger }
                 */
                public void setObjectId(BigInteger value) {
                    this.objectId = value;
                }

                /**
                 * Gets the value of the objectSsn property.
                 *
                 * @return possible object is
                 * {@link java.math.BigInteger }
                 */
                public BigInteger getObjectSsn() {
                    return objectSsn;
                }

                /**
                 * Sets the value of the objectSsn property.
                 *
                 * @param value allowed object is
                 *              {@link java.math.BigInteger }
                 */
                public void setObjectSsn(BigInteger value) {
                    this.objectSsn = value;
                }

                /**
                 * Gets the value of the currentStatus property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getCurrentStatus() {
                    return currentStatus;
                }

                /**
                 * Sets the value of the currentStatus property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setCurrentStatus(String value) {
                    this.currentStatus = value;
                }

            }

        }

    }

}
