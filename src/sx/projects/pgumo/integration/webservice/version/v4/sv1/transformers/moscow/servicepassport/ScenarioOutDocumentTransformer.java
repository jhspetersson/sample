package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScenarioOutDocumentTransformer<T extends GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(ScenarioOutDocumentTransformer.class);

    public ScenarioOutDocumentTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T outDoc) throws RguTransformationException {
        return RguTransformationUtils.parseRef(outDoc.getWorkDocument().getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T outDoc) {
        return outDoc.getWorkDocument().getId();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T outDoc, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(getRguIdAttrName(), getObjRguId(outDoc));
        map.put("guid", getObjRguId(outDoc));
        //map.put("title", getObjTitle(outDoc));
        map.put("cnt", outDoc.getQuantity());

        if (outDoc.getDocumentType() != null) {
            GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.DocumentType documentType = outDoc.getDocumentType();
            String ref = documentType.getRef();
            RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
            SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
            if (sxId != null) {
                map.put("type", sxId);
            }
        }

        if (outDoc.getIssueTypes() != null) {
            GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes issueTypes = outDoc.getIssueTypes();
            List<GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes.DocumentIssueType> documentIssueTypes = issueTypes.getDocumentIssueType();
            List<SXId> out = new ArrayList<SXId>();
            for (GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes.DocumentIssueType documentIssueType : documentIssueTypes) {
                String ref = documentIssueType.getRef();
                RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
                SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
                if (sxId != null) {
                    out.add(sxId);
                }
            }
            map.put("out", out);
        }
        map.put("document", getDocument(outDoc));
        return map;
    }

    private SXId getDocument(T outDoc) throws RguTransformationException {
        GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.WorkDocument workDocument = outDoc.getWorkDocument();
        if (workDocument == null) {
            return null;
        }
        String ref = workDocument.getId();
        if (Strings.isNullOrEmpty(ref)) {
            return null;
        }
        return RguTransformationUtils.downloadFileAndCreateCmsFile(ref, "egScenarioOutDocuments", "document");
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T offDoc, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(offDoc, "egScenarioOutDocuments", callback);
    }


}
