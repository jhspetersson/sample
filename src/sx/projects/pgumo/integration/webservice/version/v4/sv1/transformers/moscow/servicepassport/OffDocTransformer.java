package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class OffDocTransformer<T extends OffDoc> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(OffDocTransformer.class);

    public OffDocTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T offDoc) throws RguTransformationException {
        return RguTransformationUtils.parseRef(offDoc.getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "guid";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T offDoc) {
        return offDoc.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T offDoc, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("guid", getObjRguId(offDoc));
        map.put("title", offDoc.getTitle());
        map.put("date", RguTransformationUtils.convertDate(offDoc.getApprovalDate()));
        OffDoc.Document document = offDoc.getDocument();
        if (document != null) {
            map.put("file_LawDoc", RguTransformationUtils.createCmsFile(document.getContent(), "egNPA", "file_LawDoc"));
        } else {
            log.warn("�������� ��� �� ��������, ID " + getObjRguId(offDoc));
        }
        map.put("npaType", offDoc.getDocumentClass() == null ? null : transformObj(offDoc.getDocumentClass(), callback).getId());
        map.put("url", offDoc.getUrl());
        map.put("idRgu", getObjRguId(offDoc));
        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T offDoc, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(offDoc, "egNPA", callback);
    }

}
