package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetListDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetListUserDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetUserDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.lanit.spgu.rgu.v2 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "size");
    private final static QName _GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "blobData");
    private final static QName _GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "fileName");
    private final static QName _GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "fileType");
    private final static QName _GetRStateStructureResponseRStateStructureOfficesOfficeTerritoriesAddress2Territory_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "Address2Territory");
    private final static QName _GetPsPassportResponsePsPassportAuditsAuditOffDocsOffDoc_QNAME = new QName("http://spgu.lanit.ru/rgu/v2.5", "offDoc");

    private final static QName _MessageClass_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "MessageClass");
    private final static QName _GetRevokationList_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getRevokationList");
    private final static QName _TestMsg_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "TestMsg");
    private final static QName _MessageData_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "MessageData");
    private final static QName _Request_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Request");
    private final static QName _GetListUserDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getListUserDictionary");
    private final static QName _PsPassportPreview_QNAME = new QName("http://rgu.lanit.ru/rev111111", "psPassportPreview");
    private final static QName _GetFeedBack_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getFeedBack");
    private final static QName _ExchangeType_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "ExchangeType");
    private final static QName _NodeId_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "NodeId");
    private final static QName _Sender_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Sender");
    private final static QName _RequestIdRef_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "RequestIdRef");
    private final static QName _Include_QNAME = new QName("http://www.w3.org/2004/08/xop/include", "Include");
    private final static QName _Date_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Date");
    private final static QName _Header_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Header");
    private final static QName _TypeCode_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "TypeCode");
    private final static QName _GetEPGUSectionDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getEPGUSectionDictionary");
    private final static QName _MessageId_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "MessageId");
    private final static QName _UpdateStatus_QNAME = new QName("http://rgu.lanit.ru/rev111111", "updateStatus");
    private final static QName _ServiceCode_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "ServiceCode");
    private final static QName _Recipient_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Recipient");
    private final static QName _CaseNumber_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "CaseNumber");
    private final static QName _OriginRequestIdRef_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "OriginRequestIdRef");
    private final static QName _DigestValue_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "DigestValue");
    private final static QName _GetRStateStructure_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getRStateStructure");
    private final static QName _Response_QNAME = new QName("http://rgu.lanit.ru/rev111111", "response");
    private final static QName _Originator_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Originator");
    private final static QName _AppDocument_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "AppDocument");
    private final static QName _Message_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Message");
    private final static QName _BinaryData_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "BinaryData");
    private final static QName _AppData_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "AppData");
    private final static QName _Status_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Status");
    private final static QName _TimeStamp_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "TimeStamp");
    private final static QName _RegisterFeedBack_QNAME = new QName("http://rgu.lanit.ru/rev111111", "registerFeedBack");
    private final static QName _GetPsPassport_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getPsPassport");
    private final static QName _BaseMessage_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "BaseMessage");
    private final static QName _Reference_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "Reference");
    private final static QName _GetUserDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getUserDictionary");
    private final static QName _RequestCode_QNAME = new QName("http://smev.gosuslugi.ru/rev111111", "RequestCode");
    private final static QName _GetChanges_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getChanges");
    private final static QName _GetDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getDictionary");
    private final static QName _GetListDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getListDictionary");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.lanit.spgu.rgu.v2
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document }
     */
    public GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document createGetPsPassportResponsePsPassportTkmvTkmvDocTextDocument() {
        return new GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases.NoResponseCase }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases.NoResponseCase createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealPauseReasonsAppealPauseReasonNoResponseCasesNoResponseCase() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases.NoResponseCase();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Participants.Paticipant.Organization }
     */
    public GetPsPassportResponse.PsPassport.Participants.Paticipant.Organization createGetPsPassportResponsePsPassportParticipantsPaticipantOrganization() {
        return new GetPsPassportResponse.PsPassport.Participants.Paticipant.Organization();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments createGetPsPassportResponsePsPassportWorkDocuments() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions.TargetInstruction }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions.TargetInstruction createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetInstructionsTargetInstruction() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions.TargetInstruction();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator }
     */
    public GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator createGetPsPassportResponsePsPassportQualityIndicatorsQualityIndicator() {
        return new GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.PlacesRequirements }
     */
    public GetPsPassportResponse.PsPassport.PlacesRequirements createGetPsPassportResponsePsPassportPlacesRequirements() {
        return new GetPsPassportResponse.PsPassport.PlacesRequirements();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.WorkDocument }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.WorkDocument createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocumentsTargetInDocumentsWorkDocument() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.WorkDocument();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Keywords }
     */
    public GetPsPassportResponse.PsPassport.Keywords createGetPsPassportResponsePsPassportKeywords() {
        return new GetPsPassportResponse.PsPassport.Keywords();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceOffDoc() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioLegalActionsLegalAction() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocuments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc }
     */
    public GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDoc() {
        return new GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Category }
     */
    public GetPsPassportResponse.PsPassport.Category createGetPsPassportResponsePsPassportCategory() {
        return new GetPsPassportResponse.PsPassport.Category();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions createGetPsPassportResponsePsPassportAdmProceduresAdmProcedureAdmActions() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes.ReceiptType }
     */
    public GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes.ReceiptType createGetPsPassportResponsePsPassportQualityIndicatorsQualityIndicatorReceiptTypesReceiptType() {
        return new GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes.ReceiptType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office }
     */
    public GetRStateStructureResponse.RStateStructure.Offices.Office createGetRStateStructureResponseRStateStructureOfficesOffice() {
        return new GetRStateStructureResponse.RStateStructure.Offices.Office();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List.ObjectRef }
     */
    public GetChangesResponse.ObjectRefList.List.ObjectRef createGetChangesResponseObjectRefListListObjectRef() {
        return new GetChangesResponse.ObjectRefList.List.ObjectRef();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Participants }
     */
    public GetPsPassportResponse.PsPassport.Participants createGetPsPassportResponsePsPassportParticipants() {
        return new GetPsPassportResponse.PsPassport.Participants();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInformationSystemsInformationSystem() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.WorkDocument }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.WorkDocument createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocumentsScenarioOutDocumentsWorkDocument() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.WorkDocument();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType }
     */
    public GetRStateStructureResponse.RStateStructure.SubordinationType createGetRStateStructureResponseRStateStructureSubordinationType() {
        return new GetRStateStructureResponse.RStateStructure.SubordinationType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices }
     */
    public GetRStateStructureResponse.RStateStructure.Offices createGetRStateStructureResponseRStateStructureOffices() {
        return new GetRStateStructureResponse.RStateStructure.Offices();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory.RecipientCategory }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory.RecipientCategory createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetRecipientCategoriesService2RRecipientCategoryRecipientCategory() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory.RecipientCategory();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplate() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures.AdmProcedure }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures.AdmProcedure createGetPsPassportResponsePsPassportServicesPassportServicesServiceAdmproceduresAdmProcedure() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures.AdmProcedure();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.ExpertComments.PsPassportExpertComments }
     */
    public GetPsPassportResponse.PsPassport.ExpertComments.PsPassportExpertComments createGetPsPassportResponsePsPassportExpertCommentsPsPassportExpertComments() {
        return new GetPsPassportResponse.PsPassport.ExpertComments.PsPassportExpertComments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse }
     */
    public GetPsPassportResponse createGetPsPassportResponse() {
        return new GetPsPassportResponse();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments.ServiceExpertComments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments.ServiceExpertComments createGetPsPassportResponsePsPassportServicesPassportServicesServiceExpertCommentsServiceExpertComments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments.ServiceExpertComments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Tkmv }
     */
    public GetPsPassportResponse.PsPassport.Tkmv createGetPsPassportResponsePsPassportTkmv() {
        return new GetPsPassportResponse.PsPassport.Tkmv();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText }
     */
    public GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText createGetPsPassportResponsePsPassportTkmvTkmvDocText() {
        return new GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects createGetPsPassportResponsePsPassportAppealSubjects() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address }
     */
    public GetRStateStructureResponse.RStateStructure.Offices.Office.Address createGetRStateStructureResponseRStateStructureOfficesOfficeAddress() {
        return new GetRStateStructureResponse.RStateStructure.Offices.Office.Address();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes.DocumentIssueType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes.DocumentIssueType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocumentsScenarioOutDocumentsIssueTypesDocumentIssueType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes.DocumentIssueType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList }
     */
    public GetChangesResponse.ObjectRefList createGetChangesResponseObjectRefList() {
        return new GetChangesResponse.ObjectRefList();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits.Audit }
     */
    public GetPsPassportResponse.PsPassport.Audits.Audit createGetPsPassportResponsePsPassportAuditsAudit() {
        return new GetPsPassportResponse.PsPassport.Audits.Audit();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList createGetPsPassportResponsePsPassportAdmProceduresAdmProcedureDecidingCriteriaList() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal.GroundType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal.GroundType createGetPsPassportResponsePsPassportServicesPassportServicesServiceGroundsOfRefusalGroundOfRefusalGroundType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal.GroundType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms createGetPsPassportResponsePsPassportServicesPassportServicesServiceCommunicationForms() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.DocumentType }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.DocumentType createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentDocumentType() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.DocumentType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetInstructions() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetInstructions();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service createGetPsPassportResponsePsPassportServicesPassportServicesService() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealReason() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements.PlacesRequirement }
     */
    public GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements.PlacesRequirement createGetPsPassportResponsePsPassportPlacesRequirementsPsPassport2RPlacesRequirementsPlacesRequirement() {
        return new GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements.PlacesRequirement();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInformationSystems() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Type }
     */
    public GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Type createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocType() {
        return new GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Type();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasons() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure createGetPsPassportResponsePsPassportAdmProceduresAdmProcedure() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Participants.Paticipant }
     */
    public GetPsPassportResponse.PsPassport.Participants.Paticipant createGetPsPassportResponsePsPassportParticipantsPaticipant() {
        return new GetPsPassportResponse.PsPassport.Participants.Paticipant();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults.AppealResult }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults.AppealResult createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealReasonAppealResultsAppealResult() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults.AppealResult();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services }
     */
    public GetPsPassportResponse.PsPassport.Services createGetPsPassportResponsePsPassportServices() {
        return new GetPsPassportResponse.PsPassport.Services();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc.OffDoc }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc.OffDoc createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceOffDocOffDoc() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc.OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm.CommunicationFormRef }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm.CommunicationFormRef createGetPsPassportResponsePsPassportServicesPassportServicesServiceCommunicationFormsCommunicationFormCommunicationFormRef() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm.CommunicationFormRef();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse }
     */
    public GetChangesResponse createGetChangesResponse() {
        return new GetChangesResponse();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealReasonAppealResults() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.AppealResults();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits }
     */
    public GetPsPassportResponse.PsPassport.Audits createGetPsPassportResponsePsPassportAudits() {
        return new GetPsPassportResponse.PsPassport.Audits();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem }
     */
    public GetRStateStructureResponse.RStateStructure.Emblem createGetRStateStructureResponseRStateStructureEmblem() {
        return new GetRStateStructureResponse.RStateStructure.Emblem();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.DecidingCriteriaList.DecidingCriteria }
     */
    public GetPsPassportResponse.PsPassport.DecidingCriteriaList.DecidingCriteria createGetPsPassportResponsePsPassportDecidingCriteriaListDecidingCriteria() {
        return new GetPsPassportResponse.PsPassport.DecidingCriteriaList.DecidingCriteria();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTarget() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service.LifeEvent }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service.LifeEvent createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetLifeEventsLifeEvent2ServiceLifeEvent() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service.LifeEvent();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.ScenarioType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.ScenarioType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioScenarioType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.ScenarioType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport }
     */
    public GetPsPassportResponse.PsPassport createGetPsPassportResponsePsPassport() {
        return new GetPsPassportResponse.PsPassport();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.DocumentType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.DocumentType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocumentsTargetInDocumentsDocumentType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.DocumentType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes }
     */
    public GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes createGetPsPassportResponsePsPassportParticipantsPaticipantParticipantTypes() {
        return new GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory }
     */
    public GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory createGetRStateStructureResponseRStateStructureOfficesOfficeTerritoriesAddress2Territory() {
        return new GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocumentsTargetInDocuments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent }
     */
    public GetRStateStructureResponse.RStateStructure.Parent createGetRStateStructureResponseRStateStructureParent() {
        return new GetRStateStructureResponse.RStateStructure.Parent();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts }
     */
    public GetRStateStructureResponse.RStateStructure.Contacts createGetRStateStructureResponseRStateStructureContacts() {
        return new GetRStateStructureResponse.RStateStructure.Contacts();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.DocumentType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.DocumentType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocumentsScenarioOutDocumentsDocumentType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.DocumentType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema createGetPsPassportResponsePsPassportServicesPassportServicesServiceBlockSchema() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.StatusInfo }
     */
    public GetPsPassportResponse.PsPassport.StatusInfo createGetPsPassportResponsePsPassportStatusInfo() {
        return new GetPsPassportResponse.PsPassport.StatusInfo();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures createGetPsPassportResponsePsPassportAdmProcedures() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm createGetPsPassportResponsePsPassportServicesPassportServicesServiceCommunicationFormsCommunicationForm() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.ResponsibleOrganization }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.ResponsibleOrganization createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealReasonResponsibleOrganization() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealReason.ResponsibleOrganization();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Tkmv.OffDoc }
     */
    public GetPsPassportResponse.PsPassport.Tkmv.OffDoc createGetPsPassportResponsePsPassportTkmvOffDoc() {
        return new GetPsPassportResponse.PsPassport.Tkmv.OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetLifeEvents() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories }
     */
    public GetRStateStructureResponse.RStateStructure.Offices.Office.Territories createGetRStateStructureResponseRStateStructureOfficesOfficeTerritories() {
        return new GetRStateStructureResponse.RStateStructure.Offices.Office.Territories();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs }
     */
    public GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs createGetPsPassportResponsePsPassportAuditsAuditOffDocs() {
        return new GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal createGetPsPassportResponsePsPassportServicesPassportServicesServiceGroundsOfRefusalGroundOfRefusal() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList.DecidingCriteria }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList.DecidingCriteria createGetPsPassportResponsePsPassportAdmProceduresAdmProcedureDecidingCriteriaListDecidingCriteria() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.DecidingCriteriaList.DecidingCriteria();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioLegalActions() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo }
     */
    public GetRStateStructureResponse.RStateStructure.StatusInfo createGetRStateStructureResponseRStateStructureStatusInfo() {
        return new GetRStateStructureResponse.RStateStructure.StatusInfo();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenarios() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact }
     */
    public GetRStateStructureResponse.RStateStructure.Contacts.Contact createGetRStateStructureResponseRStateStructureContactsContact() {
        return new GetRStateStructureResponse.RStateStructure.Contacts.Contact();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits.Audit.AuditType }
     */
    public GetPsPassportResponse.PsPassport.Audits.Audit.AuditType createGetPsPassportResponsePsPassportAuditsAuditAuditType() {
        return new GetPsPassportResponse.PsPassport.Audits.Audit.AuditType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.InteragencyTypes.InteragencyType }
     */
    public GetPsPassportResponse.PsPassport.InteragencyTypes.InteragencyType createGetPsPassportResponsePsPassportInteragencyTypesInteragencyType() {
        return new GetPsPassportResponse.PsPassport.InteragencyTypes.InteragencyType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments createGetPsPassportResponsePsPassportServicesPassportServicesServiceExpertComments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ExpertComments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs }
     */
    public GetPsPassportResponse.PsPassport.OffDocs createGetPsPassportResponsePsPassportOffDocs() {
        return new GetPsPassportResponse.PsPassport.OffDocs();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory }
     */
    public GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory createGetRStateStructureResponseRStateStructureOfficesOfficeTerritoriesAddress2TerritoryTerritory() {
        return new GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocuments() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealPauseReasons() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document }
     */
    public GetRStateStructureResponse.RStateStructure.Emblem.Document createGetRStateStructureResponseRStateStructureEmblemDocument() {
        return new GetRStateStructureResponse.RStateStructure.Emblem.Document();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction.ActionType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction.ActionType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioLegalActionsLegalActionActionType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.LegalActions.LegalAction.ActionType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document }
     */
    public GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocument() {
        return new GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions.AdmAction }
     */
    public GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions.AdmAction createGetPsPassportResponsePsPassportAdmProceduresAdmProcedureAdmActionsAdmAction() {
        return new GetPsPassportResponse.PsPassport.AdmProcedures.AdmProcedure.AdmActions.AdmAction();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc }
     */
    public GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc createGetPsPassportResponsePsPassportAuditsAuditOffDocsOffDoc() {
        return new GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.PassportClass }
     */
    public GetPsPassportResponse.PsPassport.PassportClass createGetPsPassportResponsePsPassportPassportClass() {
        return new GetPsPassportResponse.PsPassport.PassportClass();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List }
     */
    public GetChangesResponse.ObjectRefList.List createGetChangesResponseObjectRefListList() {
        return new GetChangesResponse.ObjectRefList.List();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetLifeEventsLifeEvent2Service() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.LifeEvents.LifeEvent2Service();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargets() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Tkmv.OffDoc.DocumentClass }
     */
    public GetPsPassportResponse.PsPassport.Tkmv.OffDoc.DocumentClass createGetPsPassportResponsePsPassportTkmvOffDocDocumentClass() {
        return new GetPsPassportResponse.PsPassport.Tkmv.OffDoc.DocumentClass();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc }
     */
    public GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDoc() {
        return new GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Sample }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Sample createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentSample() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Sample();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Reglament.OffDoc }
     */
    public GetPsPassportResponse.PsPassport.Reglament.OffDoc createGetPsPassportResponsePsPassportReglamentOffDoc() {
        return new GetPsPassportResponse.PsPassport.Reglament.OffDoc();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.RStateStructure }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.RStateStructure createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInformationSystemsInformationSystemRStateStructure() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.RStateStructure();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin createFin() {
        return new sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.QualityIndicators }
     */
    public GetPsPassportResponse.PsPassport.QualityIndicators createGetPsPassportResponsePsPassportQualityIndicators() {
        return new GetPsPassportResponse.PsPassport.QualityIndicators();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.DecidingCriteriaList }
     */
    public GetPsPassportResponse.PsPassport.DecidingCriteriaList createGetPsPassportResponsePsPassportDecidingCriteriaList() {
        return new GetPsPassportResponse.PsPassport.DecidingCriteriaList();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.DocumentClass }
     */
    public GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.DocumentClass createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocumentClass() {
        return new GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.DocumentClass();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AdministrativeLevel }
     */
    public GetPsPassportResponse.PsPassport.AdministrativeLevel createGetPsPassportResponsePsPassportAdministrativeLevel() {
        return new GetPsPassportResponse.PsPassport.AdministrativeLevel();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Reglament }
     */
    public GetPsPassportResponse.PsPassport.Reglament createGetPsPassportResponsePsPassportReglament() {
        return new GetPsPassportResponse.PsPassport.Reglament();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel }
     */
    public GetRStateStructureResponse.RStateStructure.AdministrativeLevel createGetRStateStructureResponseRStateStructureAdministrativeLevel() {
        return new GetRStateStructureResponse.RStateStructure.AdministrativeLevel();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure }
     */
    public GetRStateStructureResponse.RStateStructure createGetRStateStructureResponseRStateStructure() {
        return new GetRStateStructureResponse.RStateStructure();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenario() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocumentsTargetInDocumentsUseTypes() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealPauseReasonsAppealPauseReason() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal createGetPsPassportResponsePsPassportServicesPassportServicesServiceGroundsOfRefusal() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument }
     */
    public GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocument() {
        return new GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubject() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.InteragencyTypes }
     */
    public GetPsPassportResponse.PsPassport.InteragencyTypes createGetPsPassportResponsePsPassportInteragencyTypes() {
        return new GetPsPassportResponse.PsPassport.InteragencyTypes();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes.DocumentUseType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes.DocumentUseType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocumentsTargetInDocumentsUseTypesDocumentUseType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes.DocumentUseType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices createGetPsPassportResponsePsPassportServicesPassportServices() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocumentsScenarioOutDocumentsIssueTypes() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.IssueTypes();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.ExpertComments }
     */
    public GetPsPassportResponse.PsPassport.ExpertComments createGetPsPassportResponsePsPassportExpertComments() {
        return new GetPsPassportResponse.PsPassport.ExpertComments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType }
     */
    public GetRStateStructureResponse.RStateStructure.StateStructureType createGetRStateStructureResponseRStateStructureStateStructureType() {
        return new GetRStateStructureResponse.RStateStructure.StateStructureType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes.ParticipantType }
     */
    public GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes.ParticipantType createGetPsPassportResponsePsPassportParticipantsPaticipantParticipantTypesParticipantType() {
        return new GetPsPassportResponse.PsPassport.Participants.Paticipant.ParticipantTypes.ParticipantType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInDocuments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetTargetScenariosTargetScenarioOutDocumentsScenarioOutDocuments() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.SystemType }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.SystemType createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetInformationSystemsInformationSystemSystemType() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InformationSystems.InformationSystem.SystemType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetRecipientCategoriesService2RRecipientCategory() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories.Service2RRecipientCategory();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements }
     */
    public GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements createGetPsPassportResponsePsPassportPlacesRequirementsPsPassport2RPlacesRequirements() {
        return new GetPsPassportResponse.PsPassport.PlacesRequirements.PsPassport2RPlacesRequirements();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.ResponsibleOrganization }
     */
    public GetPsPassportResponse.PsPassport.ResponsibleOrganization createGetPsPassportResponsePsPassportResponsibleOrganization() {
        return new GetPsPassportResponse.PsPassport.ResponsibleOrganization();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin.FinancialDetailsStructured }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin.FinancialDetailsStructured createFinFinancialDetailsStructured() {
        return new Fin.FinancialDetailsStructured();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes }
     */
    public GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes createGetPsPassportResponsePsPassportQualityIndicatorsQualityIndicatorReceiptTypes() {
        return new GetPsPassportResponse.PsPassport.QualityIndicators.QualityIndicator.ReceiptTypes();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases }
     */
    public GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases createGetPsPassportResponsePsPassportAppealSubjectsPretrialAppealSubjectAppealReasonsAppealPauseReasonsAppealPauseReasonNoResponseCases() {
        return new GetPsPassportResponse.PsPassport.AppealSubjects.PretrialAppealSubject.AppealReasons.AppealPauseReasons.AppealPauseReason.NoResponseCases();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse }
     */
    public GetRStateStructureResponse createGetRStateStructureResponse() {
        return new GetRStateStructureResponse();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures createGetPsPassportResponsePsPassportServicesPassportServicesServiceAdmprocedures() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Admprocedures();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories }
     */
    public GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories createGetPsPassportResponsePsPassportServicesPassportServicesServiceServiceTargetsServiceTargetRecipientCategories() {
        return new GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.RecipientCategories();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "size", scope = GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize_QNAME, String.class, GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "blobData", scope = GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class)
    public JAXBElement<byte[]> createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData(byte[] value) {
        return new JAXBElement<byte[]>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData_QNAME, byte[].class, GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileName", scope = GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName_QNAME, String.class, GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileType", scope = GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType_QNAME, String.class, GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "size", scope = GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportTkmvTkmvDocTextDocumentSize(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize_QNAME, String.class, GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "blobData", scope = GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class)
    public JAXBElement<byte[]> createGetPsPassportResponsePsPassportTkmvTkmvDocTextDocumentBlobData(byte[] value) {
        return new JAXBElement<byte[]>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData_QNAME, byte[].class, GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileName", scope = GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportTkmvTkmvDocTextDocumentFileName(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName_QNAME, String.class, GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileType", scope = GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportTkmvTkmvDocTextDocumentFileType(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType_QNAME, String.class, GetPsPassportResponse.PsPassport.Tkmv.TkmvDocText.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "Address2Territory", scope = GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.class)
    public JAXBElement<GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory> createGetRStateStructureResponseRStateStructureOfficesOfficeTerritoriesAddress2Territory(GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory value) {
        return new JAXBElement<GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory>(_GetRStateStructureResponseRStateStructureOfficesOfficeTerritoriesAddress2Territory_QNAME, GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.class, GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "offDoc", scope = GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.class)
    public JAXBElement<GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc> createGetPsPassportResponsePsPassportAuditsAuditOffDocsOffDoc(GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc value) {
        return new JAXBElement<GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc>(_GetPsPassportResponsePsPassportAuditsAuditOffDocsOffDoc_QNAME, GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.OffDoc.class, GetPsPassportResponse.PsPassport.Audits.Audit.OffDocs.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "size", scope = GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocumentSize(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize_QNAME, String.class, GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "blobData", scope = GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class)
    public JAXBElement<byte[]> createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocumentBlobData(byte[] value) {
        return new JAXBElement<byte[]>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData_QNAME, byte[].class, GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileName", scope = GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocumentFileName(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName_QNAME, String.class, GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileType", scope = GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportOffDocsPsPassport2OffDocOffDocDocumentFileType(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType_QNAME, String.class, GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.Document.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "size", scope = GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportServicesPassportServicesServiceBlockSchemaSize(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateSize_QNAME, String.class, GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "blobData", scope = GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class)
    public JAXBElement<byte[]> createGetPsPassportResponsePsPassportServicesPassportServicesServiceBlockSchemaBlobData(byte[] value) {
        return new JAXBElement<byte[]>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateBlobData_QNAME, byte[].class, GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileName", scope = GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportServicesPassportServicesServiceBlockSchemaFileName(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileName_QNAME, String.class, GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://spgu.lanit.ru/rgu/v2.5", name = "fileType", scope = GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class)
    public JAXBElement<String> createGetPsPassportResponsePsPassportServicesPassportServicesServiceBlockSchemaFileType(String value) {
        return new JAXBElement<String>(_GetPsPassportResponsePsPassportWorkDocumentsPassportWorkDocumentsWorkDocumentTemplateFileType_QNAME, String.class, GetPsPassportResponse.PsPassport.Services.PassportServices.Service.BlockSchema.class, value);
    }


    /**
     * Create an instance of {@link GetListUserDictionary }
     */
    public GetListUserDictionary createGetListUserDictionary() {
        return new GetListUserDictionary();
    }

    /**
     * Create an instance of {@link GetUserDictionary }
     */
    public GetUserDictionary createGetUserDictionary() {
        return new GetUserDictionary();
    }

    /**
     * Create an instance of {@link GetListDictionary }
     */
    public GetListDictionary createGetListDictionary() {
        return new GetListDictionary();
    }

    public GetListDictionaryResponse createGetListDictionaryResponse() {
        return new GetListDictionaryResponse();
    }

    /**
     * Create an instance of {@link GetDictionary }
     */
    public GetDictionary createGetDictionary() {
        return new GetDictionary();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListUserDictionary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getListUserDictionary")
    public JAXBElement<GetListUserDictionary> createGetListUserDictionary(GetListUserDictionary value) {
        return new JAXBElement<GetListUserDictionary>(_GetListUserDictionary_QNAME, GetListUserDictionary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDictionary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getUserDictionary")
    public JAXBElement<GetUserDictionary> createGetUserDictionary(GetUserDictionary value) {
        return new JAXBElement<GetUserDictionary>(_GetUserDictionary_QNAME, GetUserDictionary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDictionary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getDictionary")
    public JAXBElement<GetDictionary> createGetDictionary(GetDictionary value) {
        return new JAXBElement<GetDictionary>(_GetDictionary_QNAME, GetDictionary.class, null, value);
    }

    public GetDictionaryResponse createGetDictionaryResponse() {
        return new GetDictionaryResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListDictionary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getListDictionary")
    public JAXBElement<GetListDictionary> createGetListDictionary(GetListDictionary value) {
        return new JAXBElement<GetListDictionary>(_GetListDictionary_QNAME, GetListDictionary.class, null, value);
    }

    public GetRevokationListResponse createGetRevokationListResponse() {
        return new GetRevokationListResponse();
    }
}
