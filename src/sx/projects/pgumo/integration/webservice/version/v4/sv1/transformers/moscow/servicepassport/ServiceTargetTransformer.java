package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.common.SXUtils;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.datastore.params.SXUpdateObjParams;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceTargetTransformer<T extends ServiceTarget> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(ServiceTargetTransformer.class);

    public ServiceTargetTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }


    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T serviceTarget) throws RguTransformationException {
        return RguTransformationUtils.parseRef(serviceTarget.getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "guid";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T serviceTarget) {
        return serviceTarget.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T serviceTarget, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("guid", getObjRguId(serviceTarget));
        map.put("idRgu", getObjRguId(serviceTarget));
        map.put("title", getObjTitle(serviceTarget));
        map.put("shortTitle", serviceTarget.getShortTitle());
        map.put("description", serviceTarget.getDescription());
        map.put("free", serviceTarget.getFree().equals("true"));
        map.put("isInteragency", serviceTarget.getIsInteragency().equals("true"));
        map.put("isNotDeclarative", SXUtils.isTrue(serviceTarget.getIsNotDeclarative()));
        map.put("url", serviceTarget.getUrl());
        map.put("term", serviceTarget.getTerm());
        map.put("maxTermQueue", serviceTarget.getMaxTermQueue());
        map.put("requestRegistration", serviceTarget.getRequestRegistration());
        map.put("scenario", getServiceTargetsScenarioIds(serviceTarget, callback));
        map.put("egPayment", getPayments(serviceTarget, callback));

        ServiceTarget.RecipientCategories recipientCategories = serviceTarget.getRecipientCategories();
        if (recipientCategories != null) {
            ServiceTarget.RecipientCategories.Service2RRecipientCategory service2RRecipientCategory = recipientCategories.getService2RRecipientCategory();
            if (service2RRecipientCategory != null) {
                ServiceTarget.RecipientCategories.Service2RRecipientCategory.RecipientCategory recipientCategory = service2RRecipientCategory.getRecipientCategory();
                if (recipientCategory != null) {
                    List<SXId> result = new ArrayList<SXId>();
                    String ref = recipientCategory.getRef();
                    RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
                    SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
                    if (sxId != null) {
                        result.add(sxId);
                        map.put("recipientCategory", result);
                    }
                }
            }
        }
        return map;
    }

    /**
     * ������������� �������� ���������
     *
     * @param serviceTarget   ���� ��������� - �� ������ �������
     * @param egServiceTarget ���� ��������� - SXID
     * @param callback        callback  @throws RguTransformationException
     */
    private void setInDocuments(T serviceTarget, SXId egServiceTarget, RguTransformerCallback callback) throws Exception {
        ServiceTarget.InDocuments inDocuments = serviceTarget.getInDocuments();
        if (inDocuments == null) {
            return;
        }
        List<ServiceTarget.InDocuments.TargetInDocuments> targetInDocumentsList = inDocuments.getTargetInDocuments();
        if (targetInDocumentsList == null) {
            return;
        }
        for (ServiceTarget.InDocuments.TargetInDocuments targetInDocuments : targetInDocumentsList) {
            SXObj documentObj = transformObj(targetInDocuments, callback);
            if (documentObj == null) {
                continue;
            }
            SXUpdateObjParams params = new SXUpdateObjParams(documentObj.getId());
            Map<String, Object> updateMap = new HashMap<String, Object>(1);
            updateMap.put("target", egServiceTarget);
            params.setUpdateReadOnlyAttr(true).setRunHandlers(false).setData(updateMap).updateObj();
        }
    }

    @Nonnull
    public List<SXId> getServiceTargetsScenarioIds(@Nonnull T serviceTarget, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        List<SXId> serviceTargetsScenarioIds = new ArrayList<SXId>(serviceTarget.getTargetScenarios().getTargetScenario().size());
        for (GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario serviceTargetScenario : serviceTarget.getTargetScenarios().getTargetScenario()) {
            serviceTargetsScenarioIds.add(transformObj(serviceTargetScenario, callback).getId());
        }
        return serviceTargetsScenarioIds;
    }

    @Nonnull
    public List<SXId> getPayments(@Nonnull T serviceTarget, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        List<SXId> payments = new ArrayList<SXId>();

        try {
            if (serviceTarget.getPayments() != null && serviceTarget.getPayments().getServicePayment() != null) {
                for (ServiceTarget.Payments.ServicePayment payment : serviceTarget.getPayments().getServicePayment()) {
                    payments.add(transformObj(payment, callback).getId());
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return payments;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T serviceTarget, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        SXObj egServiceTarget = transformation(serviceTarget, "egServiceTarget", callback);
        try {
            setInDocuments(serviceTarget, egServiceTarget.getId(), callback);
        } catch (Exception e) {
            throw new RguTransformationException(e.getMessage(), e);
        }
        return egServiceTarget;
    }
}
