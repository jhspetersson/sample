package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ServiceFaqTransformer<T extends GetPsPassportResponse.PsPassport.Services.PassportServices.Service.Faqs.Faq> extends RguAbstractTransformer<T> {

    public ServiceFaqTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    protected String getRguIdAttrName() {
        return "guid";
    }

    @Override
    public SXObj transform(@Nonnull T obj, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(obj, "egServiceFaq", callback);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T faq) throws RguTransformationException {
        return new BigInteger((new Integer(faq.getQuestion().hashCode())).toString());
    }

    @Nonnull
    @Override
    protected String getObjTitle(@Nonnull T faq) {
        return faq.getQuestion();
    }

    @Nonnull
    @Override
    protected Map<String, Object> prepareDataMap(@Nonnull T faq, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("question", faq.getQuestion());
        map.put("answer", faq.getAnswer());
        return map;
    }
}