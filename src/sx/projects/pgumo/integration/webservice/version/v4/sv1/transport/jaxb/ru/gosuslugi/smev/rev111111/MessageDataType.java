package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDocumentType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MessageDataType complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="MessageDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AppData" type="{http://smev.gosuslugi.ru/rev111111}AppDataType" minOccurs="0"/>
 *         &lt;element name="AppDocument" type="{http://smev.gosuslugi.ru/rev111111}AppDocumentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageDataType", propOrder = {
        "appData",
        "appDocument"
})
public class MessageDataType {

    @XmlElement(name = "AppData")
    protected AppDataType appData;
    @XmlElement(name = "AppDocument")
    protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDocumentType appDocument;

    /**
     * Gets the value of the appData property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDataType }
     */
    public AppDataType getAppData() {
        return appData;
    }

    /**
     * Sets the value of the appData property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDataType }
     */
    public void setAppData(AppDataType value) {
        this.appData = value;
    }

    /**
     * Gets the value of the appDocument property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDocumentType }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDocumentType getAppDocument() {
        return appDocument;
    }

    /**
     * Sets the value of the appDocument property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.AppDocumentType }
     */
    public void setAppDocument(AppDocumentType value) {
        this.appDocument = value;
    }

}
