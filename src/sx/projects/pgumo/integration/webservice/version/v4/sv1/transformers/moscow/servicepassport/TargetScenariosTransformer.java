package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author izerenev
 *         Date: 24.01.2015
 */
public class TargetScenariosTransformer<T extends GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario> extends RguAbstractTransformer<T> {

    public TargetScenariosTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Override
    public SXObj transform(@Nonnull T obj, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(obj, "egTargetScenario", callback);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T targetScenario) throws RguTransformationException {
        return RguTransformationUtils.parseRef(targetScenario.getId()).id;
    }

    @Nonnull
    @Override
    protected String getObjTitle(@Nonnull T transformingObject) {
        return transformingObject.getTitle();
    }

    @Nonnull
    @Override
    protected Map<String, Object> prepareDataMap(@Nonnull T scenario, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("title", scenario.getTitle());
        map.put("idRgu", scenario.getId());
        if (scenario.getScenarioType() != null) {
            GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.ScenarioType scenarioType = scenario.getScenarioType();
            String ref = scenarioType.getRef();
            RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
            SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
            if (sxId != null) {
                map.put("type", sxId);
            }
        }

        map.put("outDocs", getOutDocs(scenario, callback));
        return map;
    }

    private List<SXId> getOutDocs(T scenario, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        List<GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments> outDocuments = scenario.getOutDocuments().getScenarioOutDocuments();
        List<SXId> result = new ArrayList<SXId>(outDocuments.size());
        for (GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments outDocument : outDocuments) {
            SXObj sxObj = transformObj(outDocument, callback);
            if (sxObj != null) {
                result.add(sxObj.getId());
            }
        }

        return result;
    }
}
