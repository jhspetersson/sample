package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.commons.io.FilenameUtils;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author izerenev
 *         Date: 22.01.2015
 */
public class TargetInDocumentsTransformer<T extends GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments>
        extends RguAbstractTransformer<T> {

    public TargetInDocumentsTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T serviceTarget) throws RguTransformationException {
        return BigInteger.valueOf(serviceTarget.getId());
    }

    @Override
    public SXObj transform(@Nonnull T obj, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(obj, "egTargetInDocuments", callback);
    }

    @Nonnull
    @Override
    protected String getObjTitle(@Nonnull T transformingObject) {
        return String.valueOf(transformingObject.getId());
    }

    @Nonnull
    @Override
    protected Map<String, Object> prepareDataMap(@Nonnull T transformingObject, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("docName", getDocumentName(transformingObject));
        map.put("out", getOut(transformingObject));
        map.put("cnt", transformingObject.getQuantity());
        map.put("type", getDocumentType(transformingObject));
        map.put("idRgu", transformingObject.getId().toString());
        map.put("document", getDocument(transformingObject));
        return map;
    }

    private SXId getDocument(T transformingObject) throws RguTransformationException {
        GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.WorkDocument workDocument = transformingObject.getWorkDocument();
        if (workDocument == null) {
            return null;
        }
        String ref = workDocument.getId();
        if (Strings.isNullOrEmpty(ref)) {
            return null;
        }
        return RguTransformationUtils.downloadFileAndCreateCmsFile(ref, "egTargetInDocuments", "document");
    }

    private String getDocumentName(T transformingObject) throws RguTransformationException {
        GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.WorkDocument workDocument = transformingObject.getWorkDocument();
        if (workDocument == null) {
            return null;
        }
        String ref = workDocument.getId();
        if (Strings.isNullOrEmpty(ref)) {
            return null;
        }

        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
        try {
            SXObj pprDocObj = RguTransformationUtils.getPprDocByRguId(rguRef.id);
            return FilenameUtils.getBaseName(pprDocObj.getStringAttr("rguDocName"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private SXId getDocumentType(T transformingObject) throws RguTransformationException {
        GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.DocumentType documentType = transformingObject.getDocumentType();
        if (documentType == null) {
            return null;
        }
        String ref = documentType.getRef();
        if (Strings.isNullOrEmpty(ref)) {
            return null;
        }
        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
        return RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
    }

    private List<SXId> getOut(T transformingObject) throws RguTransformationException {
        List<SXId> result = new ArrayList<SXId>();

        try {
            for (GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.UseTypes.DocumentUseType documentUseType : transformingObject.getUseTypes().getDocumentUseType()) {
                RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(documentUseType.getRef());
                SXId out = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
                if (out != null) {
                    result.add(out);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }
}
