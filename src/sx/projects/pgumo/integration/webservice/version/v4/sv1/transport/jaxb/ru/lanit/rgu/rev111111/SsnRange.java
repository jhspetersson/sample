package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ssnRange complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="ssnRange">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ssnFrom" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ssnTo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ssnRange", propOrder = {
        "ssnFrom",
        "ssnTo"
})
public class SsnRange {

    protected long ssnFrom;
    protected Long ssnTo;

    /**
     * Gets the value of the ssnFrom property.
     */
    public long getSsnFrom() {
        return ssnFrom;
    }

    /**
     * Sets the value of the ssnFrom property.
     */
    public void setSsnFrom(long value) {
        this.ssnFrom = value;
    }

    /**
     * Gets the value of the ssnTo property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getSsnTo() {
        return ssnTo;
    }

    /**
     * Sets the value of the ssnTo property.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setSsnTo(Long value) {
        this.ssnTo = value;
    }

}
