package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.statestructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBElement;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;

public class OfficeTransformer<T extends Office> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(OfficeTransformer.class);

    public OfficeTransformer(RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Override
    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T office) throws RguTransformationException {
        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(office.getId());
        return rguRef.id;
    }

    @Override
    @Nonnull
    protected String getObjTitle(@Nonnull T transformingObject) {
        return transformingObject.getTitle();
    }

    @Nonnull
    public SXObj transform(@Nonnull T office, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(office, "egOffice", callback);
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "rguId";
    }

    @Override
    @Nonnull
    protected Map<String, Object> prepareDataMap(T office, RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> crMap = new HashMap<String, Object>();
        crMap.put(getRguIdAttrName(), getObjRguId(office));
        crMap.put("title", office.getTitle());
        Office.Address address = office.getAddress();
        crMap.put("subj", address == null ? null : address.getFederalSubject());
        crMap.put("region", address == null ? null : address.getArea());
        crMap.put("city", address == null ? null : address.getCity());
        crMap.put("street", address == null ? null : address.getStreet());
        crMap.put("house", address == null ? null : address.getHouse());
        crMap.put("building", address == null ? null : address.getBuilding());
        crMap.put("office", address == null ? null : address.getRoom());
        crMap.put("index", address == null ? null : address.getZip());
        crMap.put("phone", office.getPhone());
        crMap.put("fax", office.getFax());
        crMap.put("email", office.getEmail());
        crMap.put("workHours", office.getExpeditionSchedule());
        crMap.put("classification", getClassification(office, callback));
        return crMap;
    }

    @Nonnull
    public List<SXId> getClassification(@Nonnull T office, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        if (office.getTerritories().getContent().isEmpty()) {
            return Collections.emptyList();
        }

        List<SXId> result = new ArrayList<SXId>();
        List<Serializable> list = null;

        // TODO: implement later
//    try {
//      list = office.getTerritories().getContent();
//      for (Serializable addr2terr : list) {
//        Office.Territories.Address2Territory.Territory territory;
//        if (addr2terr instanceof JAXBElement) {
//          territory = ((Office.Territories.Address2Territory) ((JAXBElement) addr2terr).getValue()).getTerritory();
//        } else if (addr2terr instanceof Office.Territories.Address2Territory) {
//          territory = ((Office.Territories.Address2Territory) addr2terr).getTerritory();
//        } else {
//          log.error("��������� ������� ��������� ���� egOffice.classification" + list);
//          continue;
//        }
//        RguTransformationUtils.RguRef ref = RguTransformationUtils.parseRef(territory.getRef());
//        SXId clazz = RguTransformationUtils.findClassification(ref.type, ref.id.toString());
//        if (clazz != null) {
//          result.add(clazz);
//        }
//      }
//    } catch (Exception e) {
//      log.error("��������� ������� ��������� ���� egOffice.classification " + list, e);
//    }

        return result;
    }
}
