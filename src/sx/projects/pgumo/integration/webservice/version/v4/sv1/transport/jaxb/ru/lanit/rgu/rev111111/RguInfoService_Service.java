package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 */
@WebServiceClient(name = "rguInfoService", targetNamespace = "http://rgu.lanit.ru/rev111111")
public class RguInfoService_Service
        extends Service {

    private final static Logger logger = Logger.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService_Service.class.getName());


    public RguInfoService_Service(String wsdl) {
        super(getUrl(wsdl), new QName("http://rgu.lanit.ru/rev111111", "rguInfoService"));
    }

    private static URL getUrl(String wsdlUrl) {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService_Service.class.getResource(".");
            url = new URL(baseUrl, wsdlUrl);
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: '" + wsdlUrl + "', retrying as a local file");
            logger.warning(e.getMessage());
        }
        return url;
    }

    /**
     * @return returns RguInfoService
     */
    @WebEndpoint(name = "rguInfoServicePort")
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService getRguInfoServicePort() {
        return super.getPort(new QName("http://rgu.lanit.ru/rev111111", "rguInfoServicePort"), sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService.class);
    }

    /**
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return returns RguInfoService
     */
    @WebEndpoint(name = "rguInfoServicePort")
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RguInfoService getRguInfoServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://rgu.lanit.ru/rev111111", "rguInfoServicePort"), RguInfoService.class, features);
    }

}
