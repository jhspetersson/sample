package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.statestructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel;

import javax.annotation.Nonnull;

public class AdministrativeLevelTransformer<T extends AdministrativeLevel> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(AdministrativeLevelTransformer.class);

    public AdministrativeLevelTransformer(RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    public SXObj transform(@Nonnull T level, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        String value = level.getValue();
        log.debug("����������� ���������������� ������� " + value);
        return RguTransformationUtils.getOrCreateDictionaryItem("egAdmLevel", "code", value);
    }
}
