package sx.projects.pgumo.integration.webservice.version.v4.sv1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.RegistryInfoService;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.*;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List.ObjectRef;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.*;

/**
 * ����� ��� ������� ������� ������ �� ���
 */
public class RGUServiceFacade {
  protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.RGUServiceFacade.class);
  /**
   * ������ ���
   */
  private RegistryInfoService registryInfoService;

  /**
   * �����������
   *
   * @param wsdl ���� �������
   */
  public RGUServiceFacade(String wsdl) {
    registryInfoService = new RegistryInfoService(wsdl);
  }

  public static void main(String[] args) {
    sx.projects.pgumo.integration.webservice.version.v4.sv1.RGUServiceFacade service = new sx.projects.pgumo.integration.webservice.version.v4.sv1.RGUServiceFacade("http://195.239.151.219:8889/RGU_WAR_2/ws-services/registryInfoService");
    service.setSenderCode("MER001001");
    service.setSenderName("MER001001");
    service.setRecipientCode("MER001001");
    service.setRecipientName("MER001001");
    service.setOriginatorCode("MER001001");
    service.setOriginatorName("MER001001");
  }

  /**
   * ������������� ������ ������� ������� � ������� ����
   *
   * @param smevSign ������ ������� ������� � ������� ����
   */
  public void setSmevSign(RegistryInfoService.SmevSign smevSign) {
    registryInfoService.setSmevSign(smevSign);
  }

  /**
   * @param senderCode ��� �����������
   */
  public void setSenderCode(String senderCode) {
    registryInfoService.getCommonInfo().setSenderCode(senderCode);
  }

  /**
   * @param senderName ������������ ����������
   */
  public void setSenderName(String senderName) {
    registryInfoService.getCommonInfo().setSenderName(senderName);
  }

  /**
   * @param recipientCode ��� ����������
   */
  public void setRecipientCode(String recipientCode) {
    registryInfoService.getCommonInfo().setRecipientCode(recipientCode);
  }

  /**
   * @param recipientName ������������ ����������
   */
  public void setRecipientName(String recipientName) {
    registryInfoService.getCommonInfo().setRecipientName(recipientName);
  }

  /**
   * @param originatorCode ��� �����������
   */
  public void setOriginatorCode(String originatorCode) {
    registryInfoService.getCommonInfo().setOriginatorCode(originatorCode);
  }

  /**
   * @param originatorName ������������ �����������
   */
  public void setOriginatorName(String originatorName) {
    registryInfoService.getCommonInfo().setOriginatorName(originatorName);
  }


  /**
   * ���������� ������ �� �������� ������
   *
   * @param id ������������� �������� ������
   * @return ������� ������
   */
  public GetPsPassportResponse.PsPassport getServicePassport(long id) {
    return registryInfoService.getPsPassport(id);
  }

  /**
   * ���������� ������ �� �����������
   *
   * @param id ������������� �����������
   * @return �����������
   */
  public GetRStateStructureResponse.RStateStructure getStateStructure(long id) {
    return registryInfoService.getRStateStructure(id);
  }

  /**
   * ���������� ������ ��������� � ���� ���
   *
   * @param ssnFrom ����� �������, ������� � �������� �������� ���������
   * @return ��������� ������� � ������ �������
   */
  public ChangedObjects getChangedObjects(long ssnFrom, long ssnTo) {
    ChangedObjects result = new ChangedObjects();
    GetChangesResponse.ObjectRefList changes = registryInfoService.getChanges(ssnFrom, (ssnTo > 0) ? (((ssnFrom + 1000) < ssnTo) ? (ssnFrom + 1000) : ssnTo) : (ssnFrom + 1000));
    result.getObjects().addAll(changes.getList().getObjectRef());

    long finalSsn = (ssnTo > 0) ? ssnTo : changes.getCurrentSsn().longValue();
    ssnFrom += 1000;
    ssnTo = ssnFrom + 1000;
    if (ssnTo > finalSsn) {
      ssnTo = finalSsn;
    }

    while (finalSsn >= ssnTo) {
      changes = registryInfoService.getChanges(ssnFrom, ssnTo);
      result.getObjects().addAll(changes.getList().getObjectRef());
      ssnFrom += 1000;
      ssnTo = ssnFrom + 1000;
    }

    result.setCurrentSsn(changes.getCurrentSsn().longValue());
    return result;
  }

  public static class ChangedObjects {
    private long currentSsn;
    private List<ObjectRef> objects = new LinkedList<ObjectRef>();

    public long getCurrentSsn() {
      return currentSsn;
    }

    public void setCurrentSsn(long currentSsn) {
      this.currentSsn = currentSsn;
    }

    public List<ObjectRef> getObjects() {
      return objects;
    }

    public void setObjects(List<ObjectRef> objects) {
      this.objects = objects;
    }
  }

    /**
     * ��������� ��������, �������� �������������
     * ���� ��� �� ��������
     *
     * @param ssnFrom ��������� ������� ������ ���
     * @return ������ ��������
     */
  public DeletedObjects getDeletedObjects(long ssnFrom, long ssnTo) {
    DeletedObjects result = new DeletedObjects();
    GetRevokationListResponse.ObjectRefList changes = registryInfoService.getRevokationList(ssnFrom, (ssnTo > 0) ? (((ssnFrom + 1000) < ssnTo) ? (ssnFrom + 1000) : ssnTo) : (ssnFrom + 1000));
    result.getObjects().addAll(changes.getList().getObjectRef());

    long finalSsn = (ssnTo > 0) ? ssnTo : changes.getCurrentSsn().longValue();
    ssnFrom += 1000;
    ssnTo = ssnFrom + 1000;
    if (ssnTo > finalSsn) {
        ssnTo = finalSsn;
    }

    while (finalSsn >= ssnTo) {
      changes = registryInfoService.getRevokationList(ssnFrom, ssnTo);
      result.getObjects().addAll(changes.getList().getObjectRef());
      ssnFrom += 1000;
      ssnTo = ssnFrom + 1000;
    }

    result.setCurrentSsn(changes.getCurrentSsn().longValue());
    return result;
  }

  public static class DeletedObjects {
    private long currentSsn;
    private List<GetRevokationListResponse.ObjectRefList.List.ObjectRef> objects = new LinkedList<GetRevokationListResponse.ObjectRefList.List.ObjectRef>();

    public long getCurrentSsn() {
      return currentSsn;
    }

    public void setCurrentSsn(long currentSsn) {
      this.currentSsn = currentSsn;
    }

    public List<GetRevokationListResponse.ObjectRefList.List.ObjectRef> getObjects() {
      return objects;
    }

    public void setObjects(List<GetRevokationListResponse.ObjectRefList.List.ObjectRef> objects) {
      this.objects = objects;
    }
  }

  /**
   * �������, ������� ���������������� � �������� ����� �����������
   *
   * @return ������ ��������
   */
  public DictionaryObjects getDictionaryObjects() {
    DictionaryObjects result = new DictionaryObjects();
    GetListDictionaryResponse.ObjectRefList changes = registryInfoService.getListDictionaries();
    try {
      List<GetListDictionaryResponse.ObjectRefList.List.ObjectRef> objectRefs = changes.getList().getObjectRef();
      result.getDictionaries().addAll(objectRefs);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }

    return result;
  }

  public static class DictionaryObjects {
    private List<GetListDictionaryResponse.ObjectRefList.List.ObjectRef> dictionaries = new LinkedList<GetListDictionaryResponse.ObjectRefList.List.ObjectRef>();

    public List<GetListDictionaryResponse.ObjectRefList.List.ObjectRef> getDictionaries() {
      return dictionaries;
    }

    public void setDictionaries(List<GetListDictionaryResponse.ObjectRefList.List.ObjectRef> dictionaries) {
      this.dictionaries = dictionaries;
    }
  }

    /**
     * ��������� �������� �������� � �������� ������ �����������
     *
     * @param code ��� �������� �����, ���� OBJECT_TYPES, LIFE_EVENTS � �.�.
     * @param parent �������� ��� ������� �������� ���� TERRITORY, �.�. �� ����� �����, ��� �� ����������� � ������������� ������ � ������������,
     *               �� � ��� ����� ������ ���. ������ ���������� �������, ��� ������ ������ �������� �������� ������������ ID �������� � ���
     * @return ������ ��������
     */
  public DictionaryObjectsByCode getDictionary(String code, String parent) {
    DictionaryObjectsByCode result = new DictionaryObjectsByCode();
    GetDictionaryResponse.ObjectRefList changes = registryInfoService.getDictionary(code, parent);
    try {
      List<GetDictionaryResponse.ObjectRefList.List.ObjectRef> objectRefs = changes.getList().getObjectRef();
      result.getDictionaries().addAll(objectRefs);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }

    return result;
  }

  public static class DictionaryObjectsByCode {
    private List<GetDictionaryResponse.ObjectRefList.List.ObjectRef> dictionaries = new LinkedList<GetDictionaryResponse.ObjectRefList.List.ObjectRef>();

    public List<GetDictionaryResponse.ObjectRefList.List.ObjectRef> getDictionaries() {
      return dictionaries;
    }

    public void setDictionaries(List<GetDictionaryResponse.ObjectRefList.List.ObjectRef> dictionaries) {
      this.dictionaries = dictionaries;
    }
  }
}
