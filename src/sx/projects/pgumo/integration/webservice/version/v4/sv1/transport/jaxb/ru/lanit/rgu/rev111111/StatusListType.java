package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for statusListType complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="statusListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusDescriptor" maxOccurs="100">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ObjectRef">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="objectType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="PsPassport"/>
 *                                   &lt;enumeration value="RStateStructure"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ssn" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="status">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;enumeration value="1"/>
 *                         &lt;enumeration value="2"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="statusDetails" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="4000"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "statusListType", propOrder = {
        "statusDescriptor"
})
public class StatusListType {

    @XmlElement(required = true)
    protected List<StatusDescriptor> statusDescriptor;

    /**
     * Gets the value of the statusDescriptor property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statusDescriptor property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatusDescriptor().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType.StatusDescriptor }
     */
    public List<StatusDescriptor> getStatusDescriptor() {
        if (statusDescriptor == null) {
            statusDescriptor = new ArrayList<StatusDescriptor>();
        }
        return this.statusDescriptor;
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ObjectRef">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="objectType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="PsPassport"/>
     *                         &lt;enumeration value="RStateStructure"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ssn" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="status">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;enumeration value="1"/>
     *               &lt;enumeration value="2"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="statusDetails" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="4000"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "objectRef",
            "ssn",
            "status",
            "statusDetails"
    })
    public static class StatusDescriptor {

        @XmlElement(name = "ObjectRef", required = true)
        protected ObjectRef objectRef;
        protected long ssn;
        @XmlElement(required = true)
        protected BigInteger status;
        protected String statusDetails;

        /**
         * Gets the value of the objectRef property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType.StatusDescriptor.ObjectRef }
         */
        public ObjectRef getObjectRef() {
            return objectRef;
        }

        /**
         * Sets the value of the objectRef property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType.StatusDescriptor.ObjectRef }
         */
        public void setObjectRef(ObjectRef value) {
            this.objectRef = value;
        }

        /**
         * Gets the value of the ssn property.
         */
        public long getSsn() {
            return ssn;
        }

        /**
         * Sets the value of the ssn property.
         */
        public void setSsn(long value) {
            this.ssn = value;
        }

        /**
         * Gets the value of the status property.
         *
         * @return possible object is
         * {@link java.math.BigInteger }
         */
        public BigInteger getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         *
         * @param value allowed object is
         *              {@link java.math.BigInteger }
         */
        public void setStatus(BigInteger value) {
            this.status = value;
        }

        /**
         * Gets the value of the statusDetails property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getStatusDetails() {
            return statusDetails;
        }

        /**
         * Sets the value of the statusDetails property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setStatusDetails(String value) {
            this.statusDetails = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="objectType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="PsPassport"/>
         *               &lt;enumeration value="RStateStructure"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "objectType",
                "objectId"
        })
        public static class ObjectRef {

            @XmlElement(required = true)
            protected String objectType;
            protected long objectId;

            /**
             * Gets the value of the objectType property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getObjectType() {
                return objectType;
            }

            /**
             * Sets the value of the objectType property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setObjectType(String value) {
                this.objectType = value;
            }

            /**
             * Gets the value of the objectId property.
             */
            public long getObjectId() {
                return objectId;
            }

            /**
             * Sets the value of the objectId property.
             */
            public void setObjectId(long value) {
                this.objectId = value;
            }

        }

    }

}
