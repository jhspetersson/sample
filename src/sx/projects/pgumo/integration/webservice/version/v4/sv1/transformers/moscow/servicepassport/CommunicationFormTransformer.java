package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm;

import javax.annotation.Nonnull;

public class CommunicationFormTransformer<T extends CommunicationForm> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(CommunicationFormTransformer.class);

    public CommunicationFormTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T communicationForm, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        String title = communicationForm.getCommunicationFormRef().getValue();
        log.debug("����������� ����� �������������� " + title);
        return RguTransformationUtils.getOrCreateDictionaryItem("EgRCommunicationForm", "title", title);
    }
}
