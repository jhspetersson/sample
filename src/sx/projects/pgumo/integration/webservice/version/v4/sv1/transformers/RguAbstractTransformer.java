package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXDsFactory;
import sx.datastore.SXObj;
import sx.datastore.SXObjList;
import sx.datastore.db.utils.Criterion;
import sx.datastore.db.utils.SqlEnum;
import sx.datastore.meta.SXAttr;
import sx.datastore.params.SXCreateObjParams;
import sx.datastore.params.SXObjListParams;
import sx.datastore.params.SXUpdateObjParams;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.exceptions.RguTransformationRuntimeException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * ������ ��� �������-�������������
 *
 * @param <T> Java-����� ����������������� �������, ���� �� ��������������� ������ ��� SOAP-������
 */
public abstract class RguAbstractTransformer<T> implements sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer.class);

    private RguTransformerFactory transformerFactory;

    public RguAbstractTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        this.transformerFactory = transformerFactory;
    }

    @Nonnull
    public RguTransformerFactory getTransformerFactory() {
        return transformerFactory;
    }

    /**
     * ����� ������ � ������-����������, ������� ������������ � �������
     *
     * @param obj
     * @param callback
     * @return
     * @throws RguTransformationException
     */
    protected SXObj transformObj(@Nonnull Object obj, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        RguTransformer transformer = getTransformerFactory().getTransformer(obj);
        return transformer.transform(obj, callback);
    }

    /**
     * ������������� ������� � ��������� ����������� ��� � ����, ���� ���� - ��������� ������,
     * � ��������� ������ �������
     *
     * @param transformingObject
     * @param sxClassName
     * @param callback
     * @param runHandlers        �������� ��� ��� �������� �� SiTex-�����, �������� ��� ����������� ������� ������ egClassification,
     *                           �� ������� �������� �������� � ������-������� �������� ����, � ��� ��� ����
     * @return SiTex-������
     * @throws RguTransformationException
     */
    @Nonnull
    protected SXObj transformation(@Nonnull T transformingObject,
                                   @Nonnull String sxClassName,
                                   @Nonnull RguTransformerCallback callback,
                                   boolean runHandlers) throws RguTransformationException {
        boolean saveArchivedObjects = callback.getSaveArchivedObjects();
        BigInteger objRguId = getObjRguId(transformingObject);
        log.debug("������ ������� (" + transformingObject.getClass().getSimpleName() + ") " + getObjTitle(transformingObject) + "(" + objRguId + ")");
        SXObj sxObj = getSXObj(transformingObject, sxClassName, saveArchivedObjects);
        return sxObj == null
                ? createSXObj(transformingObject, sxClassName, callback, runHandlers)
                : updateSXObj(transformingObject, sxObj, callback, runHandlers);
    }

    /**
     * ������������� ������� � �� ��������� ����������� ���������� �� SiTex-�����
     *
     * @param transformingObject
     * @param sxClassName
     * @param callback
     * @return
     * @throws RguTransformationException
     */
    @Nonnull
    protected SXObj transformation(@Nonnull T transformingObject,
                                   @Nonnull String sxClassName,
                                   @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(transformingObject, sxClassName, callback, true);
    }

    /**
     * ����� SiTex-�������, ������� ������������� �����������������
     * �������������� �� ������� ������������ ��������� �������� (������ idRgu)
     * <p/>
     * ����������� ����� "��������� �������������� �������"
     *
     * @param transformingObject  ���������������� ������
     * @param sxClassName         ����� SiTex-�������
     * @param saveArchivedObjects ����� "��������� �������������� �������"
     * @return SiTex-������
     * @throws RguTransformationException
     * @see <a href="http://bugs.mysitex.com/browse/MSMEV-3460">http://bugs.mysitex.com/browse/MSMEV-3460</a>
     */
    @Nullable
    protected SXObj getSXObj(T transformingObject, String sxClassName, boolean saveArchivedObjects) throws RguTransformationException {
        SXObjListParams olParams = new SXObjListParams(sxClassName);
        olParams.addCondition(getRguIdAttrName(), getObjRguId(transformingObject).toString());

        if (saveArchivedObjects) {
            SXAttr archiveAttr = SXDsFactory.getDs().getAttr(sxClassName, "archive");
            if (archiveAttr != null) {
                Criterion notArchive = olParams.getCriteria().getNewCriterion(archiveAttr.getFullName(), Boolean.TRUE, SqlEnum.NOT_EQUAL);
                Criterion nullArchive = olParams.getCriteria().getNewCriterion(archiveAttr.getFullName(), null, SqlEnum.ISNULL);
                olParams.getCriteria().and(notArchive.or(nullArchive));
            }
        }

        SXObj sxObj = null;
        try {
            sxObj = olParams.getObj();
        } catch (Exception e) {
            //throw new RguDbException("������ ��� ��������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject), e);
            log.error("������ ��� ��������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject) + ": " + e.getMessage(), e);
        }
        return sxObj;
    }

    /**
     * ��������� ��������� ��������, � ������� ����������� ID �� ���
     *
     * @param transformingObject ���������������� ������
     * @return ������������ ������� � ���
     * @throws RguTransformationException
     */
    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T transformingObject) throws RguTransformationException {
        throw new RguTransformationRuntimeException("Method getObjRguId(T transformingObject) don't support in class " + getClass().getName());
    }

    /**
     * ��������� �������� ��������� ��������, � ������� ����������� ID �� ���
     * ������ ��� idRgu
     *
     * @return �������� ��������� �������� ID �� ���
     */
    @Nonnull
    protected String getRguIdAttrName() {
        throw new RguTransformationRuntimeException("Method getRguIdAttrName() don't support in class " + getClass().getName());
    }

    /**
     * ��������� ������������ ����������������� �������
     *
     * @param transformingObject ���������������� ������
     * @return ������������ �������
     */
    @Nonnull
    protected String getObjTitle(@Nonnull T transformingObject) {
        throw new RguTransformationRuntimeException("Method getObjTitle(T transformingObject) don't support in class " + getClass().getName());
    }

    /**
     * ���������� ������ ��� �������� ��� ���������� SiTex-�������
     *
     * @param transformingObject ���������������� ������
     * @param callback
     * @return ������ ��� SXCreateObjParams
     * @throws RguTransformationException
     */
    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T transformingObject, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        throw new RguTransformationRuntimeException("Method prepareDataMap(T transformingObject, RguTransformerCallback callback) don't support in class " + getClass().getName());
    }

    @Nonnull
    protected Collection<String> getAttrsForCascadeDel() {
        return Collections.emptySet();
    }

    /**
     * ������� ��������� �������� � ����������������� �������, ����� ������� �� ������������ ������ � �������� �������
     *
     * @param transformingObject ���������������� ������
     * @param sxObj              SiTex-������
     * @throws RguTransformationException
     */
    private void removeLinkedObjs(@Nonnull T transformingObject, @Nonnull SXObj sxObj) throws RguTransformationException {
        for (String attrName : getAttrsForCascadeDel()) {
            SXAttr sxAttr = sxObj.getSXClass().getAttr(attrName);
            if (sxAttr != null && sxAttr.getRefClass() != null) {
                if (sxAttr.isLinkNtoM() || sxAttr.isBackReference()) {
                    log.debug("������� ��������� ������� (" + sxAttr.getRefClass().getName() + "), ����������� � ������� (" + getObjRguId(transformingObject) + ") ����� ������� (" + sxAttr.getName() + "), ��� �� ������ �����");
                    SXObjList linkedObjects;
                    try {
                        linkedObjects = new SXObjListParams(sxAttr.getRefClass())
                                .setLinkTo(sxObj.getId(), sxAttr.getName())
                                .getObjList();

                        if (linkedObjects != null) {
                            for (SXObj linkedObj : linkedObjects.getSXObjList()) {
                                try {
                                    log.debug("������� ������ " + linkedObj.getId().getLink());
                                    linkedObj.getId().deleteObj();
                                } catch (Exception e) {
                                    //throw new RguDbException("������ ��� �������� ������� " + linkedObj.getId().getLink(), e);
                                    log.error("������ ��� �������� ������� " + linkedObj.getId().getLink(), e);
                                }
                            }
                        }
                    } catch (Exception e) {
                        //throw new RguDbException("������ ��� ������� �������� �� �������� (" + sxAttr.getName() + ") � ������� " + sxObj.getId().getLink(), e);
                        log.error("������ ��� ������� �������� �� �������� (" + sxAttr.getName() + ") � ������� " + sxObj.getId().getLink(), e);
                    }

                }
            }
        }
    }

    /**
     * ���������������� ���������� ����������������� ������� � ��������� ����������� ���������,
     * ����� ������� �� ������
     *
     * @param transformingObject ���������������� ������
     * @param sxObj              SiTex-������
     * @param callback
     * @param runHandlers
     * @return
     * @throws RguTransformationException
     */
    protected SXObj updateSXObj(@Nonnull T transformingObject,
                                @Nonnull SXObj sxObj,
                                @Nonnull RguTransformerCallback callback,
                                boolean runHandlers) throws RguTransformationException {
        log.error("������ (" + transformingObject.getClass().getSimpleName() + ") ������ - ���������");
        removeLinkedObjs(transformingObject, sxObj);
        SXUpdateObjParams uParams;
        Map<String, Object> uMap = null;
        try {
            uParams = new SXUpdateObjParams(sxObj.getId());

            uMap = prepareDataMap(transformingObject, callback);
            uParams.setData(uMap);
            uParams.setUpdateReadOnlyAttr(true);
            uParams.setCheckMandatoryAttrs(false);
            uParams.setRunHandlers(runHandlers);

            sxObj = uParams.updateObj();
            log.error("������ (" + transformingObject.getClass().getSimpleName() + ") �������� - " + sxObj.getId());
            return sxObj;
        } catch (Exception e) {
            //throw new RguDbException("������ ��� ���������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject) + " dataMap = " + uMap, e);
            log.error("������ ��� ���������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject) + " dataMap = " + uMap, e);
        }
        return null;
    }

    /**
     * ���������������� �������� SiTex-������� �� �����������������
     *
     * @param transformingObject ���������������� ������
     * @param sxClassName        SiTex-�����
     * @param callback
     * @param runHandlers
     * @return
     * @throws RguTransformationException
     */
    protected SXObj createSXObj(@Nonnull T transformingObject,
                                @Nonnull String sxClassName,
                                @Nonnull RguTransformerCallback callback,
                                boolean runHandlers) throws RguTransformationException {
        log.error("������ (" + transformingObject.getClass().getSimpleName() + ") �� ������ - ������");
        SXCreateObjParams crParams = new SXCreateObjParams(sxClassName);
        Map<String, Object> crMap = prepareDataMap(transformingObject, callback);
        crParams.setData(crMap);
        crParams.setCheckReadOnly(false);
        crParams.setCheckMandatoryAttrList(false);
        crParams.setRunHandlers(runHandlers);
        try {
            SXObj obj = crParams.createObj();
            log.error("������ " + transformingObject.getClass().getSimpleName() + " ������ - " + obj.getId());
            return obj;
        } catch (Exception e) {
            //throw new RguDbException("������ ��� �������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject) + " runHandlers = " + runHandlers + " dataMap = " + crMap, e);
            log.error("������ ��� �������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject) + " runHandlers = " + runHandlers + " dataMap = " + crMap, e);
        }
        return null;
    }

    /**
     * @return �������� SiTex-������ ��� ����������������� �������
     */
    protected String getSxClassName() {
        return "";
    }

    /**
     * "�������" ������ � �������, ������ ��� � ����������
     *
     * @param transformingObject ���������������� ������
     * @param callback           RguTransformerCallback
     * @throws RguTransformationException
     */
    public void delete(@Nonnull T transformingObject, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        boolean saveArchivedObjects = callback.getSaveArchivedObjects();
        SXObj sxObj = getSXObj(transformingObject, getSxClassName(), saveArchivedObjects);
        if (sxObj != null) {
            deleteSXObj(transformingObject, sxObj);
        }
    }

    /**
     * ������� � SXObj'a ������� �����������
     *
     * @param transformingObject ���������������� ������
     * @param sxObj              SiTex-������
     * @throws RguTransformationException
     */
    protected void deleteSXObj(T transformingObject, SXObj sxObj) throws RguTransformationException {
        log.error("������� ������ (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject));

        try {
            SXUpdateObjParams params = new SXUpdateObjParams(sxObj.getId());
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("isPublish", false);
            params.setData(dataMap);
            params.updateObj();
        } catch (Exception e) {
            log.error("������ ��� �������� ������� (" + transformingObject.getClass().getSimpleName() + ") � ����� ��� = " + getObjRguId(transformingObject), e);
        }
    }

}
