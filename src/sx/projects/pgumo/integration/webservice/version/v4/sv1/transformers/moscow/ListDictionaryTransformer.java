package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetListDictionaryResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class ListDictionaryTransformer<T extends GetListDictionaryResponse.ObjectRefList.List.ObjectRef> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(ListDictionaryTransformer.class);

    public ListDictionaryTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "cod";
    }

    @Override
    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T transformingObject) throws RguTransformationException {
        return transformingObject.getId();
    }

    @Override
    @Nonnull
    protected String getObjTitle(@Nonnull T transformingObject) {
        return transformingObject.getDescription();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T dictionary, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        System.out.println(dictionary.getId() + " " + dictionary.getName());

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", dictionary.getId().toString());
        map.put("cod", dictionary.getId().toString());
        map.put("title", !StringUtils.isBlank(dictionary.getDescription()) ? dictionary.getDescription() : "Заголовок отсутствует");
        map.put("codeDictRgu", dictionary.getName());

        log.error("DICTIONARY DATA MAP: " + map);

        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T offDoc, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(offDoc, "egClassification", callback, false);
    }

}
