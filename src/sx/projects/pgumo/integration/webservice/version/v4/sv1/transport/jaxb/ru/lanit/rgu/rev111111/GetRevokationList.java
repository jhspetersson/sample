package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getRevokationList complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="getRevokationList">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="SSNInterval" type="{http://rgu.lanit.ru/rev111111}ssnRange"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getRevokationList", propOrder = {
        "ssnInterval"
})
public class GetRevokationList
        extends Request {

    @XmlElement(name = "SSNInterval", required = true)
    protected SsnRange ssnInterval;

    /**
     * Gets the value of the ssnInterval property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange }
     */
    public SsnRange getSSNInterval() {
        return ssnInterval;
    }

    /**
     * Sets the value of the ssnInterval property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange }
     */
    public void setSSNInterval(SsnRange value) {
        this.ssnInterval = value;
    }

}
