package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.common.SXUtils;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.datastore.params.SXUpdateObjParams;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServicePaymentTransformer<T extends ServiceTarget.Payments.ServicePayment> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(ServicePaymentTransformer.class);

    public ServicePaymentTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }


    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T servicePayment) throws RguTransformationException {
        return RguTransformationUtils.parseRef(servicePayment.getPaymentInfo().getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T servicePayment) {
        return servicePayment.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T servicePayment, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("guid", getObjRguId(servicePayment));
        map.put("idRgu", getObjRguId(servicePayment));
        map.put("title", getObjTitle(servicePayment));
        map.put("sum", servicePayment.getPaymentInfo().getPaymentValue());
        map.put("kbk", servicePayment.getPaymentInfo().getKbk());
        map.put("comment", servicePayment.getPaymentInfo().getMethodDescription());
        map.put("descr", servicePayment.getPaymentInfo().getGroundForAction());

        try {
            List<ServiceTarget.Payments.ServicePayment.PaymentTypes.PaymentType> paymentTypes = servicePayment.getPaymentTypes().getPaymentTypes();
            if (!paymentTypes.isEmpty()) {
                ServiceTarget.Payments.ServicePayment.PaymentTypes.PaymentType paymentType = paymentTypes.get(0);
                String ref = paymentType.getRef();
                RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
                SXId paymentTypeId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
                map.put("RPaymentType", paymentTypeId);
            }
        } catch (Exception e) {
            log.error("������ � ������������ ���� �������");
            log.error(e.getMessage(), e);
        }

        List<SXId> ids = new ArrayList<SXId>();

        try {
            if (servicePayment.getPaymentInfo() != null && servicePayment.getPaymentInfo().getOffDocs() != null) {
                List<ServiceTarget.Payments.ServicePayment.PaymentInfo.Offdocs.Offdoc> offDocs = servicePayment.getPaymentInfo().getOffDocs().getOffDoc();
                for (ServiceTarget.Payments.ServicePayment.PaymentInfo.Offdocs.Offdoc doc : offDocs) {
                    RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(doc.getRef());
                    ids.add(RguTransformationUtils.getDictionaryItem("egNPA", "idRgu", rguRef.id.toString()).getId());
                }
            }
        } catch (Exception e) {
            log.error("������ ���������� �������� offDOcs � egPayment");
            log.error(e.getMessage(), e);
        }

        map.put("offDocs", ids);

        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T servicePayment, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(servicePayment, "egPayment", callback);
    }
}
