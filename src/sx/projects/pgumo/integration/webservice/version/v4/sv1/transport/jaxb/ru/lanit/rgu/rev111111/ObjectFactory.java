package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.DocumentType;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.FilesListType;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Response;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.lanit.rgu.rev111111 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRevokationList_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getRevokationList");
    private final static QName _PsPassportPreview_QNAME = new QName("http://rgu.lanit.ru/rev111111", "psPassportPreview");
    private final static QName _GetFeedBack_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getFeedBack");
    private final static QName _GetInteragencyMap_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getInteragencyMap");
    private final static QName _GetEPGUSectionDictionary_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getEPGUSectionDictionary");
    private final static QName _UpdateStatus_QNAME = new QName("http://rgu.lanit.ru/rev111111", "updateStatus");
    private final static QName _GetInteragencyMapRights_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getInteragencyMapRights");
    private final static QName _GetRStateStructure_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getRStateStructure");
    private final static QName _Response_QNAME = new QName("http://rgu.lanit.ru/rev111111", "response");
    private final static QName _GetWorkDocument_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getWorkDocument");
    private final static QName _GetAdmReglament_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getAdmReglament");
    private final static QName _RegisterFeedBack_QNAME = new QName("http://rgu.lanit.ru/rev111111", "registerFeedBack");
    private final static QName _GetPsPassport_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getPsPassport");
    private final static QName _GetChanges_QNAME = new QName("http://rgu.lanit.ru/rev111111", "getChanges");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.lanit.rgu.rev111111
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament }
     */
    public GetAdmReglament createGetAdmReglament() {
        return new GetAdmReglament();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType }
     */
    public StatusListType createStatusListType() {
        return new StatusListType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack }
     */
    public GetFeedBack createGetFeedBack() {
        return new GetFeedBack();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetPsPassport }
     */
    public GetPsPassport createGetPsPassport() {
        return new GetPsPassport();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights }
     */
    public GetInteragencyMapRights createGetInteragencyMapRights() {
        return new GetInteragencyMapRights();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRevokationList }
     */
    public GetRevokationList createGetRevokationList() {
        return new GetRevokationList();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange createSsnRange() {
        return new SsnRange();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack }
     */
    public RegisterFeedBack createRegisterFeedBack() {
        return new RegisterFeedBack();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.DocumentType }
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary }
     */
    public GetEPGUSectionDictionary createGetEPGUSectionDictionary() {
        return new GetEPGUSectionDictionary();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap }
     */
    public GetInteragencyMap createGetInteragencyMap() {
        return new GetInteragencyMap();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure }
     */
    public GetRStateStructure createGetRStateStructure() {
        return new GetRStateStructure();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.FilesListType }
     */
    public FilesListType createFilesListType() {
        return new FilesListType();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Response }
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType.StatusDescriptor }
     */
    public StatusListType.StatusDescriptor createStatusListTypeStatusDescriptor() {
        return new StatusListType.StatusDescriptor();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview }
     */
    public PsPassportPreview createPsPassportPreview() {
        return new PsPassportPreview();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges }
     */
    public GetChanges createGetChanges() {
        return new GetChanges();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus }
     */
    public UpdateStatus createUpdateStatus() {
        return new UpdateStatus();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.StatusListType.StatusDescriptor.ObjectRef }
     */
    public StatusListType.StatusDescriptor.ObjectRef createStatusListTypeStatusDescriptorObjectRef() {
        return new StatusListType.StatusDescriptor.ObjectRef();
    }

    /**
     * Create an instance of {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument }
     */
    public GetWorkDocument createGetWorkDocument() {
        return new GetWorkDocument();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRevokationList }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getRevokationList")
    public JAXBElement<GetRevokationList> createGetRevokationList(GetRevokationList value) {
        return new JAXBElement<GetRevokationList>(_GetRevokationList_QNAME, GetRevokationList.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "psPassportPreview")
    public JAXBElement<PsPassportPreview> createPsPassportPreview(PsPassportPreview value) {
        return new JAXBElement<PsPassportPreview>(_PsPassportPreview_QNAME, PsPassportPreview.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getFeedBack")
    public JAXBElement<GetFeedBack> createGetFeedBack(GetFeedBack value) {
        return new JAXBElement<GetFeedBack>(_GetFeedBack_QNAME, GetFeedBack.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getInteragencyMap")
    public JAXBElement<GetInteragencyMap> createGetInteragencyMap(GetInteragencyMap value) {
        return new JAXBElement<GetInteragencyMap>(_GetInteragencyMap_QNAME, GetInteragencyMap.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getEPGUSectionDictionary")
    public JAXBElement<GetEPGUSectionDictionary> createGetEPGUSectionDictionary(GetEPGUSectionDictionary value) {
        return new JAXBElement<GetEPGUSectionDictionary>(_GetEPGUSectionDictionary_QNAME, GetEPGUSectionDictionary.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "updateStatus")
    public JAXBElement<UpdateStatus> createUpdateStatus(UpdateStatus value) {
        return new JAXBElement<UpdateStatus>(_UpdateStatus_QNAME, UpdateStatus.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getInteragencyMapRights")
    public JAXBElement<GetInteragencyMapRights> createGetInteragencyMapRights(GetInteragencyMapRights value) {
        return new JAXBElement<GetInteragencyMapRights>(_GetInteragencyMapRights_QNAME, GetInteragencyMapRights.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getRStateStructure")
    public JAXBElement<GetRStateStructure> createGetRStateStructure(GetRStateStructure value) {
        return new JAXBElement<GetRStateStructure>(_GetRStateStructure_QNAME, GetRStateStructure.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "response")
    public JAXBElement<Response> createResponse(Response value) {
        return new JAXBElement<Response>(_Response_QNAME, Response.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getWorkDocument")
    public JAXBElement<GetWorkDocument> createGetWorkDocument(GetWorkDocument value) {
        return new JAXBElement<GetWorkDocument>(_GetWorkDocument_QNAME, GetWorkDocument.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getAdmReglament")
    public JAXBElement<GetAdmReglament> createGetAdmReglament(GetAdmReglament value) {
        return new JAXBElement<GetAdmReglament>(_GetAdmReglament_QNAME, GetAdmReglament.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "registerFeedBack")
    public JAXBElement<RegisterFeedBack> createRegisterFeedBack(RegisterFeedBack value) {
        return new JAXBElement<RegisterFeedBack>(_RegisterFeedBack_QNAME, RegisterFeedBack.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetPsPassport }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getPsPassport")
    public JAXBElement<GetPsPassport> createGetPsPassport(GetPsPassport value) {
        return new JAXBElement<GetPsPassport>(_GetPsPassport_QNAME, GetPsPassport.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges }{@code >}}
     */
    @XmlElementDecl(namespace = "http://rgu.lanit.ru/rev111111", name = "getChanges")
    public JAXBElement<GetChanges> createGetChanges(GetChanges value) {
        return new JAXBElement<GetChanges>(_GetChanges_QNAME, GetChanges.class, null, value);
    }

}
