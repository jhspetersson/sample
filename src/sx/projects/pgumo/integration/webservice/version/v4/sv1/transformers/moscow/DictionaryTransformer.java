package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetDictionaryResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class DictionaryTransformer<T extends GetDictionaryResponse.ObjectRefList.List.ObjectRef> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(DictionaryTransformer.class);

    public DictionaryTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "code";
    }

    @Override
    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T transformingObject) throws RguTransformationException {
        return transformingObject.getId();
    }

    @Override
    @Nonnull
    protected String getObjTitle(@Nonnull T transformingObject) {
        return transformingObject.getDescription();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T dictionary, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        System.out.println(dictionary.getId() + " " + dictionary.getTitle());

        String code = dictionary.getId().toString();

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            log.error("PARENT: " + dictionary.getParent().toString());

            SXObj parent = null;
            if (dictionary.getParent().toString().equals("1001") && dictionary.getId().toString().equals("1036")) {
                parent = RguTransformationUtils.getDictionaryItem("egClassification", "codeDictRgu", "TERRITORY");
            }
            if (parent == null) {
                parent = RguTransformationUtils.getDictionaryItem("egClassification", "code", dictionary.getParent().toString());
            }
//      if (parent == null) {
//        parent = RguTransformationUtils.getDictionaryItem("egClassification", "cod", dictionary.getParent().toString());
//      }
            if (parent != null) {
                map.put("parent", parent.getId());
                code = RguTransformationUtils.updateCode(parent.getId(), code);
                log.error("FOUND PARENT OBJ: " + parent.getId().toString() + " NEW CODE: " + code);
            } else {
                RguTransformationUtils.addOrphan(dictionary.getId().toString(), dictionary.getParent().toString());
            }
        } catch (Exception e) {

        }

        String title = RguTransformationUtils.containsCyrillic(dictionary.getTitle()) ? dictionary.getTitle() : dictionary.getDescription();
        if (StringUtils.isBlank(title)) {
            title = dictionary.getTitle();
        }
        if (StringUtils.isBlank(title)) {
            title = dictionary.getDescription();
        }
        if (StringUtils.isBlank(title)) {
            title = "Заголовок отсутствует";
        }

        map.put("code", code);
        map.put("cod", dictionary.getId().toString());
        map.put("title", title);

        log.error("DICTIONARY DATA MAP: " + map);

        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T dictionary, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(dictionary, "egClassification", callback, false);
    }

}
