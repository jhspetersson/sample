package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import com.google.common.base.Strings;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.addons.sprsystem.classification.beans.EgClassification;
import sx.cms.CmsActionUtils;
import sx.common.SXDateFormatStore;
import sx.datastore.*;
import sx.datastore.db.utils.SqlEnum;
import sx.datastore.impl.fs.SXDsFs;
import sx.datastore.impl.sitex2.beans.Folder;
import sx.datastore.meta.SXAttr;
import sx.datastore.meta.SXClass;
import sx.datastore.meta.SXMetaConsts;
import sx.datastore.params.SXCreateObjParams;
import sx.datastore.params.SXObjListParams;
import sx.datastore.params.SXUpdateObjParams;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;
import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class RguTransformationUtils {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils.class);
    private final static Map<String, String> orphans = new HashMap<String, String>();

    public static void clearOrphans() {
        orphans.clear();
    }

    public static Map<String, String> getOrphans() {
        return orphans;
    }

    public static void addOrphan(String code, String parent) {
        log.error("Adding orphan " + code + " with parent " + parent);
        orphans.put(code, parent);
    }

    public static RguRef parseRef(String ref) throws RguTransformationException {
        String[] split = ref.split("_");
        if (split.length == 2) {
            String objectType = split[0];
            String id = split[1];
            return new RguRef(StringUtils.isEmpty(id) ? null : new BigInteger(id), objectType);
        } else if (split.length == 3) {
            String objectType = split[0] + "_" + split[1];
            String id = split[2];
            return new RguRef(StringUtils.isEmpty(id) ? null : new BigInteger(id), objectType);
        } else if (split.length == 1) {
            return new RguRef(null, split[0]);
        }
        throw new RguTransformationException("���������� ���������� ������ - " + ref);
    }

    @Nullable
    public static SXObj getDictionaryItem(@Nonnull String clazz, @Nonnull String keyField, @Nonnull Object keyValue) {
        SXObjListParams olParams = new SXObjListParams(clazz);
        olParams.addCondition(keyField, keyValue);
        olParams.addSelectedAttr(keyField);
        try {
            SXObj obj = olParams.getObj();
            if (obj != null) {
                log.error("������ ������ - " + obj.getId().toString());
                return obj;
            } else {
                log.error("������ �� ������: " + clazz + " �� �������� " + keyField + " �� ��������� \"" + keyValue + "\"");
            }
        } catch (Exception e) {
            throw new RguDbException("������ ��� ��������� ������� ����������� " + clazz + " �� �������� " + keyField + " �� ��������� \"" + keyValue + "\"", e);
        }

        return null;
    }

    @Nonnull
    public static SXObj createDictionaryItem(@Nonnull String clazz, @Nonnull String keyField, @Nonnull Object keyValue) {
        SXCreateObjParams crParams = new SXCreateObjParams(clazz);
        Map<String, Object> crMap = new HashMap<String, Object>();
        crMap.put(keyField, keyValue);
        crParams.setData(crMap);
        crParams.setCheckReadOnly(false);
        crParams.setCheckMandatoryAttrList(false);
        try {
            SXObj obj = crParams.createObj();
            log.debug("������ ������ - " + obj.getId());
            return obj;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RguDbException("������ ��� �������� ������� ����������� " + clazz + " �� �������� " + keyField + " �� ��������� \"" + keyValue + "\"", e);
        }
        return new SXObj();
    }

    @Nonnull
    public static SXObj getOrCreateDictionaryItem(@Nonnull String clazz, @Nonnull String keyField, @Nonnull Object keyValue) {
        SXObj dictionaryItem = getDictionaryItem(clazz, keyField, keyValue);
        if (dictionaryItem == null) {
            dictionaryItem = createDictionaryItem(clazz, keyField, keyValue);
        }
        return dictionaryItem;
    }

    public static class RguRef {
        public BigInteger id;
        public String type;

        public RguRef(BigInteger id, String type) {
            this.id = id;
            this.type = type;
        }
    }

    @Nullable
    public static SXId createCmsFile(List<Serializable> fileData, String className, String attrName) {
        SXAttr attr = SXDsFactory.getDs().getClass(className).getAttr(attrName);
        if (attr == null) {
            throw new RguTransformationRuntimeException("�� ������ ������� " + attrName + " � ������ " + className);
        }
        SXId selectRootId = attr.getSelectRootId();
        if (selectRootId == null) {
            throw new RguTransformationRuntimeException("�� ������ ��������� ����� ������ � �������� " + attrName + " � ������ " + className);
        }
        SXDs folderDs = selectRootId.getSXClass().getDs();
        if (!selectRootId.getSXClass().isCompatible(SXDsFactory.getDs().getClass(SXMetaConsts.SXFOLDER_CLASS))
                && !(folderDs instanceof SXDsFs)) {
            throw new RguTransformationRuntimeException("������� ��������� ����� ������ � �������� " + attrName + " � ������ " + className + " �� �������� ������");
        }
        if (!(folderDs instanceof SXDsFs)) {
            Folder folder;
            try {
                folder = CmsActionUtils.getSXFolder(selectRootId);
            } catch (Exception e) {
                throw new RguTransformationRuntimeException("������ ���������� " + selectRootId + " ��� �����", e);
            }
            String dsCode = folder.getExtDs() != null ? folder.getExtDsName() : null;
            if (dsCode != null) {
                if (SXDsFactory.getDs(dsCode) instanceof SXDsFs) {
                    selectRootId = folder.getExtLink();
                }
            }
        }
        String fileName = null;
        byte[] fileBody = null;
        for (Object fileDataObj : fileData) {
            if (fileDataObj instanceof JAXBElement) {
                JAXBElement fileElement = (JAXBElement) fileDataObj;
                if ("fileName".equals(fileElement.getName().getLocalPart()) && fileElement.getValue() != null) {
                    fileName = fileElement.getValue().toString();
                }
                if ("blobData".equals(fileElement.getName().getLocalPart())) {
                    fileBody = (byte[]) fileElement.getValue();
                }
            }
        }
        SXCreateObjParams crParams = new SXCreateObjParams(selectRootId.getDs(), SXMetaConsts.CMS_FILE_CLASS);
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(SXMetaConsts.CMS_FILE_PARENT_ATTR, selectRootId);
        dataMap.put(SXMetaConsts.CMS_FILE_NAME_ATTR, fileName);
        dataMap.put(SXMetaConsts.CMS_FILE_BODY_ATTR, fileBody);
        crParams.setData(dataMap);
        try {
            return crParams.createObj().getId();
        } catch (Exception e) {
            throw new RguTransformationRuntimeException("������ ��� �������� ����� " + fileData, e);
        }
        return null;
    }

    @Nullable
    public static SXId createCmsFile(URL downloadUrl, String className, String attrName, SXObj pprDocObj) throws Exception {
        ByteArrayOutputStream bais = new ByteArrayOutputStream();
        InputStream is = null;
        byte[] bytes;
        try {
            is = downloadUrl.openStream();
            bytes = new byte[4096];
            int n;
            while ((n = is.read(bytes)) > 0) {
                bais.write(bytes, 0, n);
            }
        } catch (IOException e) {
            log.error("Failed while reading bytes from %s: %s", downloadUrl.toExternalForm(), e.getMessage());
        } finally {
            if (is != null) {
                is.close();
            }
        }
        byte[] fileData = bais.toByteArray();
        SXAttr attr = SXDsFactory.getDs().getClass(className).getAttr(attrName);
        SXId selectRootId = attr.getSelectRootId();
        SXCreateObjParams crParams = new SXCreateObjParams(selectRootId.getDs(), SXMetaConsts.CMS_FILE_CLASS);
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(SXMetaConsts.CMS_FILE_PARENT_ATTR, selectRootId);
        dataMap.put(SXMetaConsts.CMS_FILE_NAME_ATTR, pprDocObj.getStringAttr("rguDocName"));
        dataMap.put(SXMetaConsts.CMS_FILE_LENGTH_ATTR, pprDocObj.getIntegerAttr("rguDocSize"));
        dataMap.put(SXMetaConsts.CMS_FILE_BODY_ATTR, fileData);
        crParams.setData(dataMap);
        return crParams.createObj().getId();
    }

    public static Date convertDate(String stringDate) throws RguTransformationException {
        if (stringDate != null) {
            SimpleDateFormat dateFormat = SXDateFormatStore.getDateFormat("yyyy-MM-dd hh:mm:ss.S");
            try {
                Date date = dateFormat.parse(stringDate);
                return date;
            } catch (ParseException e) {
                throw new RguTransformationException("������ ��� ����������� ���� " + stringDate, e);
            }
        }
        return null;
    }

    public static boolean containsCyrillic(String string) {
        for (char c : string.toCharArray()) {
            if (Character.UnicodeBlock.CYRILLIC.equals(Character.UnicodeBlock.of(c))) {
                return true;
            }
        }

        return false;
    }

    public static String updateCode(SXId parentId, String code) {
        EgClassification parent = null;
        String parentCode = null;

        if (parentId != null) {
            try {
                parent = (EgClassification) parentId.getObj(EgClassification.CODE_ATTR);
            } catch (Exception e) {
                return code;
            }

            if (parent != null) {
                parentCode = parent.getCode();
            }
        }

        if (code != null) {
            if (parentCode != null) {
                code = parentCode + "." + code;
            }
        }

        return code;
    }

    public static void updateClassification(String codeDictRgu, String codeReqDicRgu) {
        SXObj obj = getDictionaryItem("egClassification", "codeDictRgu", codeDictRgu);
        if (obj != null) {
            try {
                obj.setReadOnly(false);
                SXUpdateObjParams params = new SXUpdateObjParams(obj.getId());
                params.setUpdateReadOnlyAttr(true);
                Map<String, Object> data = new HashMap<String, Object>();
                data.put("codeReqDicRgu", codeReqDicRgu);
                params.setData(data);
                params.updateObj();
            } catch (Exception e) {
                log.error("ERROR updateClassification: codeReqDicRgu = " + codeReqDicRgu);
                log.error(e.getMessage(), e);
            }
        }
    }

    private static Map<String, SXId> classificationsCached = new HashMap<String, SXId>();
    private static Map<SXId, Collection<SXId>> descendantsCached = new HashMap<SXId, Collection<SXId>>();

    public static SXId findClassification(String clazz, String code) {
        return findClassification(clazz, code, true);
    }

    /**
     * @param clazz
     * @param code
     * @param codeEndsWith - ���� �� ��������� ���� ������� � ���������� ������ �������� (��� �����)
     * @return
     */
    public static SXId findClassification(String clazz, String code, boolean codeEndsWith) {
        try {
            log.error("���� classification " + clazz + " " + code);

            if (classificationsCached.containsKey(clazz + "_" + code)) {
                return classificationsCached.get(clazz + "_" + code);
            }

            SXObj root = getDictionaryItem("egClassification", "codeReqDicRgu", clazz);
            Collection<SXId> descendants;
            if (codeEndsWith) {
                descendants = getDescendants(root.getId(), false);
            } else {
                descendants = getDescendants(root.getId());
            }

            SXClass cls = SXDsFactory.getDs().getClass("egClassification");
            SXObjListParams params = new SXObjListParams(cls);
            if (codeEndsWith) {
                params.addCondition("cod", "%" + code, SqlEnum.LIKE);
            } else {
                params.addCondition("cod", code);
            }
            params.addCondition(cls.getSimpleKeyAttr(), descendants, SqlEnum.IN);
            SXId sxId = params.getObj().getId();
            classificationsCached.put(clazz + "_" + code, sxId);
            return sxId;
        } catch (Exception e) {
            log.error("�� ������ ������ �������������� " + clazz + "_" + code, e);
            return null;
        }
    }

    private static Collection<SXId> getDescendants(SXId root) {
        return getDescendants(root, true);
    }

    private static Collection<SXId> getDescendants(SXId root, boolean withRoot) {
        if (descendantsCached.containsKey(root)) {
            return descendantsCached.get(root);
        }

        Collection<SXId> descendants = new HashSet<SXId>();
        if (withRoot) {
            descendants.add(root);
        }

        try {
            SXObjListParams params = new SXObjListParams("egClassification");
            params.addCondition("parent", root);
            SXObjList objList = params.getObjList();

            if (objList != null) {
                List<SXObj> deptList = objList.getSXObjList();
                for (SXObj dept : deptList) {
                    descendants.addAll(getDescendants(dept.getId()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        Collection<SXId> tmp = new ArrayList<SXId>(descendants);
        tmp.add(root);
        descendantsCached.put(root, tmp);

        return descendants;
    }

    public static boolean downloadFile(String fileUrl, String outputFileName) {
        try {
            URL url = new URL(fileUrl);
            ReadableByteChannel channel = Channels.newChannel(url.openStream());
            FileOutputStream out = new FileOutputStream(outputFileName);
            out.getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public static SXObj getPprDocByRguId(BigInteger documentRguId) throws Exception {
        SXObjListParams olParams = new SXObjListParams("pprDoc");
        olParams.addCondition("idRgu", documentRguId.toString())
                .addSelectedAttr(SXClass.ALL_ATTRS)
                .addCondition("idDocRgu", null, SqlEnum.ISNOTNULL)
                .addCondition("rguDocName", null, SqlEnum.ISNOTNULL);
        return olParams.getObj();
    }

    /**
     * ����� ������������ ��� ���������� � �������� cmsFile
     *
     * @param workDocumentRguId rguId workDocument'a
     * @return cmsFile
     * @throws RguTransformationException
     */
    public static SXId downloadFileAndCreateCmsFile(String workDocumentRguId, String className, String attrName) throws RguTransformationException {
        BigInteger documentRguId = RguTransformationUtils.parseRef(workDocumentRguId).id;
        if (documentRguId.equals(BigInteger.ZERO)) {
            return null;
        }
        SXObj pprDocObj;
        try {
            pprDocObj = RguTransformationUtils.getPprDocByRguId(documentRguId);
        } catch (Exception e) {
            throw new RguTransformationException(e.getMessage(), e);
        }
        if (pprDocObj == null) {
            return null;
        }
        String idDocRgu = pprDocObj.getStringAttr("idDocRgu");
        if (Strings.isNullOrEmpty(idDocRgu)) {
            return null;
        }
        String downloadUrl = "http://213.85.255.111:8080/RGU_WAR_2/servlet.gdnld?dfid=" + idDocRgu;
        URL url;
        try {
            url = new URL(downloadUrl);
            url.openStream();
        } catch (Exception e) {
            log.error("Error downloading file from URL: " + downloadUrl);
            return null;
        }
        try {
            return RguTransformationUtils.createCmsFile(url, className, attrName, pprDocObj);
        } catch (Exception e) {
            throw new RguTransformationException(e.getMessage(), e);
        }
    }

    public static String cutFileName(String fileName) {
        if (fileName.length() > 100 + 1 + FilenameUtils.getExtension(fileName).length()) {
            return FilenameUtils.getBaseName(fileName).substring(0, 100) + "." + FilenameUtils.getExtension(fileName);
        }

        return fileName;
    }
}
