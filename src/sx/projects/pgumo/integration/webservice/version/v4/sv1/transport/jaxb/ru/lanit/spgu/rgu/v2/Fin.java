package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for fin complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="fin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="financialDetailsStructured" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="receiverFullTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="receiverShortTitle" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="receiverINN" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="receiverKPP" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="receiverBankTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="receiverBankBIC" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="receiverCorrespAcc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="receiverAcc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="receiverOKATO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="receiverOKTMO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fin", propOrder = {
        "financialDetailsStructured"
})
public class Fin {

    protected List<FinancialDetailsStructured> financialDetailsStructured;

    /**
     * Gets the value of the financialDetailsStructured property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialDetailsStructured property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialDetailsStructured().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin.FinancialDetailsStructured }
     */
    public List<FinancialDetailsStructured> getFinancialDetailsStructured() {
        if (financialDetailsStructured == null) {
            financialDetailsStructured = new ArrayList<FinancialDetailsStructured>();
        }
        return this.financialDetailsStructured;
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="receiverFullTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receiverShortTitle" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="receiverINN" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="receiverKPP" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="receiverBankTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receiverBankBIC" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="receiverCorrespAcc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receiverAcc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="receiverOKATO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="receiverOKTMO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "receiverFullTitle",
            "receiverShortTitle",
            "receiverINN",
            "receiverKPP",
            "receiverBankTitle",
            "receiverBankBIC",
            "receiverCorrespAcc",
            "receiverAcc",
            "receiverOKATO",
            "receiverOKTMO"
    })
    public static class FinancialDetailsStructured {

        @XmlElement(required = true)
        protected String receiverFullTitle;
        @XmlElement(required = true)
        protected BigInteger receiverShortTitle;
        protected long receiverINN;
        protected int receiverKPP;
        @XmlElement(required = true)
        protected String receiverBankTitle;
        protected int receiverBankBIC;
        @XmlElement(required = true)
        protected String receiverCorrespAcc;
        @XmlElement(required = true)
        protected BigInteger receiverAcc;
        @XmlElement(required = true)
        protected String receiverOKATO;
        @XmlElement(required = true)
        protected String receiverOKTMO;

        /**
         * Gets the value of the receiverFullTitle property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReceiverFullTitle() {
            return receiverFullTitle;
        }

        /**
         * Sets the value of the receiverFullTitle property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReceiverFullTitle(String value) {
            this.receiverFullTitle = value;
        }

        /**
         * Gets the value of the receiverShortTitle property.
         *
         * @return possible object is
         * {@link java.math.BigInteger }
         */
        public BigInteger getReceiverShortTitle() {
            return receiverShortTitle;
        }

        /**
         * Sets the value of the receiverShortTitle property.
         *
         * @param value allowed object is
         *              {@link java.math.BigInteger }
         */
        public void setReceiverShortTitle(BigInteger value) {
            this.receiverShortTitle = value;
        }

        /**
         * Gets the value of the receiverINN property.
         */
        public long getReceiverINN() {
            return receiverINN;
        }

        /**
         * Sets the value of the receiverINN property.
         */
        public void setReceiverINN(long value) {
            this.receiverINN = value;
        }

        /**
         * Gets the value of the receiverKPP property.
         */
        public int getReceiverKPP() {
            return receiverKPP;
        }

        /**
         * Sets the value of the receiverKPP property.
         */
        public void setReceiverKPP(int value) {
            this.receiverKPP = value;
        }

        /**
         * Gets the value of the receiverBankTitle property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReceiverBankTitle() {
            return receiverBankTitle;
        }

        /**
         * Sets the value of the receiverBankTitle property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReceiverBankTitle(String value) {
            this.receiverBankTitle = value;
        }

        /**
         * Gets the value of the receiverBankBIC property.
         */
        public int getReceiverBankBIC() {
            return receiverBankBIC;
        }

        /**
         * Sets the value of the receiverBankBIC property.
         */
        public void setReceiverBankBIC(int value) {
            this.receiverBankBIC = value;
        }

        /**
         * Gets the value of the receiverCorrespAcc property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReceiverCorrespAcc() {
            return receiverCorrespAcc;
        }

        /**
         * Sets the value of the receiverCorrespAcc property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReceiverCorrespAcc(String value) {
            this.receiverCorrespAcc = value;
        }

        /**
         * Gets the value of the receiverAcc property.
         *
         * @return possible object is
         * {@link java.math.BigInteger }
         */
        public BigInteger getReceiverAcc() {
            return receiverAcc;
        }

        /**
         * Sets the value of the receiverAcc property.
         *
         * @param value allowed object is
         *              {@link java.math.BigInteger }
         */
        public void setReceiverAcc(BigInteger value) {
            this.receiverAcc = value;
        }

        /**
         * Gets the value of the receiverOKATO property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReceiverOKATO() {
            return receiverOKATO;
        }

        /**
         * Sets the value of the receiverOKATO property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReceiverOKATO(String value) {
            this.receiverOKATO = value;
        }

        /**
         * Gets the value of the receiverOKTMO property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getReceiverOKTMO() {
            return receiverOKTMO;
        }

        /**
         * Sets the value of the receiverOKTMO property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setReceiverOKTMO(String value) {
            this.receiverOKTMO = value;
        }

    }

}
