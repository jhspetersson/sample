package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.datastore.SXObjList;
import sx.datastore.meta.SXClass;
import sx.datastore.params.SXObjListParams;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.*;

public class PsPassportTransformer<T extends PsPassport> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(PsPassportTransformer.class);

    public PsPassportTransformer(RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Override
    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T passport) throws RguTransformationException {
        return RguTransformationUtils.parseRef(passport.getId()).id;
    }

    @Override
    @Nonnull
    protected String getObjTitle(@Nonnull T passport) {
        return passport.getShortTitle();
    }


    @Nonnull
    @Override
    protected Collection<String> getAttrsForCascadeDel() {
        return Arrays.asList(new String[]{"procedure"});
    }

    protected String getSxClassName() {
        return "egService";
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T passport, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(passport, getSxClassName(), callback);
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Override
    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T passport, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(getRguIdAttrName(), transformObj(passport.getStatusInfo(), callback).getId());
        map.put("isPublish", isPublished(passport));
        map.put("name", passport.getShortTitle());
        map.put("fullname", passport.getFullTitle());
        map.put("popularname", passport.getShortTitle());
        map.put("actsNpa", getOffDocsIds(passport, callback));
        map.put("procedure", getStepsIds(passport, callback));
        if (passport.getResponsibleOrganization() != null && !Strings.isNullOrEmpty(passport.getResponsibleOrganization().getRef())) {
            Object transformedObject = callback.getTransformedObject(passport.getResponsibleOrganization().getRef());
            if (transformedObject instanceof SXObj) {
                map.put("mainOrgan", ((SXObj) transformedObject).getId());
            }
        }
        map.put("BASE_INFO", passport.getDescription());
        map.put("resultDescription", passport.getResult());
        map.put("classification", getClassification(passport));
        map.put("price", passport.getPaymentDescription());
        map.put("result", passport.getResult());
        map.put("functioncontrol", "true".equals(passport.getIsFunction()));

        List<SXId> terrOrgans = getParticipants(passport, 602, callback);
        map.put("terrOrgans", terrOrgans);
        try {
            map.put("offices", getOfficesByTerrOrgans(terrOrgans));
        } catch (Exception e) {
            throw new RguTransformationException(e.getMessage(), e);
        }
        map.put("partOrgans", getParticipants(passport, 603, callback));
        map.put("org_consultation", getParticipants(passport, 600, callback));
        map.put("org_control", getParticipants(passport, 604, callback));
        map.put("org_complain", getParticipants(passport, 601, callback));

        map.put("informRules", passport.getInfoDescription());
        map.put("appealRules", passport.getAppealDescription());
        map.put("keywords", passport.getKeywords().getKeyword());
        map.put("about_advising", passport.getInfoDescription());
        map.put("appeal_Information", passport.getAppealDescription());

        if (passport.getReglament() != null) {
            String regText = passport.getReglament().getReglamentText();
            if (regText != null) {
                map.put("regText", regText);
            }
        }

        map.put("grounds_refusal", passport.getRefusalDescription());
        PsPassport.StatusInfo statusInfo = passport.getStatusInfo();
        if (statusInfo != null) {
            map.put("statusRGU", statusInfo.getPublished());
        }

        map.put("doc", getWorkDocumentIds(passport, callback));

        if (passport.getAdministrativeLevel() != null) {
            map.put("level", passport.getAdministrativeLevel().getValue());
        }

        return map;
    }

    private List<SXId> getOfficesByTerrOrgans(List<SXId> terrOrgans) throws Exception {
        if (terrOrgans != null) {
            List<SXId> result = new ArrayList<SXId>();
            for (SXId terrOrgan : terrOrgans) {
                SXObjList offices = new SXObjListParams("egOffice")
                        .addSelectedAttr(SXClass.NONE_ATTRS)
                        .setCheckSecurity(false)
                        .setRunHandlers(false)
                        .addCondition("org", terrOrgan)
                        .getObjList();
                if (offices != null) {
                    List<SXId> sxIdList = offices.getSXIdList();
                    if (sxIdList != null) {
                        result.addAll(sxIdList);
                    }
                }
            }
            if (!result.isEmpty()) {
                return result;
            }
        }
        return null;
    }

    private static Collection<String> PUBLISHED_STATUSES = new HashSet<String>();

    static {
        PUBLISHED_STATUSES.add("PUBLISHED");
        PUBLISHED_STATUSES.add("EDITED");
        PUBLISHED_STATUSES.add("EDITED_REVIEW");
        PUBLISHED_STATUSES.add("EDITED_REJECTED");
        PUBLISHED_STATUSES.add("EDITED_EXPERTIZE");
        PUBLISHED_STATUSES.add("EDITED_PUBLISH_REJECTED");
        PUBLISHED_STATUSES.add("DELETE_PROPOSED");
    }

    private boolean isPublished(T passport) {
        return PUBLISHED_STATUSES.contains(passport.getStatusInfo().getPublished());
    }

    @Nonnull
    private List<SXId> getOffDocsIds(@Nonnull T passport, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        PsPassport.OffDocs offDocs = passport.getOffDocs();
        if (offDocs == null || offDocs.getPsPassport2OffDoc().isEmpty()) {
            return Collections.emptyList();
        }
        List<SXId> offDocsIds = new ArrayList<SXId>(offDocs.getPsPassport2OffDoc().size());
        for (PsPassport.OffDocs.PsPassport2OffDoc offDoc : offDocs.getPsPassport2OffDoc()) {
            offDocsIds.add(transformObj(offDoc.getOffDoc(), callback).getId());
        }
        return offDocsIds;
    }

    @Nonnull
    private List<SXId> getStepsIds(@Nonnull T passport, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Services services = passport.getServices();
        if (services == null || services.getPassportServices().isEmpty()) {
            return Collections.emptyList();
        }
        List<SXId> stepsIds = new ArrayList<SXId>(passport.getServices().getPassportServices().size());
        for (Services.PassportServices passportServices : services.getPassportServices()) {
            Services.PassportServices.Service service = passportServices.getService();
            SXObj sxObj = transformObj(service, callback);
            if (sxObj != null) {
                stepsIds.add(sxObj.getId());
            }
        }
        return stepsIds;
    }

    private List<SXId> getWorkDocumentIds(@Nonnull T passport, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        List<SXId> workDocumentIds = new ArrayList<SXId>();

        if (passport.getWorkDocuments() != null) {
            List<PsPassport.WorkDocuments.PassportWorkDocuments> passportWorkDocuments = passport.getWorkDocuments().getPassportWorkDocuments();
            for (PsPassport.WorkDocuments.PassportWorkDocuments passportWorkDocument : passportWorkDocuments) {
                PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument workDocument = passportWorkDocument.getWorkDocument();
                if (workDocument != null) {
                    SXObj sxObj = transformObj(workDocument, callback);
                    if (sxObj != null) {
                        workDocumentIds.add(sxObj.getId());
                    }
                }
            }
        }

        return workDocumentIds;
    }

    private List<SXId> getClassification(T passport) {
        Set<SXId> classification = new HashSet<SXId>();

        try {
            String ref = passport.getAdministrativeLevel().getRef();
            RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
            SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
            if (sxId != null) {
                classification.add(sxId);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            String ref = passport.getCatalogSection().getRef();
            RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
            SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
            if (sxId != null) {
                classification.add(sxId);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            for (Services.PassportServices passportServices : passport.getServices().getPassportServices()) {
                Services.PassportServices.Service service = passportServices.getService();
                for (Services.PassportServices.Service.ServiceTargets.ServiceTarget serviceTarget : service.getServiceTargets().getServiceTarget()) {
                    try {
                        String ref = serviceTarget.getRecipientCategories().getService2RRecipientCategory().getRecipientCategory().getRef();
                        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
                        SXId sxId = RguTransformationUtils.findClassification(rguRef.type, rguRef.id.toString());
                        if (sxId != null) {
                            classification.add(sxId);
                        }
                    } catch (Exception e) {
                        log.error("ref1", e);
                    }

                    try {
                        if (serviceTarget.getSubjectRPGU() != null) {
                            String ref2 = serviceTarget.getSubjectRPGU().getRef();
                            RguTransformationUtils.RguRef rguRef2 = RguTransformationUtils.parseRef(ref2);
                            if (rguRef2.id != null && rguRef2.id.compareTo(BigInteger.ZERO) > 0) {
                                SXId sxId2 = RguTransformationUtils.findClassification(rguRef2.type, rguRef2.id.toString());
                                if (sxId2 != null) {
                                    classification.add(sxId2);
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error("������������ �������� �������� subjectRPGU!");
                    }

                    try {
                        if (serviceTarget.getRecipientCategoriesRPGU() != null && serviceTarget.getRecipientCategoriesRPGU().getService2RRecipientCategoryRPGU() != null
                                && serviceTarget.getRecipientCategoriesRPGU().getService2RRecipientCategoryRPGU().getRecipientCategory() != null) {
                            String ref3 = serviceTarget.getRecipientCategoriesRPGU().getService2RRecipientCategoryRPGU().getRecipientCategory().getRef();
                            RguTransformationUtils.RguRef rguRef3 = RguTransformationUtils.parseRef(ref3);
                            SXId sxId3 = RguTransformationUtils.findClassification(rguRef3.type, rguRef3.id.toString());
                            if (sxId3 != null) {
                                classification.add(sxId3);
                            }
                        }
                    } catch (Exception e) {
                        log.error("ref3", e);
                    }

                    try {
                        if (serviceTarget.getLifeEventsRPGU() != null && serviceTarget.getLifeEventsRPGU().getLifeEventRPGU2Service() != null && serviceTarget.getLifeEventsRPGU().getLifeEventRPGU2Service().getLifeEvent() != null) {
                            String ref4 = serviceTarget.getLifeEventsRPGU().getLifeEventRPGU2Service().getLifeEvent().getRef();
                            RguTransformationUtils.RguRef rguRef4 = RguTransformationUtils.parseRef(ref4);
                            SXId sxId4 = RguTransformationUtils.findClassification(rguRef4.type, rguRef4.id.toString());
                            if (sxId4 != null) {
                                classification.add(sxId4);
                            }
                        }
                    } catch (Exception e) {
                        log.error("ref4", e);
                    }

                    try {
                        if (serviceTarget.getLifeEvents() != null && serviceTarget.getLifeEvents().getLifeEvent2Service() != null && serviceTarget.getLifeEvents().getLifeEvent2Service().getLifeEvent() != null) {
                            String ref5 = serviceTarget.getLifeEvents().getLifeEvent2Service().getLifeEvent().getRef();
                            RguTransformationUtils.RguRef rguRef5 = RguTransformationUtils.parseRef(ref5);
                            SXId sxId5 = RguTransformationUtils.findClassification(rguRef5.type, rguRef5.id.toString());
                            if (sxId5 != null) {
                                classification.add(sxId5);
                            }
                        }
                    } catch (Exception e) {
                        log.error("ref5", e);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return new ArrayList<SXId>(classification);
    }

    @Nonnull
    private List<SXId> getParticipants(T passport, int code, RguTransformerCallback callback) {
        List<SXId> result = new ArrayList<SXId>();

        if (passport.getParticipants() != null && passport.getParticipants().getPaticipant() != null) {
            for (PsPassport.Participants.Paticipant paticipant : passport.getParticipants().getPaticipant()) {
                try {
                    String ref = paticipant.getParticipantTypes().getParticipantType().getRef();
                    RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(ref);
                    if (rguRef != null && rguRef.id.intValue() == code) {
                        String orgRef = paticipant.getOrganization().getRef();
                        SXObj obj = (SXObj) callback.getTransformedObject(orgRef);
                        if (obj != null) {
                            result.add(obj.getId());
                        }
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        return result;
    }
}
