package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.*;

public class ServiceTransformer<T extends Service> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(ServiceTransformer.class);

    public ServiceTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    protected Collection<String> getAttrsForCascadeDel() {
        return Arrays.asList(new String[]{"refusalReason", "targets"});
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T service) throws RguTransformationException {
        return RguTransformationUtils.parseRef(service.getId()).id;
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T service) {
        return service.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T service, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("title", service.getTitle());
        map.put("reason", service.getGroundForAction());
        map.put("result", service.getResult());
        map.put("refusalReason", getGroundsOfRefusalIds(service, callback));
        map.put("idRgu", getObjRguId(service));
        map.put("requestForm", getCommunicationFormsIds(service, "REQUEST", callback));
        map.put("ansForm", getCommunicationFormsIds(service, "RESPONSE", callback));
        map.put("targets", getServiceTargetsIds(service, callback));
        map.put("isGroundless", "true".equals(service.getIsGroundless()));
        map.put("free_comments", service.getComments());
        map.put("description", service.getGroundForAction());
        map.put("actsNpa", getServiceOffDocsIds(service, callback));
        map.put("egFaqs", getFaqs(service, callback));

        return map;
    }


    @Nonnull
    public List<SXId> getServiceOffDocsIds(@Nonnull T service, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        if (service.getServiceOffDoc().isEmpty()) {
            return Collections.emptyList();
        }
        List<SXId> serviceOffDocsIds = new ArrayList<SXId>(service.getServiceOffDoc().size());
        for (Service.ServiceOffDoc serviceOffDoc : service.getServiceOffDoc()) {
            serviceOffDocsIds.add(transformObj(serviceOffDoc, callback).getId());
        }
        return serviceOffDocsIds;
    }

    @Nonnull
    public List<SXId> getGroundsOfRefusalIds(@Nonnull T service, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Service.GroundsOfRefusal groundsOfRefusal = service.getGroundsOfRefusal();
        if (groundsOfRefusal == null || groundsOfRefusal.getGroundOfRefusal().isEmpty()) {
            return Collections.emptyList();
        }
        List<SXId> groundsOfRefusalIds = new ArrayList<SXId>(groundsOfRefusal.getGroundOfRefusal().size());
        for (Service.GroundsOfRefusal.GroundOfRefusal groundOfRefusal : groundsOfRefusal.getGroundOfRefusal()) {
            groundsOfRefusalIds.add(transformObj(groundOfRefusal, callback).getId());
        }
        return groundsOfRefusalIds;
    }

    @Nonnull
    public List<SXId> getCommunicationFormsIds(@Nonnull T service, @Nonnull String type,
                                               @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Service.CommunicationForms communicationForms = service.getCommunicationForms();
        if (communicationForms == null || communicationForms.getCommunicationForm().isEmpty()) {
            return Collections.emptyList();
        }
        List<SXId> communicationFormsIds = new ArrayList<SXId>(communicationForms.getCommunicationForm().size());
        for (Service.CommunicationForms.CommunicationForm communicationForm : communicationForms.getCommunicationForm()) {
            if (type.equals(communicationForm.getCommunicationAction())) {
                communicationFormsIds.add(transformObj(communicationForm, callback).getId());
            }
        }
        return communicationFormsIds;
    }

    @Nonnull
    public List<SXId> getServiceTargetsIds(@Nonnull T service, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        List<SXId> serviceTargetsIds = new ArrayList<SXId>(service.getServiceTargets().getServiceTarget().size());
        for (Service.ServiceTargets.ServiceTarget serviceTarget : service.getServiceTargets().getServiceTarget()) {
            serviceTargetsIds.add(transformObj(serviceTarget, callback).getId());
        }
        return serviceTargetsIds;
    }

    @Nonnull
    public List<SXId> getFaqs(@Nonnull T service, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        List<SXId> faqs = new ArrayList<SXId>();

        try {
            if (service.getFaqs() != null && service.getFaqs().getFaq() != null) {
                for (Service.Faqs.Faq faq : service.getFaqs().getFaq()) {
                    faqs.add(transformObj(faq, callback).getId());
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return faqs;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T service, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return createSXObj(service, "egProcedureStep", callback, true);
    }
}
