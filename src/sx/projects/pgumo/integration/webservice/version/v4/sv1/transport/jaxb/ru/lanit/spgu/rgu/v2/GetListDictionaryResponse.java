package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectRefList"
})
@XmlRootElement(name = "getListDictionaryResponse")
public class GetListDictionaryResponse {
    @XmlElement(name = "ObjectRefList", required = true)
    protected ObjectRefList objectRefList;

    public ObjectRefList getObjectRefList() {
        return objectRefList;
    }

    /**
     * Sets the value of the objectRefList property.
     *
     * @param value allowed object is
     *              {@link ObjectRefList }
     */
    public void setObjectRefList(ObjectRefList value) {
        this.objectRefList = value;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "list"
    })
    public static class ObjectRefList {
        @XmlElement(namespace = "", required = true)
        protected List list;

        /**
         * Gets the value of the list property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List }
         */
        public List getList() {
            return list;
        }

        /**
         * Sets the value of the list property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List }
         */
        public void setList(List value) {
            this.list = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "objectRef"
        })
        public static class List {

            @XmlElement(name = "ObjectRef", namespace = "", required = true)
            protected java.util.List<ObjectRef> objectRef;

            /**
             * Gets the value of the objectRef property.
             * <p/>
             * <p/>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the objectRef property.
             * <p/>
             * <p/>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getObjectRef().add(newItem);
             * </pre>
             * <p/>
             * <p/>
             * <p/>
             * Objects of the following type(s) are allowed in the list
             * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetChangesResponse.ObjectRefList.List.ObjectRef }
             */
            public java.util.List<ObjectRef> getObjectRef() {
                if (objectRef == null) {
                    objectRef = new ArrayList<ObjectRef>();
                }
                return this.objectRef;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "id",
                    "name",
                    "description"
            })
            public static class ObjectRef {

                @XmlElement(namespace = "", required = true)
                @XmlJavaTypeAdapter(value = BigIntegerAdapter.class, type = BigInteger.class)
                @XmlSchemaType(name = "id")
                protected BigInteger id;
                @XmlElement(namespace = "", required = true)
                protected String name;
                @XmlElement(namespace = "", required = true)
                protected String description;


                public BigInteger getId() {
                    return id;
                }

                public void setId(BigInteger id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }
            }
        }
    }
}
