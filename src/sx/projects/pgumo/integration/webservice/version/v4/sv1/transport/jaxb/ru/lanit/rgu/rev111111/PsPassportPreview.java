package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for psPassportPreview complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="psPassportPreview">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="psPassportId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="generationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="loadingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "psPassportPreview", propOrder = {
        "psPassportId",
        "url",
        "generationDate",
        "loadingDate"
})
public class PsPassportPreview
        extends Request {

    protected long psPassportId;
    @XmlElement(required = true)
    protected String url;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar generationDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar loadingDate;

    /**
     * Gets the value of the psPassportId property.
     */
    public long getPsPassportId() {
        return psPassportId;
    }

    /**
     * Sets the value of the psPassportId property.
     */
    public void setPsPassportId(long value) {
        this.psPassportId = value;
    }

    /**
     * Gets the value of the url property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the generationDate property.
     *
     * @return possible object is
     * {@link javax.xml.datatype.XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getGenerationDate() {
        return generationDate;
    }

    /**
     * Sets the value of the generationDate property.
     *
     * @param value allowed object is
     *              {@link javax.xml.datatype.XMLGregorianCalendar }
     */
    public void setGenerationDate(XMLGregorianCalendar value) {
        this.generationDate = value;
    }

    /**
     * Gets the value of the loadingDate property.
     *
     * @return possible object is
     * {@link javax.xml.datatype.XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getLoadingDate() {
        return loadingDate;
    }

    /**
     * Sets the value of the loadingDate property.
     *
     * @param value allowed object is
     *              {@link javax.xml.datatype.XMLGregorianCalendar }
     */
    public void setLoadingDate(XMLGregorianCalendar value) {
        this.loadingDate = value;
    }

}
