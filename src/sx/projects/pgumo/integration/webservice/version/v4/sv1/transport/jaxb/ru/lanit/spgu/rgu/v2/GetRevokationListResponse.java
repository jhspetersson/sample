package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectRefList"
})
@XmlRootElement(name = "getRevokationListResponse")
public class GetRevokationListResponse {
    @XmlElement(name = "ObjectRefList", required = true)
    protected ObjectRefList objectRefList;

    public ObjectRefList getObjectRefList() {
        return objectRefList;
    }

    public void setObjectRefList(ObjectRefList value) {
        this.objectRefList = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "currentSsn",
            "list"
    })
    public static class ObjectRefList {

        @XmlElement(namespace = "", required = true)
        protected BigInteger currentSsn;
        @XmlElement(namespace = "", required = true)
        protected List list;

        public BigInteger getCurrentSsn() {
            return currentSsn;
        }

        public void setCurrentSsn(BigInteger value) {
            this.currentSsn = value;
        }

        public List getList() {
            return list;
        }

        public void setList(List value) {
            this.list = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "objectRef"
        })
        public static class List {

            @XmlElement(name = "ObjectRef", namespace = "", required = true)
            protected java.util.List<ObjectRef> objectRef;

            public java.util.List<ObjectRef> getObjectRef() {
                if (objectRef == null) {
                    objectRef = new ArrayList<ObjectRef>();
                }
                return this.objectRef;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "objectType",
                    "objectId",
                    "objectSsn",
                    "currentStatus"
            })
            public static class ObjectRef {

                @XmlElement(namespace = "", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "NCName")
                protected String objectType;
                @XmlElement(namespace = "", required = true)
                protected BigInteger objectId;
                @XmlElement(namespace = "", required = true)
                protected BigInteger objectSsn;
                @XmlElement(namespace = "", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "NCName")
                protected String currentStatus;

                /**
                 * Gets the value of the objectType property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getObjectType() {
                    return objectType;
                }

                /**
                 * Sets the value of the objectType property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setObjectType(String value) {
                    this.objectType = value;
                }

                /**
                 * Gets the value of the objectId property.
                 *
                 * @return possible object is
                 * {@link java.math.BigInteger }
                 */
                public BigInteger getObjectId() {
                    return objectId;
                }

                /**
                 * Sets the value of the objectId property.
                 *
                 * @param value allowed object is
                 *              {@link java.math.BigInteger }
                 */
                public void setObjectId(BigInteger value) {
                    this.objectId = value;
                }

                /**
                 * Gets the value of the objectSsn property.
                 *
                 * @return possible object is
                 * {@link java.math.BigInteger }
                 */
                public BigInteger getObjectSsn() {
                    return objectSsn;
                }

                /**
                 * Sets the value of the objectSsn property.
                 *
                 * @param value allowed object is
                 *              {@link java.math.BigInteger }
                 */
                public void setObjectSsn(BigInteger value) {
                    this.objectSsn = value;
                }

                /**
                 * Gets the value of the currentStatus property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getCurrentStatus() {
                    return currentStatus;
                }

                /**
                 * Sets the value of the currentStatus property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setCurrentStatus(String value) {
                    this.currentStatus = value;
                }

            }
        }
    }
}
