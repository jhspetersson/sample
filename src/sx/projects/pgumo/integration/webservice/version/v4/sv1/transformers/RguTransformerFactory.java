package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import sx.projects.pgumo.integration.exceptions.RguTransformationFactoryException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformer;

import javax.annotation.Nonnull;

public interface RguTransformerFactory {

    public RguTransformer getTransformer(@Nonnull Object obj) throws RguTransformationFactoryException;

}
