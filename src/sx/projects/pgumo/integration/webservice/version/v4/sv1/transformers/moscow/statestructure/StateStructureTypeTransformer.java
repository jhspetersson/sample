package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.statestructure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType;

import javax.annotation.Nonnull;

public class StateStructureTypeTransformer<T extends StateStructureType> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(StateStructureTypeTransformer.class);

    public StateStructureTypeTransformer(RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    public SXObj transform(@Nonnull T type, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        String typeName = type.getValue();
        log.debug("����������� ��� ����������� " + typeName);
        return RguTransformationUtils.getOrCreateDictionaryItem("Type_org", "Type_org_name", typeName);
    }
}
