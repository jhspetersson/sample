package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.*;
import sx.smev.handlers.SignHandler;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.*;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.*;
import sx.smev.security.base.KeyPairPool;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.Node;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;


public class RegistryInfoService {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.RegistryInfoService.class);
    /**
     * ������
     */
    RguInfoService_Service service;
    /**
     * ��������
     */
    RguInfoService port;
    /**
     * ����� ������ �������
     */
    private CommonInfo commonInfo = new CommonInfo();
    /**
     * ������� ����
     */
    private SmevSign smevSign;

    /**
     * �����������
     *
     * @param wsdl WSDL �������
     */
    public RegistryInfoService(String wsdl) {
        service = new RguInfoService_Service(wsdl);
        addSmevSignHandler();
        port = service.getRguInfoServicePort();

    }

    /**
     * ���������� ����� ���������� �� ���������� �������
     *
     * @return ���������� � ���������� �������
     */
    public CommonInfo getCommonInfo() {
        return commonInfo;
    }

    /**
     * ���������� ���������� � ���������� ������� ������� � ������� ����
     *
     * @return ���������� �� �������
     */
    public SmevSign getSmevSign() {
        return smevSign;
    }

    /**
     * ������������� ���������� � ���������� ������� ������� � ������� ����
     *
     * @param smevSign ���������� �� �������
     */
    public void setSmevSign(SmevSign smevSign) {
        this.smevSign = smevSign;
    }

    /**
     * �������� ������� ��� ������������ ������ � ���
     */
    private void addSmevSignHandler() {
        if (getSmevSign() != null) {
            HandlerResolver handlerResolver = service.getHandlerResolver();
            if (handlerResolver == null) {
                service.setHandlerResolver(new HandlerResolver() {
                    List<Handler> handlers = new LinkedList<Handler>();

                    @Override
                    public List<Handler> getHandlerChain(PortInfo portInfo) {
                        return handlers;
                    }
                });
            }
            List<Handler> handlerChain = service.getHandlerResolver().getHandlerChain(null);
            SignHandler signHandler = new SignHandler(getSmevSign().getKeyPairPool());
            signHandler.setStoreAlias(getSmevSign().getStoreAlias());
            signHandler.setStorePassword(getSmevSign().getStorePassword());
            handlerChain.add(signHandler);
        }
    }

    /**
     * ���������� ������ �� �������� ������
     *
     * @param id ������������� �������� ������
     * @return ������� ������
     */
    public GetPsPassportResponse.PsPassport getPsPassport(long id) {
        GetPsPassport passportRequest = new GetPsPassport();
        passportRequest.setPsPassportId(id);
        BaseMessageType body = getBody(getPsPassportData(passportRequest));
        try {
            MessageDataType response = invoke(body);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {
                    return ((GetPsPassportResponse) any.get(0)).getPsPassport();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getPsPassport");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ���������� ������ �� �����������
     *
     * @param id ������������� �����������
     * @return �����������
     */
    public GetRStateStructureResponse.RStateStructure getRStateStructure(long id) {
        GetRStateStructure stateStructureRequest = new GetRStateStructure();
        stateStructureRequest.setRStateStructureId(id);
        BaseMessageType body = getBody(getRStateStructureData(stateStructureRequest));
        try {
            MessageDataType response = invoke(body);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {
                    return ((GetRStateStructureResponse) any.get(0)).getRStateStructure();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getRStateStructure");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ���������� ������ ��������� � ���� ���
     *
     * @param ssnFrom ����� �������, ������� � �������� �������� ���������
     * @return ��������� ������� � ������ ������� �� ������ ������� + 1000
     */
    public GetChangesResponse.ObjectRefList getChanges(long ssnFrom, long ssnTo) {
        log.error("getChanges : REVISION " + ssnFrom);
        GetChanges changesRequest = new GetChanges();
        SsnRange ssnRange = new SsnRange();
        ssnRange.setSsnFrom(ssnFrom);
        ssnRange.setSsnTo(ssnTo);
        changesRequest.setSSNInterval(ssnRange);
        BaseMessageType request = getBody(getChangesRequestData(changesRequest));
        try {
            MessageDataType response = invoke(request);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {

                    return ((GetChangesResponse) any.get(0)).getObjectRefList();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getChanges");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ���������� �������, ������� �������������
     *
     * @param ssnFrom ����� �������, ������� � �������� �������� ���������
     * @param ssnTo   ����� �������, �� ������� �������� ���������
     * @return ��������� ������� � ��������� ��������� �������
     */
    public GetRevokationListResponse.ObjectRefList getRevokationList(long ssnFrom, long ssnTo) {
        GetRevokationList changesRequest = new GetRevokationList();
        SsnRange ssnRange = new SsnRange();
        ssnRange.setSsnFrom(ssnFrom);
        ssnRange.setSsnTo(ssnTo);
        changesRequest.setSSNInterval(ssnRange);
        BaseMessageType request = getBody(getRevokationListRequestData(changesRequest));
        try {
            MessageDataType response = invoke(request);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {
                    return ((GetRevokationListResponse) any.get(0)).getObjectRefList();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getRevokationList");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ���������� ������ �������� ����� �����������
     *
     * @return ������ �����
     */
    public GetListDictionaryResponse.ObjectRefList getListDictionaries() {
        GetListDictionary listDictionary = new GetListDictionary();
        BaseMessageType request = getBody(getListDictionariesRequestData(listDictionary));
        try {
            MessageDataType response = invoke(request);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {
                    GetListDictionaryResponse dictResponse = (GetListDictionaryResponse) any.get(0);
                    return dictResponse.getObjectRefList();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getListDictionary");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ���������� ������ �������� � �������� ����� �����������
     *
     * @param code   ��� �������� �����
     * @param parent ID ������������� �������, ����� ������� ������������� ������ ������������ �����,
     *               ������� ��� ��������� �������� TERRITORY ������ ���������� �������
     * @return ������ ��������
     */
    public GetDictionaryResponse.ObjectRefList getDictionary(String code, String parent) {
        GetDictionary dictionary = new GetDictionary();
        dictionary.setTitle(code);
        dictionary.setParent(parent);
        BaseMessageType request = getBody(getDictionaryRequestData(dictionary));
        try {
            MessageDataType response = invoke(request);
            if ("SUCCESS".equals(response.getAppData().getResponse().getStatus())) {
                List<Object> any = response.getAppData().getResponse().getAny();
                if (any != null && !any.isEmpty()) {
                    GetDictionaryResponse dictResponse = (GetDictionaryResponse) any.get(0);
                    return dictResponse.getObjectRefList();
                }
            }
        } catch (Exception e) {
            log.error("������� � ������� ������ getDictionary");
            log.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * ��������� ������ � �������
     *
     * @param request ������ �������
     * @return ������ ������ �������
     */
    private MessageDataType invoke(BaseMessageType request) {
        BaseMessageType response = port.request(request);
        return response.getMessageData();
    }

    /**
     * ��������� ���� �������, ���������� �� ������ ������� ����� ����������
     *
     * @param appData ������������� ������ ��� �������
     * @return ���� ������
     */
    private BaseMessageType getBody(AppDataType appData) {
        BaseMessageType body = new BaseMessageType();
        MessageType messageType = new MessageType();
        OrgExternalType sender = new OrgExternalType();
        sender.setCode(getCommonInfo().getSenderCode());
        sender.setName(getCommonInfo().getSenderName());
        messageType.setSender(sender);
        OrgExternalType recipient = new OrgExternalType();
        recipient.setCode(getCommonInfo().getRecipientCode());
        recipient.setName(getCommonInfo().getRecipientName());
        messageType.setRecipient(recipient);
        if (getCommonInfo().getOriginatorCode() != null || getCommonInfo().getOriginatorName() != null) {
            OrgExternalType originator = new OrgExternalType();
            originator.setCode(getCommonInfo().getOriginatorCode());
            originator.setName(getCommonInfo().getSenderName());
            messageType.setOriginator(originator);
        }
        messageType.setTypeCode(TypeCodeType.GFNC);
        messageType.setStatus(StatusType.REQUEST);
        XMLGregorianCalendar xmlGregorianCalendar = new XMLGregorianCalendarImpl();
        messageType.setDate(xmlGregorianCalendar);
        messageType.setExchangeType("0");
        body.setMessage(messageType);
        MessageDataType messageDataType = new MessageDataType();
        messageDataType.setAppData(appData);
        body.setMessageData(messageDataType);
        return body;
    }

    /**
     * ��������� ������ ��� ������� ���������� ���������
     *
     * @param changesRequest ��� ������� ���������� ���������
     * @return ������ ��� ������� ���������� ���������
     */
    private AppDataType getChangesRequestData(GetChanges changesRequest) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetChanges(changesRequest);
        return appDataType;
    }

    private AppDataType getRevokationListRequestData(GetRevokationList changesRequest) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetRevokationList(changesRequest);
        return appDataType;
    }

    private AppDataType getListDictionariesRequestData(GetListDictionary getListDictionariesRequest) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetListDictionary(getListDictionariesRequest);
        return appDataType;
    }

    private AppDataType getDictionaryRequestData(GetDictionary getDictionaryRequest) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetDictionary(getDictionaryRequest);
        return appDataType;
    }

    /**
     * ��������� ������ ��� ������� �����������
     *
     * @param stateStructure ��� ������� �����������
     * @return ������ ��� ������� �����������
     */
    private AppDataType getRStateStructureData(GetRStateStructure stateStructure) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetRStateStructure(stateStructure);
        return appDataType;
    }

    /**
     * ��������� ������ ��� ������� �������� ������
     *
     * @param passport ��� ������� �������� ������
     * @return ������ ��� ������� �������� ������
     */
    private AppDataType getPsPassportData(GetPsPassport passport) {
        AppDataType appDataType = new AppDataType();
        appDataType.setGetPsPassport(passport);
        return appDataType;
    }

    /**
     * ����� ���������� ������������ ��������
     */
    public static class CommonInfo {
        /**
         * ��� �����������
         */
        private String senderCode;
        /**
         * ������������ �����������
         */
        private String senderName;
        /**
         * ��� ����������
         */
        private String recipientCode;
        /**
         * ������������ ����������
         */
        private String recipientName;
        /**
         * ��� �����������
         */
        private String originatorCode;
        /**
         * ������������ �����������
         */
        private String originatorName;

        /**
         * @return ��� �����������
         */
        public String getSenderCode() {
            return senderCode;
        }

        /**
         * @param senderCode ��� �����������
         */
        public void setSenderCode(String senderCode) {
            this.senderCode = senderCode;
        }

        /**
         * @return ������������ ����������
         */
        public String getSenderName() {
            return senderName;
        }

        /**
         * @param senderName ������������ ����������
         */
        public void setSenderName(String senderName) {
            this.senderName = senderName;
        }

        /**
         * @return ��� ����������
         */
        public String getRecipientCode() {
            return recipientCode;
        }

        /**
         * @param recipientCode ��� ����������
         */
        public void setRecipientCode(String recipientCode) {
            this.recipientCode = recipientCode;
        }

        /**
         * @return ������������ ����������
         */
        public String getRecipientName() {
            return recipientName;
        }

        /**
         * @param recipientName ������������ ����������
         */
        public void setRecipientName(String recipientName) {
            this.recipientName = recipientName;
        }

        /**
         * @return ��� �����������
         */
        public String getOriginatorCode() {
            return originatorCode;
        }

        /**
         * @param originatorCode ��� �����������
         */
        public void setOriginatorCode(String originatorCode) {
            this.originatorCode = originatorCode;
        }

        /**
         * @return ������������ �����������
         */
        public String getOriginatorName() {
            return originatorName;
        }

        /**
         * @param originatorName ������������ �����������
         */
        public void setOriginatorName(String originatorName) {
            this.originatorName = originatorName;
        }
    }

    /**
     * ���������� ��� ������� ������ � ������� ����
     */
    public static class SmevSign {
        /**
         * ����� ��������
         */
        private String storeAlias;
        /**
         * ������ ���������
         */
        private String storePassword;
        /**
         * ������� ������
         */
        private KeyPairPool keyPairPool;

        public String getStoreAlias() {
            return storeAlias;
        }

        public void setStoreAlias(String storeAlias) {
            this.storeAlias = storeAlias;
        }

        public String getStorePassword() {
            return storePassword;
        }

        public void setStorePassword(String storePassword) {
            this.storePassword = storePassword;
        }

        public KeyPairPool getKeyPairPool() {
            return keyPairPool;
        }

        public void setKeyPairPool(KeyPairPool keyPairPool) {
            this.keyPairPool = keyPairPool;
        }
    }
}
