package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFeedBack complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="getFeedBack">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="feedBackId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFeedBack", propOrder = {
        "feedBackId"
})
public class GetFeedBack
        extends Request {

    @XmlElement(required = true)
    protected String feedBackId;

    /**
     * Gets the value of the feedBackId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFeedBackId() {
        return feedBackId;
    }

    /**
     * Sets the value of the feedBackId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFeedBackId(String value) {
        this.feedBackId = value;
    }

}
