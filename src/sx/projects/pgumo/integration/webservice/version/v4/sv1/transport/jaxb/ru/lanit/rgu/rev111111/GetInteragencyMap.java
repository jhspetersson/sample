package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInteragencyMap complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="getInteragencyMap">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="interagencyMapId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInteragencyMap", propOrder = {
        "interagencyMapId"
})
public class GetInteragencyMap
        extends Request {

    protected long interagencyMapId;

    /**
     * Gets the value of the interagencyMapId property.
     */
    public long getInteragencyMapId() {
        return interagencyMapId;
    }

    /**
     * Sets the value of the interagencyMapId property.
     */
    public void setInteragencyMapId(long value) {
        this.interagencyMapId = value;
    }

}
