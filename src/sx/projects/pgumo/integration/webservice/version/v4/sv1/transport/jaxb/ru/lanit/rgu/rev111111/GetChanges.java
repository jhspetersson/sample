package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for getChanges complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="getChanges">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="SSNInterval" type="{http://rgu.lanit.ru/rev111111}ssnRange"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChanges", propOrder = {
        "ssnInterval"
})
public class GetChanges
        extends Request {

    @XmlElement(name = "SSNInterval", required = true)
    protected SsnRange ssnInterval;

    /**
     * Gets the value of the ssnInterval property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange }
     */
    public SsnRange getSSNInterval() {
        return ssnInterval;
    }

    /**
     * Sets the value of the ssnInterval property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.SsnRange }
     */
    public void setSSNInterval(SsnRange value) {
        this.ssnInterval = value;
    }

}
