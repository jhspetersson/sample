package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class PsPassportStatusInfoTransformer<T extends GetPsPassportResponse.PsPassport.StatusInfo> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(PsPassportStatusInfoTransformer.class);

    public PsPassportStatusInfoTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T status, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(status, "RGUStatus", callback);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T status) throws RguTransformationException {
        return RguTransformationUtils.parseRef(status.getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "id";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T status) {
        return status.getPublished();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T status, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", getObjRguId(status).toString());
        map.put("chDate", RguTransformationUtils.convertDate(status.getCreationDate()));
        return map;
    }
}
