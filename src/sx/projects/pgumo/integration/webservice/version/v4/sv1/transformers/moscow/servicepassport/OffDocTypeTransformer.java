package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;

public class OffDocTypeTransformer<T extends GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc.DocumentClass> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(OffDocTypeTransformer.class);

    public OffDocTypeTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T type, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return RguTransformationUtils.getOrCreateDictionaryItem("pprNpdType", "name", type.getValue());
    }
}
