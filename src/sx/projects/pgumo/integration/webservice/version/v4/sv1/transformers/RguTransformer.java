package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;

import javax.annotation.Nonnull;

public interface RguTransformer<T> {

    public SXObj transform(@Nonnull T obj, @Nonnull RguTransformerCallback callback) throws RguTransformationException;

    public void delete(@Nonnull T obj, @Nonnull RguTransformerCallback callback) throws RguTransformationException;

}
