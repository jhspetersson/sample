package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInteragencyMapRights complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="getInteragencyMapRights">
 *   &lt;complexContent>
 *     &lt;extension base="{http://rgu.lanit.ru/rev111111}request">
 *       &lt;sequence>
 *         &lt;element name="interagencyMapRightsId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInteragencyMapRights", propOrder = {
        "interagencyMapRightsId"
})
public class GetInteragencyMapRights
        extends Request {

    protected long interagencyMapRightsId;

    /**
     * Gets the value of the interagencyMapRightsId property.
     */
    public long getInteragencyMapRightsId() {
        return interagencyMapRightsId;
    }

    /**
     * Sets the value of the interagencyMapRightsId property.
     */
    public void setInteragencyMapRightsId(long value) {
        this.interagencyMapRightsId = value;
    }

}
