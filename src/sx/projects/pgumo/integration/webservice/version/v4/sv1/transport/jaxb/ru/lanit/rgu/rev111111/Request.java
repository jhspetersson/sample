package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for request complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "request")
@XmlSeeAlso({
        GetAdmReglament.class,
        GetPsPassport.class,
        RegisterFeedBack.class,
        GetEPGUSectionDictionary.class,
        GetInteragencyMap.class,
        UpdateStatus.class,
        GetUserDictionary.class,
        GetDictionary.class,
        GetChanges.class,
        GetListDictionary.class,
        GetRevokationList.class,
        GetRStateStructure.class,
        GetInteragencyMapRights.class,
        GetWorkDocument.class,
        PsPassportPreview.class,
        GetListUserDictionary.class,
        GetFeedBack.class
})
public abstract class Request {


}
