package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeCodeType.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="TypeCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GSRV"/>
 *     &lt;enumeration value="GFNC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "TypeCodeType")
@XmlEnum
public enum TypeCodeType {

    GSRV,
    GFNC;

    public static sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.TypeCodeType fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
