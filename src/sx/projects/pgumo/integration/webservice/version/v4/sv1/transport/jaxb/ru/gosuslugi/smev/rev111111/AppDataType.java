package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AppDataType complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="AppDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getChanges"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getPsPassport"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getRevokationList"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getRStateStructure"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}updateStatus"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getInteragencyMap"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getAdmReglament"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getWorkDocument"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getInteragencyMapRights"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}registerFeedBack"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getFeedBack"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}psPassportPreview"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getEPGUSectionDictionary"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getDictionary"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getListDictionary"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getUserDictionary"/>
 *           &lt;element ref="{http://rgu.lanit.ru/rev111111}getListUserDictionary"/>
 *         &lt;/choice>
 *         &lt;element ref="{http://rgu.lanit.ru/rev111111}response"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppDataType", propOrder = {
        "getChanges",
        "getPsPassport",
        "getRevokationList",
        "getRStateStructure",
        "updateStatus",
        "getInteragencyMap",
        "getAdmReglament",
        "getWorkDocument",
        "getInteragencyMapRights",
        "registerFeedBack",
        "getFeedBack",
        "psPassportPreview",
        "getEPGUSectionDictionary",
        "getDictionary",
        "getListDictionary",
        "getUserDictionary",
        "getListUserDictionary",
        "response"
})
public class AppDataType {

    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetChanges getChanges;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetPsPassport getPsPassport;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetRevokationList getRevokationList;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetRStateStructure getRStateStructure;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected UpdateStatus updateStatus;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetInteragencyMap getInteragencyMap;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetAdmReglament getAdmReglament;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetWorkDocument getWorkDocument;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetInteragencyMapRights getInteragencyMapRights;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected RegisterFeedBack registerFeedBack;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetFeedBack getFeedBack;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected PsPassportPreview psPassportPreview;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetEPGUSectionDictionary getEPGUSectionDictionary;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetDictionary getDictionary;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetListDictionary getListDictionary;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetUserDictionary getUserDictionary;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111")
    protected GetListUserDictionary getListUserDictionary;
    @XmlElement(namespace = "http://rgu.lanit.ru/rev111111", required = true)
    protected Response response;

    /**
     * Gets the value of the getChanges property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges }
     */
    public GetChanges getGetChanges() {
        return getChanges;
    }

    /**
     * Sets the value of the getChanges property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetChanges }
     */
    public void setGetChanges(GetChanges value) {
        this.getChanges = value;
    }

    /**
     * Gets the value of the getPsPassport property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetPsPassport }
     */
    public GetPsPassport getGetPsPassport() {
        return getPsPassport;
    }

    /**
     * Sets the value of the getPsPassport property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetPsPassport }
     */
    public void setGetPsPassport(GetPsPassport value) {
        this.getPsPassport = value;
    }

    /**
     * Gets the value of the getRevokationList property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRevokationList }
     */
    public GetRevokationList getGetRevokationList() {
        return getRevokationList;
    }

    /**
     * Sets the value of the getRevokationList property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRevokationList }
     */
    public void setGetRevokationList(GetRevokationList value) {
        this.getRevokationList = value;
    }

    /**
     * Gets the value of the getRStateStructure property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure }
     */
    public GetRStateStructure getGetRStateStructure() {
        return getRStateStructure;
    }

    /**
     * Sets the value of the getRStateStructure property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetRStateStructure }
     */
    public void setGetRStateStructure(GetRStateStructure value) {
        this.getRStateStructure = value;
    }

    /**
     * Gets the value of the updateStatus property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus }
     */
    public UpdateStatus getUpdateStatus() {
        return updateStatus;
    }

    /**
     * Sets the value of the updateStatus property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.UpdateStatus }
     */
    public void setUpdateStatus(UpdateStatus value) {
        this.updateStatus = value;
    }

    /**
     * Gets the value of the getInteragencyMap property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap }
     */
    public GetInteragencyMap getGetInteragencyMap() {
        return getInteragencyMap;
    }

    /**
     * Sets the value of the getInteragencyMap property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMap }
     */
    public void setGetInteragencyMap(GetInteragencyMap value) {
        this.getInteragencyMap = value;
    }

    /**
     * Gets the value of the getAdmReglament property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament }
     */
    public GetAdmReglament getGetAdmReglament() {
        return getAdmReglament;
    }

    /**
     * Sets the value of the getAdmReglament property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetAdmReglament }
     */
    public void setGetAdmReglament(GetAdmReglament value) {
        this.getAdmReglament = value;
    }

    /**
     * Gets the value of the getWorkDocument property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument }
     */
    public GetWorkDocument getGetWorkDocument() {
        return getWorkDocument;
    }

    /**
     * Sets the value of the getWorkDocument property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetWorkDocument }
     */
    public void setGetWorkDocument(GetWorkDocument value) {
        this.getWorkDocument = value;
    }

    /**
     * Gets the value of the getInteragencyMapRights property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights }
     */
    public GetInteragencyMapRights getGetInteragencyMapRights() {
        return getInteragencyMapRights;
    }

    /**
     * Sets the value of the getInteragencyMapRights property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetInteragencyMapRights }
     */
    public void setGetInteragencyMapRights(GetInteragencyMapRights value) {
        this.getInteragencyMapRights = value;
    }

    /**
     * Gets the value of the registerFeedBack property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack }
     */
    public RegisterFeedBack getRegisterFeedBack() {
        return registerFeedBack;
    }

    /**
     * Sets the value of the registerFeedBack property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.RegisterFeedBack }
     */
    public void setRegisterFeedBack(RegisterFeedBack value) {
        this.registerFeedBack = value;
    }

    /**
     * Gets the value of the getFeedBack property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack }
     */
    public GetFeedBack getGetFeedBack() {
        return getFeedBack;
    }

    /**
     * Sets the value of the getFeedBack property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetFeedBack }
     */
    public void setGetFeedBack(GetFeedBack value) {
        this.getFeedBack = value;
    }

    /**
     * Gets the value of the psPassportPreview property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview }
     */
    public PsPassportPreview getPsPassportPreview() {
        return psPassportPreview;
    }

    /**
     * Sets the value of the psPassportPreview property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.PsPassportPreview }
     */
    public void setPsPassportPreview(PsPassportPreview value) {
        this.psPassportPreview = value;
    }

    /**
     * Gets the value of the getEPGUSectionDictionary property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary }
     */
    public GetEPGUSectionDictionary getGetEPGUSectionDictionary() {
        return getEPGUSectionDictionary;
    }

    /**
     * Sets the value of the getEPGUSectionDictionary property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.GetEPGUSectionDictionary }
     */
    public void setGetEPGUSectionDictionary(GetEPGUSectionDictionary value) {
        this.getEPGUSectionDictionary = value;
    }

    /**
     * Gets the value of the getDictionary property.
     *
     * @return possible object is
     * {@link GetDictionary }
     */
    public GetDictionary getGetDictionary() {
        return getDictionary;
    }

    /**
     * Sets the value of the getDictionary property.
     *
     * @param value allowed object is
     *              {@link GetDictionary }
     */
    public void setGetDictionary(GetDictionary value) {
        this.getDictionary = value;
    }

    /**
     * Gets the value of the getListDictionary property.
     *
     * @return possible object is
     * {@link GetListDictionary }
     */
    public GetListDictionary getGetListDictionary() {
        return getListDictionary;
    }

    /**
     * Sets the value of the getListDictionary property.
     *
     * @param value allowed object is
     *              {@link GetListDictionary }
     */
    public void setGetListDictionary(GetListDictionary value) {
        this.getListDictionary = value;
    }

    /**
     * Gets the value of the getUserDictionary property.
     *
     * @return possible object is
     * {@link GetUserDictionary }
     */
    public GetUserDictionary getGetUserDictionary() {
        return getUserDictionary;
    }

    /**
     * Sets the value of the getUserDictionary property.
     *
     * @param value allowed object is
     *              {@link GetUserDictionary }
     */
    public void setGetUserDictionary(GetUserDictionary value) {
        this.getUserDictionary = value;
    }

    /**
     * Gets the value of the getListUserDictionary property.
     *
     * @return possible object is
     * {@link GetListUserDictionary }
     */
    public GetListUserDictionary getGetListUserDictionary() {
        return getListUserDictionary;
    }

    /**
     * Sets the value of the getListUserDictionary property.
     *
     * @param value allowed object is
     *              {@link GetListUserDictionary }
     */
    public void setGetListUserDictionary(GetListUserDictionary value) {
        this.getListUserDictionary = value;
    }

    /**
     * Gets the value of the response property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Response }
     */
    public Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.rgu.rev111111.Response }
     */
    public void setResponse(Response value) {
        this.response = value;
    }

}
