package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.statestructure;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.addons.orgsystem.beans.EgOrganization;
import sx.datastore.SXId;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StateStructureTransformer<T extends RStateStructure> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(StateStructureTransformer.class);

    public StateStructureTransformer(RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Override
    @Nonnull
    protected BigInteger getObjRguId(T transformingObject) throws RguTransformationException {
        RguTransformationUtils.RguRef rguRef = RguTransformationUtils.parseRef(transformingObject.getId());
        return rguRef.id;
    }

    protected String getSxClassName() {
        return EgOrganization.ORGANIZATION_CLASS;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "rguId";
    }

    @Override
    @Nonnull
    protected String getObjTitle(T transformingObject) {
        return transformingObject.getTitle();
    }

    @Nonnull
    public SXObj transform(@Nonnull T stateStructure, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(stateStructure, getSxClassName(), callback);
    }


    @Override
    @Nonnull
    protected Map<String, Object> prepareDataMap(T stateStructure,
                                                 RguTransformerCallback callback) throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(EgOrganization.NAME_ATTR, stateStructure.getTitle());
        map.put("short_title", stateStructure.getShortTitle());
        Object transformedObject = callback.getTransformedObject(stateStructure.getParent().getRef());
        if (transformedObject instanceof SXObj) {
            map.put(EgOrganization.PARENT_ATTR, ((SXObj) transformedObject).getId());
        }
        map.put(EgOrganization.HEAD_ATTR, stateStructure.getDirectorPerson());
        map.put(EgOrganization.CONTACTS_ATTR, getContacts(stateStructure));
        //todo: map.put(EgOrganization.ADDRESS_ATTR, stateStructure);
        map.put("workTime", stateStructure.getSchedule());
        map.put("Type_org", getStateStructureTypeId(stateStructure, callback));
        map.put("offices", getOfficesIds(stateStructure, callback));
        map.put(getRguIdAttrName(), getObjRguId(stateStructure));
        SXId admLevelId = getAdministrativeLevelId(stateStructure, callback);
        map.put("admLvl", admLevelId);
        List<SXId> admClassificationListId = new ArrayList<SXId>();
        if (admLevelId != null) {
            RguTransformationUtils.RguRef admLevelRef = RguTransformationUtils.parseRef(stateStructure.getAdministrativeLevel().getRef());
            SXId admClassificationId = RguTransformationUtils.findClassification(admLevelRef.type, admLevelRef.id.toString(), true);
            if (admClassificationId != null) {
                admClassificationListId.add(admClassificationId);
            }
        }
        map.put("classification", admClassificationListId);
        map.put(EgOrganization.URL_ATTR, stateStructure.getWebResource());
        map.put("isPublish", stateStructure.getStatusInfo().getPublished() != null && stateStructure.getStatusInfo().getPublished().equals("PUBLISHED"));
        return map;
    }

    private String getContacts(RStateStructure stateStructure) {
        return stateStructure.getCallCenterPhone()
                + "\n" + stateStructure.getWebResource()
                + "\n" + stateStructure.getEmail();
    }

    @Nullable
    private SXId getStateStructureTypeId(@Nonnull RStateStructure stateStructure,
                                         @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        StateStructureType stateStructureType = stateStructure.getStateStructureType();
        if (StringUtils.isEmpty(stateStructureType.getValue())) {
            return null;
        }
        return transformObj(stateStructureType, callback).getId();
    }

    @Nullable
    private SXId getAdministrativeLevelId(@Nonnull RStateStructure stateStructure,
                                          @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        AdministrativeLevel administrativeLevel = stateStructure.getAdministrativeLevel();
        if (StringUtils.isEmpty(administrativeLevel.getValue())) {
            return null;
        }
        return transformObj(administrativeLevel, callback).getId();
    }

    @Nullable
    private List<SXId> getOfficesIds(@Nonnull RStateStructure stateStructure,
                                     @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        RStateStructure.Offices offices = stateStructure.getOffices();
        if (offices == null) {
            return null;
        }
        List<RStateStructure.Offices.Office> officeList = offices.getOffice();
        List<SXId> officesIds = new ArrayList<SXId>(officeList.size());
        for (RStateStructure.Offices.Office office : officeList) {
            officesIds.add(getOfficeId(office, callback));
        }
        return officesIds;
    }

    @Nonnull
    private SXId getOfficeId(@Nonnull RStateStructure.Offices.Office office,
                             @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        return transformObj(office, callback).getId();
    }
}
