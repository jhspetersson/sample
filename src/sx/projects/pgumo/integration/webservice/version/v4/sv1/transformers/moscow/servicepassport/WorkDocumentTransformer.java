package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.datastore.SXObj;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguAbstractTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformationUtils;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerCallback;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


public class WorkDocumentTransformer<T extends GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument> extends RguAbstractTransformer<T> {
    protected static final Logger log = LoggerFactory.getLogger(WorkDocumentTransformer.class);

    public WorkDocumentTransformer(@Nonnull RguTransformerFactory transformerFactory) {
        super(transformerFactory);
    }

    @Nonnull
    protected BigInteger getObjRguId(@Nonnull T offDoc) throws RguTransformationException {
        return RguTransformationUtils.parseRef(offDoc.getId()).id;
    }

    @Nonnull
    protected String getRguIdAttrName() {
        return "idRgu";
    }

    @Nonnull
    protected String getObjTitle(@Nonnull T offDoc) {
        return offDoc.getTitle();
    }

    @Nonnull
    protected Map<String, Object> prepareDataMap(@Nonnull T workDoc, @Nonnull RguTransformerCallback callback)
            throws RguTransformationException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("guid", getObjRguId(workDoc));
        map.put("idRgu", getObjRguId(workDoc));
        GetPsPassportResponse.PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.Template workDocTemplate = workDoc.getTemplate();
        if (workDocTemplate != null) {
            String workDocTemplateId = workDocTemplate.getId();
            if (!Strings.isNullOrEmpty(workDocTemplateId)) {
                map.put("idDocRgu", RguTransformationUtils.parseRef(workDocTemplateId).id.toString());
            }
            map.put("rguDocName", RguTransformationUtils.cutFileName(workDocTemplate.getFileName()));
            if (!Strings.isNullOrEmpty(workDocTemplate.getSize())) {
                try {
                    map.put("rguDocSize", Integer.valueOf(workDocTemplate.getSize()));
                } catch (Throwable e) {
                }
            }
            map.put("rguDocMime", workDocTemplate.getFileType());
        }
        map.put("title", workDoc.getTitle());

        return map;
    }

    @Nonnull
    @Override
    public SXObj transform(@Nonnull T offDoc, @Nonnull RguTransformerCallback callback) throws RguTransformationException {
        return transformation(offDoc, "pprDoc", callback);
    }

}
