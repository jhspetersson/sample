package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigInteger;

public class BigIntegerAdapter extends XmlAdapter<String, BigInteger> {

    @Override
    public String marshal(BigInteger value) throws Exception {
        if (value != null) {
            return value.toString();
        }
        return null;
    }

    @Override
    public BigInteger unmarshal(String s) throws Exception {
        return new BigInteger(s);
    }
}