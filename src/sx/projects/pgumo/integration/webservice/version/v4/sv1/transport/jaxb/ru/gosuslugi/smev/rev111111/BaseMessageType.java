package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageDataType;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BaseMessageType complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="BaseMessageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://smev.gosuslugi.ru/rev111111}MessageType"/>
 *         &lt;element name="MessageData" type="{http://smev.gosuslugi.ru/rev111111}MessageDataType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseMessageType", propOrder = {
        "message",
        "messageData"
})
public class BaseMessageType {

    @XmlElement(name = "Message", required = true)
    protected MessageType message;
    @XmlElement(name = "MessageData", required = true)
    protected MessageDataType messageData;

    /**
     * Gets the value of the message property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageType }
     */
    public MessageType getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageType }
     */
    public void setMessage(MessageType value) {
        this.message = value;
    }

    /**
     * Gets the value of the messageData property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageDataType }
     */
    public MessageDataType getMessageData() {
        return messageData;
    }

    /**
     * Sets the value of the messageData property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.gosuslugi.smev.rev111111.MessageDataType }
     */
    public void setMessageData(MessageDataType value) {
        this.messageData = value;
    }

}
