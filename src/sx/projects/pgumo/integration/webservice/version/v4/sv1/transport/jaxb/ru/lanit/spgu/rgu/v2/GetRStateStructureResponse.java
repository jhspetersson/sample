package sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2;

import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RStateStructure">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="shortTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="directorPerson" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="callCenterPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="webResource" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="administrativeLevel">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="stateStructureType">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="subordinationType">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="parent">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="emblem">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Document">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="blobData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="offices">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Office" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="isHeadOffice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="address" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="federalSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="okrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="cityArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="townArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="townType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="building" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="room" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="estateStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="structureStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="IFNSLPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="IFNSLPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="IFNSNPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="IFNSNPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="StreetGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="HouseGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *                                       &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *                                       &lt;element name="cityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
 *                                       &lt;element name="territories">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Address2Territory" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="territory">
 *                                                             &lt;complexType>
 *                                                               &lt;simpleContent>
 *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                                                   &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/extension>
 *                                                               &lt;/simpleContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="contacts">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Contact">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="functionary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="workPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
 *                   &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="statusInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="originalNode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="modificationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="published" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ssn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "rStateStructure"
})
@XmlRootElement(name = "getRStateStructureResponse")
public class GetRStateStructureResponse {

    @XmlElement(name = "RStateStructure", required = true)
    protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure rStateStructure;

    /**
     * Gets the value of the rStateStructure property.
     *
     * @return possible object is
     * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure }
     */
    public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure getRStateStructure() {
        return rStateStructure;
    }

    /**
     * Sets the value of the rStateStructure property.
     *
     * @param value allowed object is
     *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure }
     */
    public void setRStateStructure(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure value) {
        this.rStateStructure = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p/>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p/>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="shortTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="directorPerson" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="callCenterPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="webResource" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="administrativeLevel">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="stateStructureType">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="subordinationType">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="parent">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emblem">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Document">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="blobData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="offices">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Office" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="isHeadOffice" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="address" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="federalSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="okrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="cityArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="townArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="townType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="building" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="room" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="estateStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="structureStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="IFNSLPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="IFNSLPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="IFNSNPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="IFNSNPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="StreetGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="HouseGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
     *                             &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
     *                             &lt;element name="cityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
     *                             &lt;element name="territories">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Address2Territory" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="territory">
     *                                                   &lt;complexType>
     *                                                     &lt;simpleContent>
     *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                                                         &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/extension>
     *                                                     &lt;/simpleContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="contacts">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Contact">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="functionary" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="workPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
     *         &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="statusInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="originalNode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="modificationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="published" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ssn" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "foreignId",
            "title",
            "shortTitle",
            "expeditionSchedule",
            "email",
            "directorPerson",
            "schedule",
            "callCenterPhone",
            "ogrn",
            "webResource",
            "administrativeLevel",
            "stateStructureType",
            "subordinationType",
            "parent",
            "emblem",
            "offices",
            "contacts",
            "financialDetails",
            "serviceRules",
            "statusInfo"
    })
    public static class RStateStructure {

        @XmlElement(name = "foreign_id", required = true)
        protected String foreignId;
        @XmlElement(required = true)
        protected String title;
        @XmlElement(required = true)
        protected String shortTitle;
        @XmlElement(required = true)
        protected String expeditionSchedule;
        @XmlElement(required = true)
        protected String email;
        @XmlElement(required = true)
        protected String directorPerson;
        @XmlElement(required = true)
        protected String schedule;
        @XmlElement(required = true)
        protected String callCenterPhone;
        @XmlElement(name = "OGRN", required = true)
        protected String ogrn;
        @XmlElement(required = true)
        protected String webResource;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel administrativeLevel;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType stateStructureType;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType subordinationType;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent parent;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem emblem;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices offices;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts contacts;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin financialDetails;
        @XmlElement(required = true)
        protected String serviceRules;
        @XmlElement(required = true)
        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo statusInfo;
        @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
        protected String id;

        /**
         * Gets the value of the foreignId property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getForeignId() {
            return foreignId;
        }

        /**
         * Sets the value of the foreignId property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setForeignId(String value) {
            this.foreignId = value;
        }

        /**
         * Gets the value of the title property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getTitle() {
            return title;
        }

        /**
         * Sets the value of the title property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setTitle(String value) {
            this.title = value;
        }

        /**
         * Gets the value of the shortTitle property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getShortTitle() {
            return shortTitle;
        }

        /**
         * Sets the value of the shortTitle property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setShortTitle(String value) {
            this.shortTitle = value;
        }

        /**
         * Gets the value of the expeditionSchedule property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getExpeditionSchedule() {
            return expeditionSchedule;
        }

        /**
         * Sets the value of the expeditionSchedule property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setExpeditionSchedule(String value) {
            this.expeditionSchedule = value;
        }

        /**
         * Gets the value of the email property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getEmail() {
            return email;
        }

        /**
         * Sets the value of the email property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setEmail(String value) {
            this.email = value;
        }

        /**
         * Gets the value of the directorPerson property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getDirectorPerson() {
            return directorPerson;
        }

        /**
         * Sets the value of the directorPerson property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setDirectorPerson(String value) {
            this.directorPerson = value;
        }

        /**
         * Gets the value of the schedule property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSchedule() {
            return schedule;
        }

        /**
         * Sets the value of the schedule property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSchedule(String value) {
            this.schedule = value;
        }

        /**
         * Gets the value of the callCenterPhone property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCallCenterPhone() {
            return callCenterPhone;
        }

        /**
         * Sets the value of the callCenterPhone property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCallCenterPhone(String value) {
            this.callCenterPhone = value;
        }

        /**
         * Gets the value of the ogrn property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getOGRN() {
            return ogrn;
        }

        /**
         * Sets the value of the ogrn property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setOGRN(String value) {
            this.ogrn = value;
        }

        /**
         * Gets the value of the webResource property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getWebResource() {
            return webResource;
        }

        /**
         * Sets the value of the webResource property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setWebResource(String value) {
            this.webResource = value;
        }

        /**
         * Gets the value of the administrativeLevel property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel getAdministrativeLevel() {
            return administrativeLevel;
        }

        /**
         * Sets the value of the administrativeLevel property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel }
         */
        public void setAdministrativeLevel(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel value) {
            this.administrativeLevel = value;
        }

        /**
         * Gets the value of the stateStructureType property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType getStateStructureType() {
            return stateStructureType;
        }

        /**
         * Sets the value of the stateStructureType property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType }
         */
        public void setStateStructureType(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType value) {
            this.stateStructureType = value;
        }

        /**
         * Gets the value of the subordinationType property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType getSubordinationType() {
            return subordinationType;
        }

        /**
         * Sets the value of the subordinationType property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType }
         */
        public void setSubordinationType(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.SubordinationType value) {
            this.subordinationType = value;
        }

        /**
         * Gets the value of the parent property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent getParent() {
            return parent;
        }

        /**
         * Sets the value of the parent property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent }
         */
        public void setParent(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Parent value) {
            this.parent = value;
        }

        /**
         * Gets the value of the emblem property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem getEmblem() {
            return emblem;
        }

        /**
         * Sets the value of the emblem property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem }
         */
        public void setEmblem(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem value) {
            this.emblem = value;
        }

        /**
         * Gets the value of the offices property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices getOffices() {
            return offices;
        }

        /**
         * Sets the value of the offices property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices }
         */
        public void setOffices(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices value) {
            this.offices = value;
        }

        /**
         * Gets the value of the contacts property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts getContacts() {
            return contacts;
        }

        /**
         * Sets the value of the contacts property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts }
         */
        public void setContacts(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts value) {
            this.contacts = value;
        }

        /**
         * Gets the value of the financialDetails property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin getFinancialDetails() {
            return financialDetails;
        }

        /**
         * Sets the value of the financialDetails property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin }
         */
        public void setFinancialDetails(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin value) {
            this.financialDetails = value;
        }

        /**
         * Gets the value of the serviceRules property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getServiceRules() {
            return serviceRules;
        }

        /**
         * Sets the value of the serviceRules property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setServiceRules(String value) {
            this.serviceRules = value;
        }

        /**
         * Gets the value of the statusInfo property.
         *
         * @return possible object is
         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo }
         */
        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo getStatusInfo() {
            return statusInfo;
        }

        /**
         * Sets the value of the statusInfo property.
         *
         * @param value allowed object is
         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo }
         */
        public void setStatusInfo(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StatusInfo value) {
            this.statusInfo = value;
        }

        /**
         * Gets the value of the id property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        public static class AdministrativeLevel {

            @XmlValue
            protected String value1;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String ref;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String value;

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the ref property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getRef() {
                return ref;
            }

            /**
             * Sets the value of the ref property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setRef(String value) {
                this.ref = value;
            }

            /**
             * Gets the value of the value1 property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue1() {
                return value1;
            }

            /**
             * Sets the value of the value1 property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue1(String value) {
                this.value1 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Contact">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="functionary" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="workPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "contact"
        })
        public static class Contacts {

            @XmlElement(name = "Contact", required = true)
            protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact contact;

            /**
             * Gets the value of the contact property.
             *
             * @return possible object is
             * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact }
             */
            public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact getContact() {
                return contact;
            }

            /**
             * Sets the value of the contact property.
             *
             * @param value allowed object is
             *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact }
             */
            public void setContact(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Contacts.Contact value) {
                this.contact = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * <p/>
             * <p>The following schema fragment specifies the expected content contained within this class.
             * <p/>
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="functionary" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="workPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "lastName",
                    "firstName",
                    "middleName",
                    "email",
                    "schedule",
                    "comments",
                    "functionary",
                    "fax",
                    "workPhone"
            })
            public static class Contact {

                @XmlElement(required = true)
                protected String lastName;
                @XmlElement(required = true)
                protected String firstName;
                @XmlElement(required = true)
                protected String middleName;
                @XmlElement(required = true)
                protected String email;
                @XmlElement(required = true)
                protected String schedule;
                @XmlElement(required = true)
                protected String comments;
                @XmlElement(required = true)
                protected String functionary;
                @XmlElement(required = true)
                protected String fax;
                @XmlElement(required = true)
                protected String workPhone;
                @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                protected String id;

                /**
                 * Gets the value of the lastName property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getLastName() {
                    return lastName;
                }

                /**
                 * Sets the value of the lastName property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setLastName(String value) {
                    this.lastName = value;
                }

                /**
                 * Gets the value of the firstName property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFirstName() {
                    return firstName;
                }

                /**
                 * Sets the value of the firstName property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFirstName(String value) {
                    this.firstName = value;
                }

                /**
                 * Gets the value of the middleName property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getMiddleName() {
                    return middleName;
                }

                /**
                 * Sets the value of the middleName property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setMiddleName(String value) {
                    this.middleName = value;
                }

                /**
                 * Gets the value of the email property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the schedule property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getSchedule() {
                    return schedule;
                }

                /**
                 * Sets the value of the schedule property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setSchedule(String value) {
                    this.schedule = value;
                }

                /**
                 * Gets the value of the comments property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getComments() {
                    return comments;
                }

                /**
                 * Sets the value of the comments property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setComments(String value) {
                    this.comments = value;
                }

                /**
                 * Gets the value of the functionary property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFunctionary() {
                    return functionary;
                }

                /**
                 * Sets the value of the functionary property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFunctionary(String value) {
                    this.functionary = value;
                }

                /**
                 * Gets the value of the fax property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFax() {
                    return fax;
                }

                /**
                 * Sets the value of the fax property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFax(String value) {
                    this.fax = value;
                }

                /**
                 * Gets the value of the workPhone property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getWorkPhone() {
                    return workPhone;
                }

                /**
                 * Sets the value of the workPhone property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setWorkPhone(String value) {
                    this.workPhone = value;
                }

                /**
                 * Gets the value of the id property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setId(String value) {
                    this.id = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Document">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="blobData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "document"
        })
        public static class Emblem {

            @XmlElement(name = "Document", required = true)
            protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document document;

            /**
             * Gets the value of the document property.
             *
             * @return possible object is
             * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document }
             */
            public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document getDocument() {
                return document;
            }

            /**
             * Sets the value of the document property.
             *
             * @param value allowed object is
             *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document }
             */
            public void setDocument(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Emblem.Document value) {
                this.document = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * <p/>
             * <p>The following schema fragment specifies the expected content contained within this class.
             * <p/>
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fileType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="size" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="blobData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "fileName",
                    "fileType",
                    "size",
                    "blobData"
            })
            public static class Document {

                @XmlElement(required = true)
                protected String fileName;
                @XmlElement(required = true)
                protected String fileType;
                @XmlElement(required = true)
                protected String size;
                protected byte[] blobData;
                @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                protected String id;

                /**
                 * Gets the value of the fileName property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFileName() {
                    return fileName;
                }

                /**
                 * Sets the value of the fileName property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFileName(String value) {
                    this.fileName = value;
                }

                /**
                 * Gets the value of the fileType property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFileType() {
                    return fileType;
                }

                /**
                 * Sets the value of the fileType property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFileType(String value) {
                    this.fileType = value;
                }

                /**
                 * Gets the value of the size property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getSize() {
                    return size;
                }

                /**
                 * Sets the value of the size property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setSize(String value) {
                    this.size = value;
                }

                /**
                 * Gets the value of the blobData property.
                 *
                 * @return possible object is
                 * byte[]
                 */
                public byte[] getBlobData() {
                    return blobData;
                }

                /**
                 * Sets the value of the blobData property.
                 *
                 * @param value allowed object is
                 *              byte[]
                 */
                public void setBlobData(byte[] value) {
                    this.blobData = ((byte[]) value);
                }

                /**
                 * Gets the value of the id property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setId(String value) {
                    this.id = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Office" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="isHeadOffice" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="address" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="federalSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="okrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="cityArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="townArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="townType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="building" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="room" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="estateStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="structureStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="IFNSLPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="IFNSLPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="IFNSNPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="IFNSNPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="StreetGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="HouseGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
         *                   &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
         *                   &lt;element name="cityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
         *                   &lt;element name="territories">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Address2Territory" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="territory">
         *                                         &lt;complexType>
         *                                           &lt;simpleContent>
         *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                                               &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/extension>
         *                                           &lt;/simpleContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "office"
        })
        public static class Offices {

            @XmlElement(name = "Office")
            protected List<sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office> office;

            /**
             * Gets the value of the office property.
             * <p/>
             * <p/>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the office property.
             * <p/>
             * <p/>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOffice().add(newItem);
             * </pre>
             * <p/>
             * <p/>
             * <p/>
             * Objects of the following type(s) are allowed in the list
             * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office }
             */
            public List<sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office> getOffice() {
                if (office == null) {
                    office = new ArrayList<sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office>();
                }
                return this.office;
            }


            /**
             * <p>Java class for anonymous complex type.
             * <p/>
             * <p>The following schema fragment specifies the expected content contained within this class.
             * <p/>
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="foreign_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="isHeadOffice" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="expeditionSchedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="OGRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="address" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="federalSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="okrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="cityArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="townArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="townType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="building" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="room" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="estateStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="structureStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="IFNSLPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="IFNSLPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="IFNSNPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="IFNSNPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="StreetGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="HouseGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
             *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
             *         &lt;element name="cityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="financialDetails" type="{http://spgu.lanit.ru/rgu/v2.5}fin"/>
             *         &lt;element name="territories">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Address2Territory" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="territory">
             *                               &lt;complexType>
             *                                 &lt;simpleContent>
             *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *                                     &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/extension>
             *                                 &lt;/simpleContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="serviceRules" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "foreignId",
                    "title",
                    "isHeadOffice",
                    "expeditionSchedule",
                    "ogrn",
                    "email",
                    "schedule",
                    "comments",
                    "url",
                    "phone",
                    "fax",
                    "address",
                    "latitude",
                    "longitude",
                    "cityType",
                    "financialDetails",
                    "territories",
                    "serviceRules"
            })
            public static class Office {

                @XmlElement(name = "foreign_id")
                protected int foreignId;
                @XmlElement(required = true)
                protected String title;
                @XmlElement(required = true)
                protected String isHeadOffice;
                @XmlElement(required = true)
                protected String expeditionSchedule;
                @XmlElement(name = "OGRN", required = true)
                protected String ogrn;
                @XmlElement(required = true)
                protected String email;
                @XmlElement(required = true)
                protected String schedule;
                @XmlElement(required = true)
                protected String comments;
                @XmlElement(required = true)
                protected String url;
                @XmlElement(required = true)
                protected String phone;
                @XmlElement(required = true)
                protected String fax;
                protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address address;
                protected Float latitude;
                protected Float longitude;
                @XmlElement(required = true)
                protected String cityType;
                @XmlElement(required = true)
                protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin financialDetails;
                @XmlElement(required = true)
                protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories territories;
                @XmlElement(required = true)
                protected String serviceRules;
                @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                protected String id;

                /**
                 * Gets the value of the foreignId property.
                 */
                public int getForeignId() {
                    return foreignId;
                }

                /**
                 * Sets the value of the foreignId property.
                 */
                public void setForeignId(int value) {
                    this.foreignId = value;
                }

                /**
                 * Gets the value of the title property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getTitle() {
                    return title;
                }

                /**
                 * Sets the value of the title property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setTitle(String value) {
                    this.title = value;
                }

                /**
                 * Gets the value of the isHeadOffice property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getIsHeadOffice() {
                    return isHeadOffice;
                }

                /**
                 * Sets the value of the isHeadOffice property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setIsHeadOffice(String value) {
                    this.isHeadOffice = value;
                }

                /**
                 * Gets the value of the expeditionSchedule property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getExpeditionSchedule() {
                    return expeditionSchedule;
                }

                /**
                 * Sets the value of the expeditionSchedule property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setExpeditionSchedule(String value) {
                    this.expeditionSchedule = value;
                }

                /**
                 * Gets the value of the ogrn property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getOGRN() {
                    return ogrn;
                }

                /**
                 * Sets the value of the ogrn property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setOGRN(String value) {
                    this.ogrn = value;
                }

                /**
                 * Gets the value of the email property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the schedule property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getSchedule() {
                    return schedule;
                }

                /**
                 * Sets the value of the schedule property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setSchedule(String value) {
                    this.schedule = value;
                }

                /**
                 * Gets the value of the comments property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getComments() {
                    return comments;
                }

                /**
                 * Sets the value of the comments property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setComments(String value) {
                    this.comments = value;
                }

                /**
                 * Gets the value of the url property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getUrl() {
                    return url;
                }

                /**
                 * Sets the value of the url property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setUrl(String value) {
                    this.url = value;
                }

                /**
                 * Gets the value of the phone property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getPhone() {
                    return phone;
                }

                /**
                 * Sets the value of the phone property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setPhone(String value) {
                    this.phone = value;
                }

                /**
                 * Gets the value of the fax property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getFax() {
                    return fax;
                }

                /**
                 * Sets the value of the fax property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setFax(String value) {
                    this.fax = value;
                }

                /**
                 * Gets the value of the address property.
                 *
                 * @return possible object is
                 * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address }
                 */
                public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address getAddress() {
                    return address;
                }

                /**
                 * Sets the value of the address property.
                 *
                 * @param value allowed object is
                 *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address }
                 */
                public void setAddress(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Address value) {
                    this.address = value;
                }

                /**
                 * Gets the value of the latitude property.
                 *
                 * @return possible object is
                 * {@link Float }
                 */
                public Float getLatitude() {
                    return latitude;
                }

                /**
                 * Sets the value of the latitude property.
                 *
                 * @param value allowed object is
                 *              {@link Float }
                 */
                public void setLatitude(Float value) {
                    this.latitude = value;
                }

                /**
                 * Gets the value of the longitude property.
                 *
                 * @return possible object is
                 * {@link Float }
                 */
                public Float getLongitude() {
                    return longitude;
                }

                /**
                 * Sets the value of the longitude property.
                 *
                 * @param value allowed object is
                 *              {@link Float }
                 */
                public void setLongitude(Float value) {
                    this.longitude = value;
                }

                /**
                 * Gets the value of the cityType property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getCityType() {
                    return cityType;
                }

                /**
                 * Sets the value of the cityType property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setCityType(String value) {
                    this.cityType = value;
                }

                /**
                 * Gets the value of the financialDetails property.
                 *
                 * @return possible object is
                 * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin }
                 */
                public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin getFinancialDetails() {
                    return financialDetails;
                }

                /**
                 * Sets the value of the financialDetails property.
                 *
                 * @param value allowed object is
                 *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.Fin }
                 */
                public void setFinancialDetails(Fin value) {
                    this.financialDetails = value;
                }

                /**
                 * Gets the value of the territories property.
                 *
                 * @return possible object is
                 * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories }
                 */
                public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories getTerritories() {
                    return territories;
                }

                /**
                 * Sets the value of the territories property.
                 *
                 * @param value allowed object is
                 *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories }
                 */
                public void setTerritories(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories value) {
                    this.territories = value;
                }

                /**
                 * Gets the value of the serviceRules property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getServiceRules() {
                    return serviceRules;
                }

                /**
                 * Sets the value of the serviceRules property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setServiceRules(String value) {
                    this.serviceRules = value;
                }

                /**
                 * Gets the value of the id property.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setId(String value) {
                    this.id = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * <p/>
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * <p/>
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="federalSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="okrug" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="cityArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="townArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="townType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="house" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="building" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="room" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="estateStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="structureStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="IFNSLPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="IFNSLPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="IFNSNPCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="IFNSNPAreaCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="okato" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="StreetGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="HouseGuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "federalSubject",
                        "okrug",
                        "area",
                        "city",
                        "cityArea",
                        "town",
                        "townArea",
                        "townType",
                        "street",
                        "house",
                        "houseType",
                        "building",
                        "buildingType",
                        "room",
                        "estateStatus",
                        "structureStatus",
                        "ifnslpCode",
                        "ifnslpAreaCode",
                        "ifnsnpCode",
                        "ifnsnpAreaCode",
                        "okato",
                        "oktmo",
                        "zip",
                        "streetGuid",
                        "houseGuid"
                })
                public static class Address {

                    @XmlElement(required = true)
                    protected String federalSubject;
                    @XmlElement(required = true)
                    protected String okrug;
                    @XmlElement(required = true)
                    protected String area;
                    @XmlElement(required = true)
                    protected String city;
                    @XmlElement(required = true)
                    protected String cityArea;
                    @XmlElement(required = true)
                    protected String town;
                    @XmlElement(required = true)
                    protected String townArea;
                    @XmlElement(required = true)
                    protected String townType;
                    @XmlElement(required = true)
                    protected String street;
                    @XmlElement(required = true)
                    protected String house;
                    @XmlElement(required = true)
                    protected String houseType;
                    @XmlElement(required = true)
                    protected String building;
                    @XmlElement(required = true)
                    protected String buildingType;
                    @XmlElement(required = true)
                    protected String room;
                    @XmlElement(required = true)
                    protected String estateStatus;
                    @XmlElement(required = true)
                    protected String structureStatus;
                    @XmlElement(name = "IFNSLPCode", required = true)
                    protected String ifnslpCode;
                    @XmlElement(name = "IFNSLPAreaCode", required = true)
                    protected String ifnslpAreaCode;
                    @XmlElement(name = "IFNSNPCode", required = true)
                    protected String ifnsnpCode;
                    @XmlElement(name = "IFNSNPAreaCode", required = true)
                    protected String ifnsnpAreaCode;
                    @XmlElement(required = true)
                    protected String okato;
                    @XmlElement(required = true)
                    protected String oktmo;
                    @XmlElement(required = true)
                    protected String zip;
                    @XmlElement(name = "StreetGuid", required = true)
                    protected String streetGuid;
                    @XmlElement(name = "HouseGuid", required = true)
                    protected String houseGuid;

                    /**
                     * Gets the value of the federalSubject property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getFederalSubject() {
                        return federalSubject;
                    }

                    /**
                     * Sets the value of the federalSubject property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setFederalSubject(String value) {
                        this.federalSubject = value;
                    }

                    /**
                     * Gets the value of the okrug property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getOkrug() {
                        return okrug;
                    }

                    /**
                     * Sets the value of the okrug property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setOkrug(String value) {
                        this.okrug = value;
                    }

                    /**
                     * Gets the value of the area property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getArea() {
                        return area;
                    }

                    /**
                     * Sets the value of the area property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setArea(String value) {
                        this.area = value;
                    }

                    /**
                     * Gets the value of the city property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Sets the value of the city property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Gets the value of the cityArea property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getCityArea() {
                        return cityArea;
                    }

                    /**
                     * Sets the value of the cityArea property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setCityArea(String value) {
                        this.cityArea = value;
                    }

                    /**
                     * Gets the value of the town property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getTown() {
                        return town;
                    }

                    /**
                     * Sets the value of the town property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setTown(String value) {
                        this.town = value;
                    }

                    /**
                     * Gets the value of the townArea property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getTownArea() {
                        return townArea;
                    }

                    /**
                     * Sets the value of the townArea property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setTownArea(String value) {
                        this.townArea = value;
                    }

                    /**
                     * Gets the value of the townType property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getTownType() {
                        return townType;
                    }

                    /**
                     * Sets the value of the townType property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setTownType(String value) {
                        this.townType = value;
                    }

                    /**
                     * Gets the value of the street property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getStreet() {
                        return street;
                    }

                    /**
                     * Sets the value of the street property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setStreet(String value) {
                        this.street = value;
                    }

                    /**
                     * Gets the value of the house property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getHouse() {
                        return house;
                    }

                    /**
                     * Sets the value of the house property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setHouse(String value) {
                        this.house = value;
                    }

                    /**
                     * Gets the value of the houseType property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getHouseType() {
                        return houseType;
                    }

                    /**
                     * Sets the value of the houseType property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setHouseType(String value) {
                        this.houseType = value;
                    }

                    /**
                     * Gets the value of the building property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getBuilding() {
                        return building;
                    }

                    /**
                     * Sets the value of the building property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setBuilding(String value) {
                        this.building = value;
                    }

                    /**
                     * Gets the value of the buildingType property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getBuildingType() {
                        return buildingType;
                    }

                    /**
                     * Sets the value of the buildingType property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setBuildingType(String value) {
                        this.buildingType = value;
                    }

                    /**
                     * Gets the value of the room property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getRoom() {
                        return room;
                    }

                    /**
                     * Sets the value of the room property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setRoom(String value) {
                        this.room = value;
                    }

                    /**
                     * Gets the value of the estateStatus property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getEstateStatus() {
                        return estateStatus;
                    }

                    /**
                     * Sets the value of the estateStatus property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setEstateStatus(String value) {
                        this.estateStatus = value;
                    }

                    /**
                     * Gets the value of the structureStatus property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getStructureStatus() {
                        return structureStatus;
                    }

                    /**
                     * Sets the value of the structureStatus property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setStructureStatus(String value) {
                        this.structureStatus = value;
                    }

                    /**
                     * Gets the value of the ifnslpCode property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getIFNSLPCode() {
                        return ifnslpCode;
                    }

                    /**
                     * Sets the value of the ifnslpCode property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setIFNSLPCode(String value) {
                        this.ifnslpCode = value;
                    }

                    /**
                     * Gets the value of the ifnslpAreaCode property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getIFNSLPAreaCode() {
                        return ifnslpAreaCode;
                    }

                    /**
                     * Sets the value of the ifnslpAreaCode property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setIFNSLPAreaCode(String value) {
                        this.ifnslpAreaCode = value;
                    }

                    /**
                     * Gets the value of the ifnsnpCode property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getIFNSNPCode() {
                        return ifnsnpCode;
                    }

                    /**
                     * Sets the value of the ifnsnpCode property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setIFNSNPCode(String value) {
                        this.ifnsnpCode = value;
                    }

                    /**
                     * Gets the value of the ifnsnpAreaCode property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getIFNSNPAreaCode() {
                        return ifnsnpAreaCode;
                    }

                    /**
                     * Sets the value of the ifnsnpAreaCode property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setIFNSNPAreaCode(String value) {
                        this.ifnsnpAreaCode = value;
                    }

                    /**
                     * Gets the value of the okato property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getOkato() {
                        return okato;
                    }

                    /**
                     * Sets the value of the okato property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setOkato(String value) {
                        this.okato = value;
                    }

                    /**
                     * Gets the value of the oktmo property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getOktmo() {
                        return oktmo;
                    }

                    /**
                     * Sets the value of the oktmo property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setOktmo(String value) {
                        this.oktmo = value;
                    }

                    /**
                     * Gets the value of the zip property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getZip() {
                        return zip;
                    }

                    /**
                     * Sets the value of the zip property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setZip(String value) {
                        this.zip = value;
                    }

                    /**
                     * Gets the value of the streetGuid property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getStreetGuid() {
                        return streetGuid;
                    }

                    /**
                     * Sets the value of the streetGuid property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setStreetGuid(String value) {
                        this.streetGuid = value;
                    }

                    /**
                     * Gets the value of the houseGuid property.
                     *
                     * @return possible object is
                     * {@link String }
                     */
                    public String getHouseGuid() {
                        return houseGuid;
                    }

                    /**
                     * Sets the value of the houseGuid property.
                     *
                     * @param value allowed object is
                     *              {@link String }
                     */
                    public void setHouseGuid(String value) {
                        this.houseGuid = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * <p/>
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * <p/>
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Address2Territory" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="territory">
                 *                     &lt;complexType>
                 *                       &lt;simpleContent>
                 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                 *                           &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/extension>
                 *                       &lt;/simpleContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "content"
                })
                public static class Territories {

                    @XmlElementRef(name = "Address2Territory", namespace = "http://spgu.lanit.ru/rgu/v2.5", type = JAXBElement.class)
                    @XmlMixed
                    protected List<Serializable> content;

                    /**
                     * Gets the value of the content property.
                     * <p/>
                     * <p/>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the content property.
                     * <p/>
                     * <p/>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getContent().add(newItem);
                     * </pre>
                     * <p/>
                     * <p/>
                     * <p/>
                     * Objects of the following type(s) are allowed in the list
                     * {@link javax.xml.bind.JAXBElement }{@code <}{@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory }{@code >}
                     * {@link String }
                     */
                    public List<Serializable> getContent() {
                        if (content == null) {
                            content = new ArrayList<Serializable>();
                        }
                        return this.content;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * <p/>
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * <p/>
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="territory">
                     *           &lt;complexType>
                     *             &lt;simpleContent>
                     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                     *                 &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/extension>
                     *             &lt;/simpleContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                            "territory"
                    })
                    public static class Address2Territory {

                        @XmlElement(required = true)
                        protected sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory territory;

                        /**
                         * Gets the value of the territory property.
                         *
                         * @return possible object is
                         * {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory }
                         */
                        public sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory getTerritory() {
                            return territory;
                        }

                        /**
                         * Sets the value of the territory property.
                         *
                         * @param value allowed object is
                         *              {@link sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory }
                         */
                        public void setTerritory(sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office.Territories.Address2Territory.Territory value) {
                            this.territory = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * <p/>
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * <p/>
                         * <pre>
                         * &lt;complexType>
                         *   &lt;simpleContent>
                         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                         *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="okato" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="oktmo" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/extension>
                         *   &lt;/simpleContent>
                         * &lt;/complexType>
                         * </pre>
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                                "value"
                        })
                        public static class Territory {

                            @XmlValue
                            protected String value1;
                            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                            protected String ref;
                            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                            protected String value;
                            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                            protected String okato;
                            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
                            protected String oktmo;

                            /**
                             * Gets the value of the value property.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getValue() {
                                return value;
                            }

                            /**
                             * Sets the value of the value property.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setValue(String value) {
                                this.value = value;
                            }

                            /**
                             * Gets the value of the ref property.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getRef() {
                                return ref;
                            }

                            /**
                             * Sets the value of the ref property.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setRef(String value) {
                                this.ref = value;
                            }

                            /**
                             * Gets the value of the value1 property.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getValue1() {
                                return value1;
                            }

                            /**
                             * Sets the value of the value1 property.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setValue1(String value) {
                                this.value1 = value;
                            }

                            /**
                             * Gets the value of the okato property.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getOkato() {
                                return okato;
                            }

                            /**
                             * Sets the value of the okato property.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setOkato(String value) {
                                this.okato = value;
                            }

                            /**
                             * Gets the value of the oktmo property.
                             *
                             * @return possible object is
                             * {@link String }
                             */
                            public String getOktmo() {
                                return oktmo;
                            }

                            /**
                             * Sets the value of the oktmo property.
                             *
                             * @param value allowed object is
                             *              {@link String }
                             */
                            public void setOktmo(String value) {
                                this.oktmo = value;
                            }

                        }

                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        public static class Parent {

            @XmlValue
            protected String value1;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String ref;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String value;

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the ref property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getRef() {
                return ref;
            }

            /**
             * Sets the value of the ref property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setRef(String value) {
                this.ref = value;
            }

            /**
             * Gets the value of the value1 property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue1() {
                return value1;
            }

            /**
             * Sets the value of the value1 property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue1(String value) {
                this.value1 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        public static class StateStructureType {

            @XmlValue
            protected String value1;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String ref;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String value;

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the ref property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getRef() {
                return ref;
            }

            /**
             * Sets the value of the ref property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setRef(String value) {
                this.ref = value;
            }

            /**
             * Gets the value of the value1 property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue1() {
                return value1;
            }

            /**
             * Sets the value of the value1 property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue1(String value) {
                this.value1 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="originalNode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="modificationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="published" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ssn" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "comments",
                "author",
                "originalNode",
                "creationDate",
                "modificationDate",
                "published",
                "ssn"
        })
        public static class StatusInfo {

            @XmlElement(required = true)
            protected String comments;
            @XmlElement(required = true)
            protected String author;
            @XmlElement(required = true)
            protected String originalNode;
            @XmlElement(required = true)
            protected String creationDate;
            @XmlElement(required = true)
            protected String modificationDate;
            @XmlElement(required = true)
            protected String published;
            protected int ssn;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String id;

            /**
             * Gets the value of the comments property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getComments() {
                return comments;
            }

            /**
             * Sets the value of the comments property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setComments(String value) {
                this.comments = value;
            }

            /**
             * Gets the value of the author property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getAuthor() {
                return author;
            }

            /**
             * Sets the value of the author property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setAuthor(String value) {
                this.author = value;
            }

            /**
             * Gets the value of the originalNode property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getOriginalNode() {
                return originalNode;
            }

            /**
             * Sets the value of the originalNode property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setOriginalNode(String value) {
                this.originalNode = value;
            }

            /**
             * Gets the value of the creationDate property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getCreationDate() {
                return creationDate;
            }

            /**
             * Sets the value of the creationDate property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setCreationDate(String value) {
                this.creationDate = value;
            }

            /**
             * Gets the value of the modificationDate property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getModificationDate() {
                return modificationDate;
            }

            /**
             * Sets the value of the modificationDate property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setModificationDate(String value) {
                this.modificationDate = value;
            }

            /**
             * Gets the value of the published property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getPublished() {
                return published;
            }

            /**
             * Sets the value of the published property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setPublished(String value) {
                this.published = value;
            }

            /**
             * Gets the value of the ssn property.
             */
            public int getSsn() {
                return ssn;
            }

            /**
             * Sets the value of the ssn property.
             */
            public void setSsn(int value) {
                this.ssn = value;
            }

            /**
             * Gets the value of the id property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getId() {
                return id;
            }

            /**
             * Sets the value of the id property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setId(String value) {
                this.id = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * <p/>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p/>
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="ref" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="value1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "value"
        })
        public static class SubordinationType {

            @XmlValue
            protected String value1;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String ref;
            @XmlAttribute(namespace = "http://spgu.lanit.ru/rgu/v2.5")
            protected String value;

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the ref property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getRef() {
                return ref;
            }

            /**
             * Sets the value of the ref property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setRef(String value) {
                this.ref = value;
            }

            /**
             * Gets the value of the value1 property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue1() {
                return value1;
            }

            /**
             * Sets the value of the value1 property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue1(String value) {
                this.value1 = value;
            }

        }

    }

}
