package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.projects.pgumo.integration.exceptions.RguTransformationFactoryException;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformer;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.RguTransformerFactory;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.servicepassport.*;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers.moscow.statestructure.*;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetDictionaryResponse;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetListDictionaryResponse;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.OffDocs.PsPassport2OffDoc.OffDoc;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.CommunicationForms.CommunicationForm;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.GroundsOfRefusal.GroundOfRefusal;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceOffDoc;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.AdministrativeLevel;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.Offices.Office;
import sx.projects.pgumo.integration.webservice.version.v4.sv1.transport.jaxb.ru.lanit.spgu.rgu.v2.GetRStateStructureResponse.RStateStructure.StateStructureType;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class RguMoscowTransformerFactory implements RguTransformerFactory {
    protected static final Logger log = LoggerFactory.getLogger(RguMoscowTransformerFactory.class);
    private static final Map<Class, RguTransformer> TRANSFORMERS = new HashMap<Class, RguTransformer>();

    {
        TRANSFORMERS.put(RStateStructure.class, new StateStructureTransformer<RStateStructure>(this));
        TRANSFORMERS.put(StateStructureType.class, new StateStructureTypeTransformer<StateStructureType>(this));
        TRANSFORMERS.put(AdministrativeLevel.class, new AdministrativeLevelTransformer<AdministrativeLevel>(this));
        TRANSFORMERS.put(Office.class, new OfficeTransformer<Office>(this));
        TRANSFORMERS.put(PsPassport.class, new PsPassportTransformer<PsPassport>(this));
        TRANSFORMERS.put(Service.class, new ServiceTransformer<Service>(this));
        TRANSFORMERS.put(CommunicationForm.class, new CommunicationFormTransformer<CommunicationForm>(this));
        TRANSFORMERS.put(PsPassport.StatusInfo.class, new PsPassportStatusInfoTransformer<PsPassport.StatusInfo>(this));
        TRANSFORMERS.put(GroundOfRefusal.class, new GroundOfRefusalTransformer<GroundOfRefusal>(this));
        TRANSFORMERS.put(ServiceOffDoc.class, new ServiceOffDocTransformer<ServiceOffDoc>(this));
        TRANSFORMERS.put(ServiceOffDoc.class, new ServiceOffDocTransformer<ServiceOffDoc>(this));
        TRANSFORMERS.put(OffDoc.class, new OffDocTransformer<OffDoc>(this));
        TRANSFORMERS.put(OffDoc.DocumentClass.class, new OffDocTypeTransformer<OffDoc.DocumentClass>(this));
        TRANSFORMERS.put(GetListDictionaryResponse.ObjectRefList.List.ObjectRef.class, new ListDictionaryTransformer<GetListDictionaryResponse.ObjectRefList.List.ObjectRef>(this));
        TRANSFORMERS.put(GetDictionaryResponse.ObjectRefList.List.ObjectRef.class, new DictionaryTransformer<GetDictionaryResponse.ObjectRefList.List.ObjectRef>(this));
        TRANSFORMERS.put(Service.ServiceTargets.ServiceTarget.class, new ServiceTargetTransformer<Service.ServiceTargets.ServiceTarget>(this));
        TRANSFORMERS.put(Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments.class, new TargetInDocumentsTransformer<Service.ServiceTargets.ServiceTarget.InDocuments.TargetInDocuments>(this));
        TRANSFORMERS.put(Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.class, new TargetScenariosTransformer<Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario>(this));
        TRANSFORMERS.put(GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments.class, new ScenarioOutDocumentTransformer<GetPsPassportResponse.PsPassport.Services.PassportServices.Service.ServiceTargets.ServiceTarget.TargetScenarios.TargetScenario.OutDocuments.ScenarioOutDocuments>(this));
        TRANSFORMERS.put(PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument.class, new WorkDocumentTransformer<PsPassport.WorkDocuments.PassportWorkDocuments.WorkDocument>(this));
        TRANSFORMERS.put(Service.Faqs.Faq.class, new ServiceFaqTransformer<Service.Faqs.Faq>(this));
        TRANSFORMERS.put(Service.ServiceTargets.ServiceTarget.Payments.ServicePayment.class, new ServicePaymentTransformer<Service.ServiceTargets.ServiceTarget.Payments.ServicePayment>(this));
    }

    private static RguMoscowTransformerFactory rguTambovTransformerFactory = new RguMoscowTransformerFactory();

    private RguMoscowTransformerFactory() {
    }

    public static RguMoscowTransformerFactory getFactory() {
        return rguTambovTransformerFactory;
    }

    public RguTransformer getTransformer(@Nonnull Object obj) throws RguTransformationFactoryException {
        RguTransformer rguTransformer = TRANSFORMERS.get(obj.getClass());
        if (rguTransformer != null) {
            return rguTransformer;
        }
        //throw new RguTransformationFactoryException("�� ������ ����������� ��� ������� ������ " + obj.getClass());
        log.error("�� ������ ����������� ��� ������� ������ " + obj.getClass());
        return null;
    }

}
