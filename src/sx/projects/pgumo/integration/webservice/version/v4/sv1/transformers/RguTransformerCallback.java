package sx.projects.pgumo.integration.webservice.version.v4.sv1.transformers;

import sx.projects.pgumo.integration.exceptions.RguTransformationException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface RguTransformerCallback {

    @Nullable
    Object getTransformedObject(@Nonnull String ref) throws RguTransformationException;

    boolean getSaveArchivedObjects();
}
