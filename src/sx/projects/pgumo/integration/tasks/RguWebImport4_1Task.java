package sx.projects.pgumo.integration.tasks;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.cms.CmsActionUtils;
import sx.common.MsgBean;
import sx.scheduler.SXTask;
import sx.projects.pgumo.integration.webservice.RguIntegrator;

import java.util.ArrayList;
import java.util.List;

/**
 * �������� ����� ������ ���������� ���
 *
 * @see <a href="http://bugs.mysitex.com/browse/MSMEV-2694">http://bugs.mysitex.com/browse/MSMEV-2694</a>
 * @see <a href="http://conf.mysitex.com/pages/viewpage.action?pageId=21104987">http://conf.mysitex.com/pages/viewpage.action?pageId=21104987</a>
 */
public class RguWebImport4_1Task extends SXTask {
    protected static final Logger log = LoggerFactory.getLogger(sx.projects.pgumo.integration.tasks.RguWebImport4_1Task.class);

    public String getWsdl() {
        return getStringAttr(Attribute.WSDL);
    }

    public String getProjectCode() {
        return getStringAttr(Attribute.PROJECT_CODE);
    }

    /**
     * ����� ������, ��������� �� ����� ������� ������ ���, � ������� ���������� ��������������� ������ ������
     * ����������� ����� ��������� ���������� ������, ����� ������� ��� ��������� ������� ������ �������� ��� � ���.
     *
     * @return long
     */
    public long getLastSsn() {
        return new Long(getStringAttr(Attribute.LAST_SSN));
    }

    /**
     * ����� ������, ��������� �� ����� ������� ������ ���, �� ������� ������������� ��������������� ������ ������
     *
     * @return long
     */
    public long getSsnTo() {
        String ssnTo = getStringAttr(Attribute.SSN_TO);
        return ssnTo != null ? Long.valueOf(ssnTo) : 0;
    }

    /**
     * ����� ������, ���� ����������, �� �������������� ���������� ����� getListDictionary � getDictionary ��� ������� ����������� �������,
     * � ���������� ������������� ������� egClassification � ���� �������������� ����������� (������ �� �� - ����������� - ����������).
     *
     * @return boolean
     */
    public boolean getUpdateDictionaries() {
        return getBoolAttr(Attribute.UPDATE_DICTIONARIES);
    }

    /**
     * ����� ������, ���� ���������� � true, �� ��� ������ ��������, ��� ��������� � ����, � ������������� ���������� ����������, � �� ��������,
     * ������������ ��, � ������� ������� archive == true. ��. ����� RguAbstractTransformer::getSXObj()
     *
     * ������ ����� ��������:
     *
     * egService
     * egOrganization
     * egServiceTarget
     * egProcedureStep
     * egOffice
     *
     * @see <a href="http://bugs.mysitex.com/browse/MSMEV-3460">http://bugs.mysitex.com/browse/MSMEV-3460</a>
     * @return boolean
     */
    public boolean getSaveArchivedObjects() {
        return getBoolAttr(Attribute.SAVE_ARCHIVED_OBJECTS);
    }

    /**
     * ����� ������, ���� ���������� � true, �� �� ��� ������������� ������ ������������ ������,
     * � ����������� � ������������� ������������ ��� ��������� ������� ������
     *
     * @see <a href="http://bugs.mysitex.com/browse/MSMEV-3495">http://bugs.mysitex.com/browse/MSMEV-3495</a>
     * @return boolean
     */
    public boolean getUpdateOnlyRegional() {
        return getBoolAttr(Attribute.UPDATE_ONLY_REGIONAL);
    }

    /**
     * ����� ������, ���� �� ������, �� ������ ���������� �������� ����������� ������ �� ��������� ����� ������� ���������������
     *
     * @see <a href="http://bugs.mysitex.com/browse/MSMEV-3535">http://bugs.mysitex.com/browse/MSMEV-3535</a>
     * @return ������ ��������������� ����� ���, ������� ��������� ��������
     */
    public List<String> getSelectedServices() {
        List<String> result = new ArrayList<String>();
        String values = getStringAttr(Attribute.SELECTED_SERVICES);

        if (StringUtils.isNotBlank(values)) {
            String[] ids = values.split(",");
            for (String s : ids) {
                result.add(s.trim());
            }
        }

        return result;
    }

    /**
     * ����� ������, ���� �� ������, �� ������ ���������� �������� ����������� ��� �� ��������� ����� ������� ���������������
     *
     * @see <a href="http://bugs.mysitex.com/browse/MSMEV-3535">http://bugs.mysitex.com/browse/MSMEV-3535</a>
     * @return ������ ��������������� ��� ���, ������� ��������� ��������
     */
    public List<String> getSelectedOrgs() {
        List<String> result = new ArrayList<String>();
        String values = getStringAttr(Attribute.SELECTED_ORGS);

        if (StringUtils.isNotBlank(values)) {
            String[] ids = values.split(",");
            for (String s : ids) {
                result.add(s.trim());
            }
        }

        return result;
    }

    /**
     *
     * @return MsgBean � ���������� � ���������� ���������� ������
     * @throws Exception
     */
    @Override
    protected MsgBean execute() throws Exception {
        RguIntegrator rguIntegrator = RguIntegrator.getRguIntegrator(getWsdl(), getProjectCode());
        rguIntegrator.setLastSsn(getLastSsn());
        rguIntegrator.setSsnTo(getSsnTo());
        rguIntegrator.setUpdateDictionaries(getUpdateDictionaries());
        rguIntegrator.setSaveArchivedObjects(getSaveArchivedObjects());
        rguIntegrator.setUpdateOnlyRegional(getUpdateOnlyRegional());
        rguIntegrator.setSelectedServices(getSelectedServices());
        rguIntegrator.setSelectedOrgs(getSelectedOrgs());
        MsgBean integrate = rguIntegrator.integrate();
        CmsActionUtils.updateObj(getId(), Attribute.LAST_SSN, rguIntegrator.getCurrentSsn());
        return integrate;
    }

    public interface Attribute {
        String WSDL = "wsdl";
        String PROJECT_CODE = "projectCode";
        String LAST_SSN = "lastSSN";
        String SSN_TO = "ssnTo";
        String UPDATE_DICTIONARIES = "updateDictionaries";
        String SAVE_ARCHIVED_OBJECTS = "saveArchivedObjects";
        String UPDATE_ONLY_REGIONAL = "updateOnlyRegional";
        String SELECTED_SERVICES = "selectedServices";
        String SELECTED_ORGS = "selectedOrgs";
    }
}
