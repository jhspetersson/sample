package sx.projects.pgumo.integration.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RguDbException extends RuntimeException {

    public RguDbException(String message, Throwable e) {
        super(message, e);
    }

}
