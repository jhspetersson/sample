package sx.projects.pgumo.integration.exceptions;

public class RguIntegrationException extends Exception {
    public RguIntegrationException(String message) {
        super(message);
    }
}
