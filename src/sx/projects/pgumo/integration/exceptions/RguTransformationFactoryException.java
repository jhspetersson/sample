package sx.projects.pgumo.integration.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.projects.pgumo.integration.exceptions.RguTransformationException;

public class RguTransformationFactoryException extends RguTransformationException {

    public RguTransformationFactoryException(String message) {
        super(message);
    }
}
