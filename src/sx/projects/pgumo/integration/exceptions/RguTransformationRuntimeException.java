package sx.projects.pgumo.integration.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RguTransformationRuntimeException extends RuntimeException {

    public RguTransformationRuntimeException(String message) {
        super(message);
    }

    public RguTransformationRuntimeException(String message, Throwable e) {
        super(message, e);
    }
}
