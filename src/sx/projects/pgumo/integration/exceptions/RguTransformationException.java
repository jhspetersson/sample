package sx.projects.pgumo.integration.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RguTransformationException extends Exception {

    public RguTransformationException(String message) {
        super(message);
    }

    public RguTransformationException(String message, Throwable e) {
        super(message, e);
    }
}
